<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> <i class="fa fa-plus"></i>  @lang('modules.purchaseparts.title')</h4>
</div>
<div class="modal-body">
    {!! Form::open(array('id' => 'add_purchase_form', 'class'=>'form-horizontal ','method'=>'POST')) !!}
    <div class="form-body">
        <div class="row">
            <div class="col-md-5">
                    <div id="warehouseBox" class="form-group ">
                        {{-- <label>@lang('app.account')</label> --}}
                        <select class="form-control" name="account_id">
                            @foreach(App\Accounts::all() as $w)
                            <option value="{{ $w->id }}" @if($w->is_default==1) selected @endif>{{ $w->name }}</option>
                            @endforeach
                        </select>
                        
                        <div id="errorwarehouse"></div>
                    </div>
                </div>
            <div id="addMoreBox1" class="clearfix">
                
                <div class="col-md-5" style="margin-left: 5px;">
                    <div class="form-group" id="itemBox">
                       <select class="form-control" name="part">
                            @foreach(App\Parts::all() as $w)
                            <option value="{{ $w->id }}">{{ $w->name }}</option>
                            @endforeach
                        </select>
                        <div id="erroritem"></div>
                    </div>
                </div>
                 <div class="col-md-5" style="margin-left: 5px;">
                    <div class="form-group" id="amountBox">
                       <input class="form-control " autocomplete="off" id="amount1" name="qty" type="number" value="" placeholder="Qty" required />
                        <div id="erroramount"></div>
                    </div>
                </div>
                <div class="col-md-5" style="margin-left: 5px;">
                    <div class="form-group" id="priceBox">
                       <input class="form-control " autocomplete="off" id="price1" name="price" type="number" value="" placeholder="Price" required />
                        <div id="errorprice"></div>
                    </div>
                </div>
                <div class="col-md-5" style="margin-left: 5px;">
                    <div class="form-group" id="dateBox">
                       <input class="form-control " autocomplete="off" id="date1" name="date" type="date" value="" placeholder="Date" required />
                        <div id="errordate"></div>
                    </div>
                </div>
                <div class="col-md-5" style="margin-left: 5px;">
                    <div class="form-group" id="noteBox">
                       <input class="form-control " autocomplete="off" id="note1" name="note" type="text" value="" placeholder="Note"/>
                        <div id="errornote"></div>
                    </div>
                </div>
                <div class="col-md-1">
                    {{--<button type="button"  onclick="removeBox(1)"  class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>--}}
                </div>
            </div>
            {{-- <div id="insertBefore"></div>
            <div class="clearfix">

            </div> --}}
            {{-- <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                @lang('app.add') <i class="fa fa-plus"></i>
            </button> --}}
        </div>
        <!--/row-->
    </div>
    {!! Form::close() !!}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
    <button type="button" onclick="storepurchase()" class="btn btn-info save-event waves-effect waves-light"><i class="fa fa-check"></i> @lang('app.save')
    </button>
</div>

<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script>
    var $insertBefore = $('#insertBefore');
    var $i = 0;
    // Date Picker
    jQuery('.date-picker').datepicker({
        format: '{{ $global->date_picker_format }}',
        autoclose: true,
        todayHighlight: true
    });
    // Add More Inputs
    $('#plusButton').click(function(){

        $i = $i+1;
        var indexs = $i+1;
        $(' <div id="addMoreBox'+indexs+'" class="clearfix"> ' +
            '<div class="col-md-5"><div class="form-group "><input autocomplete="off" class="form-control date-picker'+$i+'" id="dateField'+indexs+'" name="date['+$i+']" data-date-format="dd/mm/yyyy" type="text" value="" placeholder="Date"/></div></div>' +
            '<div class="col-md-5 "style="margin-left:5px;"><div class="form-group"><input class="form-control " name="occasion['+$i+']" type="text" value="" placeholder="Occasion"/></div></div>' +
            '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' +
            '</div>').insertBefore($insertBefore);

        // Recently Added date picker assign
        jQuery('#dateField'+indexs).datepicker({
            format: '{{ $global->date_picker_format }}',
            autoclose: true,
            todayHighlight: true
        });
    });
    // Remove fields
    function removeBox(index){
        $('#addMoreBox'+index).remove();
    }

    // Store Holidays
    function storepurchase(){
        $('#dateBox').removeClass("has-error");
        $('#occasionBox').removeClass("has-error");
        $('#errorDate').html('');
        $('#errorOccasion').html('');
        $('.help-block').remove();
        var url = "{{ route('admin.purchaseparts.store') }}";
        $.easyAjax({
            type: 'POST',
            url: url,
            container: '#add_purchase_form',
            data: $('#add_purchase_form').serialize(),
            success: function (response) {
                console.log([response, 'success']);
                $('#edit-column-form').modal('hide');
            },error: function (response) {
                if(response.status == '422'){
                    console.log(response.responseJSON['errors'])

                    if(typeof response.responseJSON['errors']['date'] != 'undefined' && typeof response.responseJSON['errors']['date'] != 'undefined'){
                        $('#dateBox').addClass("has-error");
                        $('#errorDate').html('<span class="help-block" id="errordate">'+response.responseJSON['errors']['date']+'</span>');
                    }
                    if(typeof response.responseJSON['errors']['warehouse'] != "undefined" && response.responseJSON['errors']['warehouse']  != 'undefined'){
                        $('#warehouseBox').addClass("has-error");
                        $('#errorwarehouse').html('<span class="help-block" id="errorwarehouse">'+response.responseJSON['errors']['warehouse']+'</span>');
                    }
                    if(typeof response.responseJSON['errors']['item'] != "undefined" && response.responseJSON['errors']['item']  != 'undefined'){
                        $('#itemBox').addClass("has-error");
                        $('#erroritem').html('<span class="help-block" id="erroritem">'+response.responseJSON['errors']['item']+'</span>');
                    }
                    if(typeof response.responseJSON['errors']['amount'] != "undefined" && response.responseJSON['errors']['amount']  != 'undefined'){
                        $('#amountBox').addClass("has-error");
                        $('#erroramount').html('<span class="help-block" id="erroramount">'+response.responseJSON['errors']['amount']+'</span>');
                    }
                    if(typeof response.responseJSON['errors']['price'] != "undefined" && response.responseJSON['errors']['price']  != 'undefined'){
                        $('#priceBox').addClass("has-error");
                        $('#errorprice').html('<span class="help-block" id="errorprice">'+response.responseJSON['errors']['price']+'</span>');
                    }

                }
            }
        });
    }

</script>


