@extends('layouts.app')

@section('page-title')
@endsection
<style>
	.rectangle {
  /*height: 50px;*/
  width: 150px;
  border-radius: 10px;
  background-color: #555;
  margin-top:10px;
  margin-bottom:10px;
  margin-right:auto;
  margin-left:auto;
  color:white;
  vertical-align: middle;
  padding:10px;
  font-weight: bold;
}
	.row{
		text-transform: capitalize;
		text-align: center;
	}
	button{
		margin-top: 20px !important;

	}
	.btn{
		text-transform: capitalize;
		max-width: 150px;
		min-width: 150px;
		
	}
	/*Now the CSS*/
* {margin: 0; padding: 0;}

.tree ul {
	padding-top: 20px; position: relative;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

.tree li {
	float: left; text-align: center;
	list-style-type: none;
	position: relative;
	padding: 20px 5px 0 5px;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

/*We will use ::before and ::after to draw the connectors*/

.tree li::before, .tree li::after{
	content: '';
	position: absolute; top: 0; right: 50%;
	border-top: 1px solid #ccc;
	width: 50%; height: 20px;
}
.tree li::after{
	right: auto; left: 50%;
	border-left: 1px solid #ccc;
}

/*We need to remove left-right connectors from elements without 
any siblings*/
.tree li:only-child::after, .tree li:only-child::before {
	display: none;
}

/*Remove space from the top of single children*/
.tree li:only-child{ padding-top: 0;}

/*Remove left connector from first child and 
right connector from last child*/
.tree li:first-child::before, .tree li:last-child::after{
	border: 0 none;
}
/*Adding back the vertical connector to the last nodes*/
.tree li:last-child::before{
	border-right: 1px solid #ccc;
	border-radius: 0 5px 0 0;
	-webkit-border-radius: 0 5px 0 0;
	-moz-border-radius: 0 5px 0 0;
}
.tree li:first-child::after{
	border-radius: 5px 0 0 0;
	-webkit-border-radius: 5px 0 0 0;
	-moz-border-radius: 5px 0 0 0;
}

/*Time to add downward connectors from parents*/
.tree ul ul::before{
	content: '';
	position: absolute; top: 0; left: 50%;
	border-left: 1px solid #ccc;
	width: 0; height: 20px;
}

.tree li a{
	border: 1px solid #ccc;
	padding: 5px 10px;
	text-decoration: none;
	color: #666;
	font-family: arial, verdana, tahoma;
	font-size: 11px;
	display: inline-block;
	max-width:80px;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

/*Time for some hover effects*/
/*We will apply the hover effect the the lineage of the element also*/
.tree li a:hover, .tree li a:hover+ul li a {
	background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
}
/*Connector styles on hover*/
.tree li a:hover+ul li::after, 
.tree li a:hover+ul li::before, 
.tree li a:hover+ul::before, 
.tree li a:hover+ul ul::before{
	border-color:  #94a0b4;
}

/*Thats all. I hope you enjoyed it.
Thanks :)*/
</style>
@section('content')
{{-- {{ $employees }} --}}
@if($ceo = App\EmployeeDetails::where('type','CEO')->first())
<div class="tree">
	<ul>
		<li>
			<a href="{{ route('admin.employees.show',$ceo->id) }}" style="font-weight: bold;">{{ $ceo->user->name }} ({{ $ceo->type }})</a>
			<ul>
				@foreach(App\EmployeeDetails::where('manager_id',$ceo->user->id)->get() as $manager)
				<li>
					<a href="{{ route('admin.employees.show',$manager->user->id) }}" style="font-weight: bold;">{{ $manager->user->name }} ({{ $manager->type }})</a>
					@if(App\EmployeeDetails::where('manager_id',$manager->user->id)->count()>0)
					<ul>
						@foreach(App\EmployeeDetails::where('manager_id',$manager->user->id)->get() as $supervisor)
						<li>
							<a href="{{ route('admin.employees.show',$supervisor->user->id) }}" style="font-weight: bold;">{{ $supervisor->user->name }} ({{ $supervisor->type }})</a>
						</li>
						@endforeach
					</ul>
					@endif
				</li>
				@endforeach
				{{-- <li>
					<a href="#">Child</a>
					<ul>
						<li><a href="#">Grand Child</a></li>
						<li>
							<a href="#">Grand Child</a>
							<ul>
								<li>
									<a href="#">Great Grand Child</a>
								</li>
								<li>
									<a href="#">Great Grand Child</a>
								</li>
								<li>
									<a href="#">Great Grand Child</a>
								</li>
							</ul>
						</li>
						<li><a href="#">Grand Child</a></li>
					</ul>
				</li> --}}
			</ul>
		</li>
	</ul>
</div>
{{-- <div class="row">
	<div class="col col-md-12 text-center">
		<div class="rectangle" style="background-color: #28a745;">{{ $ceo->user->name }} ({{ $ceo->type }})</div>
	</div>
</div>
<div class="row text-center">
	@foreach(App\EmployeeDetails::where('manager_id',$ceo->user->id)->get() as $manager)
	<div class="col col-md-2 text-center">
		<div class="rectangle" style="background-color: #007bff;">{{ $manager->user->name }} ({{ $manager->type }})</div>
		@foreach(App\EmployeeDetails::where('manager_id',$manager->user->id)->get() as $supervisor)
			<div class="rectangle" style="background-color: #ffc107;">{{ $supervisor->user->name }} ({{ $supervisor->type }})</div>
		@endforeach
	</div>
	@endforeach
</div> --}}
@endif
@endsection

@push('footer-script')
<script>
</script>
@endpush
