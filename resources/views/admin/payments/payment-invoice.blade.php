@extends('layouts.app')

@section('content')
@php
function convertNumberToWord($num = false)
{
    $num = str_replace(array(',', ' '), '' , trim($num));
    if(! $num) {
        return false;
    }
    $num = (int) $num;
    $words = array();
    $list1 = array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven',
        'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'
    );
    $list2 = array('', 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety', 'Hundred');
    $list3 = array('', 'Thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
    );
    $num_length = strlen($num);
    $levels = (int) (($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int) ($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
        $tens = (int) ($num_levels[$i] % 100);
        $singles = '';
        if ( $tens < 20 ) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
        } else {
            $tens = (int)($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int) ($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
    } //end for loop
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    return implode(' ', $words);
}
@endphp
<style type="text/css">
    .bgimg{
        background-image: url('{{asset('img/trans.png')}}');
                        background-repeat: no-repeat;
                        background-size: 250px 200px;
                        float:right;
                         background-position: center;

    }
    @media print{
       .bgimg{
        background-image: url('{{asset('img/trans.png')}}') !important;
                        background-repeat: no-repeat !important;
                        background-size: 250px 200px !important;
    } 
    *{
        background-color: rgb(0,0,0,0);
    }
    }
</style>
<div class="container  " style="width: 100%;">
        <div class="row">
            
            
            <div class="col-md-3 pull-left text-center">
                <img src="{{asset('img/watermark_header.jpeg')}}" style="width: 120px; height:100px"></div>
                <div class="col-md-7"></div>
                <div class="col-md-4 pull-right">
                <tr>
                    <td><h5 class="text-right">{{$invoice->invoice_number}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-right">{{ __('app.date')}} : {{$invoice->paid_on}}</h5></td>
                </tr>
            </div>
        </div>
        <hr>
        @php
        $user=Auth::user();
        @endphp

        <div class="container bgimg" style="width: 100%;">
            <table class="table table-borderless" style="background-color: rgb(0,0,0,0); " >
                <tr>
                    <td><h5 class="text-left">{{ __('app.project')}}:  {{$invoice->project->name}}</h5></td>
                </tr>
              {{--   <tr>
                    <td><h5 class="text-left">{{ __('app.truck')}} : {{$payment->driven->truck->code}}</h5></td>
                </tr> --}}
                <tr>
                    <td><h5 class="text-left" >  {{ __('app.paid amount')}}:  <a style="margin-right: 50px;">${{$invoice->amount}}</a></h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left" >   {{ __('app.written')}}:<a style="margin-right: 50px;"> {{ convertNumberToWord($invoice->amount)}} Dollars</a></h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">  {{ __('app.note')}}:  {{$invoice->note}}</h5></td>
                </tr>
            </table>
        </div>
        <br>
        <br>
        <hr>
        <br>
        <br>
        <div class="container" style="width: 100%;">
            <table class="table table-borderless" >
                <tr>
                    <td><p class="text-left">{{ __('app.signature')}} <br>____________ :ناو </p>
                    </td>
                    <td><p class="text-right">{{ __('app.signature')}} <br>{{ __('app.name')}} : {{$user->name}}</p></td>
                </tr>

            </table>
        </div>
        <hr>
        <br>
        <br>
        <br>
        <div class="container  " style="width: 100%;">
        <div class="row">
            
            
            <div class="col-md-3 pull-left text-center">
                <img src="{{asset('img/watermark_header.jpeg')}}" style="width: 120px; height:100px"></div>
                <div class="col-md-7"></div>
                <div class="col-md-4 pull-right">
                <tr>
                    <td><h5 class="text-right">No : SDFP00{{$invoice->id}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-right">{{ __('app.date')}} : {{$invoice->paid_on}}</h5></td>
                </tr>
            </div>
        </div>
        <hr>
        @php
        $user=Auth::user();
        @endphp

        <div class="container bgimg" style="width: 100%;">
            <table class="table table-borderless" style="background-color: rgb(0,0,0,0); " >
                <tr>
                    <td><h5 class="text-left"> {{ __('app.project')}}: {{$invoice->project->name}}</h5></td>
                </tr>
              {{--   <tr>
                    <td><h5 class="text-left">{{ __('app.truck')}} : {{$payment->driven->truck->code}}</h5></td>
                </tr> --}}
                <tr>
                    <td><h5 class="text-left" >  {{ __('app.paid amount')}}:  <a style="margin-right: 50px;">${{$invoice->amount}}</a></h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left" >   {{ __('app.written')}}:<a style="margin-right: 50px;"> {{ convertNumberToWord($invoice->amount)}} Dollars</a></h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">  {{ __('app.note')}}:  {{$invoice->note}}</h5></td>
                </tr>
            </table>
        </div>
        <br>
        <br>
        <hr>
        <br>
        <br>
        <div class="container" style="width: 100%;">
            <table class="table table-borderless" >
                <tr>
                    <td><p class="text-left">{{ __('app.signature')}} <br>____________ :ناو </p>
                    </td>
                    <td><p class="text-right">{{ __('app.signature')}} <br> {{$user->name}}</p></td>
                </tr>

            </table>
        </div>

@endsection