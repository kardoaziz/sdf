@extends('layouts.app')

@section('page-title')

@stop
@section('content')
<div style="max-width: 21cm; margin-left: auto; margin-right: auto; width:100%;">
<a class="btn btn-success btn-outline btn-sm noprint"
                 onclick="window.print();" style="margin-top: 20px;"> <span><i class="fa fa-print"></i> {{ __('modules.invoices.print') }}</span> </a>
<div class="row" style="margin-top: 50px;margin-bottom: 50px;">
	<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center"></div>
	<div class="col-md-6 col-sm-6 col-lg-6 col-xs-6 text-center" style="font-weight: bold; font-size: 30px;">فەرمانی کارگێڕی</div>
	<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-left">
		@lang('app.date'): {{ $administration->date }}<br>
		@lang('app.number'): {{ $administration->id }}
	</div>
</div>
<div class="row" style="margin-top: 30px;margin-right:20px; margin-left: 20px;">
	 {!! $administration->body !!}
</div>
</div>
{{-- <div class="row">
	 <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 text-center">
	 	بەڕێوەبەری کارگە <br>
	 	هێمن 
	 </div>
	 <div class="col-md-9 col-sm-9 col-lg-9 col-xs-9"></div>
</div> --}}
@stop