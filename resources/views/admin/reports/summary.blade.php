@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang("app.menu.home")</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">

<link rel="stylesheet" href="{{ asset('plugins/bower_components/morrisjs/morris.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<style>
    #payments-table_wrapper .dt-buttons{
        display: none !important;
    }
    .round{
        font-size:18px !important;
        border-radius: 50% !important;
        width:22px !important;
        height:22px !important;
        padding: auto !important;
    }
    .badge-danger {
    background-color: red !important;
}
.badge{
    font-size:20px !important;
}
</style>
@endpush

@section('content')


{{-- {{ $fromDate }}
{{ $toDate }} --}}
    @section('filter-section')
        <div class="row">
            <form role="form" action="{{ route('admin.summary-report.store') }}" method="POST">
                    {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="col-md-12">
                <div class="example">
                    <h5 class="box-title m-t-30">@lang('app.selectDateRange')</h5>

                    <div class="input-daterange input-group" id="date-range">
                        <input type="text" class="form-control" id="start-date"
                        name="startDate" placeholder="@lang('app.startDate')"
                                value="{{ $fromDate }}"/>
                        <span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>
                        <input type="text" class="form-control" id="end-date" name="endDate" placeholder="@lang('app.endDate')" 
                                value="{{ $toDate }}"/>
                    </div>
                </div>
            </div>

           
           {{--  <div class="col-md-12">
                <h5 >@lang('app.project')</h5>
                <div class="form-group">
                    <select class="form-control select2" name="project" id="project" data-style="form-control">
                        <option value="all">@lang('modules.client.all')</option>
                        @forelse($projects as $project)
                            <option value="{{$project->id}}">{{ $project->project_name }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
            </div> --}}
            {{-- <div class="col-md-12">
                <div class="form-group">
                    <h5 >@lang('app.client')</h5>
                    <select class="form-control select2" name="client" id="client" data-style="form-control">
                        <option value="all">@lang('modules.client.all')</option>
                        @forelse($clients as $client)
                            <option
                            value="{{ $client->id }}">{{ ucwords($client->name) }}{{ ($client->company_name != '') ? " [".$client->company_name."]" : "" }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
            </div> --}}

            <div class="col-md-12 m-t-20">
                <div class="form-group">
                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i> @lang('app.apply') </button>
        
                </div>
            </div>
        </form>
            {{-- {!! Form::close() !!} --}}

        </div>
    @endsection


   {{--  <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                <h3 class="box-title">@lang('modules.financeReport.chartTitle')</h3>
                <div id="morris-bar-chart"></div>
                <h6><span class="text-danger">@lang('app.note'):</span> @lang('modules.financeReport.noteText')
            </div>
        </div>

    </div> --}}

    <div class="row  m-t-30">
        <div class="white-box">
            <div class="table-responsive">
                 <label>  {{ __('app.incomings') }}, {{ __('app.outgoings') }}</label>
            <table class="table table-responsive">
                <thead>
                    <th>#</th>
                    <th>{{ __('app.type') }}</th>
                    <th>{{ __('app.amount') }}</th>
                    <th>{{ __('app.operation') }}</th>
                </thead>
                <tbody>
                    @php
                    $pp = App\Payment::where('paid_on','>=',$fromDate)->where('paid_on','<=',$toDate)->sum('amount');
                    $e = App\Expense::where('purchase_date','>=',$fromDate)->where('purchase_date','<=',$toDate)->sum('price');
                    $sp = App\purchase_material_payment::where('paid_on','>=',$fromDate)->where('paid_on','<=',$toDate)->sum('amount');
                    $tpp = App\ThirdPartyPayments::where('date','>=',$fromDate)->where('date','<=',$toDate)->sum('amount');
                    $sa = App\Payroll::where('paid_on','>=',$fromDate)->where('paid_on','<=',$toDate)->sum('total');
                    @endphp
                    <tr>
                        <td>1</td>
                        <td>{{ __('app.project payments') }}</td>
                        <td>{{ number_format($pp,2) }}</td>
                        <td><span class="badge round badge-success">+</span></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>{{ __('app.expenses') }}</td>
                        <td>{{ number_format($e,2) }}</td>
                        <td><span class="badge round badge-danger">-</span></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>{{ __('app.supplier payments') }}</td>
                        <td>{{ number_format($sp,2) }}</td>
                        <td><span class="badge round badge-danger">-</span></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>{{ __('app.third party payments') }}</td>
                        <td>{{ number_format($tpp,2) }}</td>
                        <td><span class="badge round badge-danger">-</span></td>
                    </tr> 
                    <tr>
                        <td>5</td>
                        <td>{{ __('app.salary') }}</td>
                        <td>{{ number_format($sa,2) }}</td>
                        <td><span class="badge round badge-danger">-</span></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>{{ __('app.total') }}</td>
                        <td><span class="badge badge-warning">{{ $pp-$e-$sp-$tpp-$sa }}</span></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <hr>
            <label>  {{ __('app.loans') }}</label>
            <table class="table table-responsive">
                <thead>
                    <th>#</th>
                    <th>{{ __('app.type') }}</th>
                    <th>{{ __('app.amount') }}</th>
                    {{-- <th>{{ __('app.operation') }}</th> --}}
                </thead>
                <tbody>
                    @php
                        $invoices = App\Invoice::select('*',DB::raw('(select sum(amount) from payments where payments.invoice_id=invoices.id ) as amount'))->get();
                        $purchases = App\purchase_material::select('*',DB::raw('(select sum(amount) from purchase_material_payments where purchase_material_payments.purchase_id=purchase_materials.id ) as amount'))->get();
                        $thirdparty_invs = App\Invoice::whereNotNull('thirdparty_id')->get();
                        $sal = App\Payroll::whereNull('paid_on')->sum('total');
                        $p=0;
                        foreach ($thirdparty_invs as $inv) {
                            $p+= $inv->payment->sum('amount')*($inv->ratio/100.0);
                        }
                    @endphp
                    <tr>
                        <td>1</td>
                        <td>{{ __('app.customers loan') }}</td>
                        <td>{{ $invoices->sum('total')-$invoices->sum('amount') }}</td>
                        {{-- <td>+</td> --}}
                    </tr>
                    <tr>
                        
                        <td>2</td>
                        <td>{{ __('app.suppliers loan') }}</td>
                        <td>{{ $purchases->sum('total')-$purchases->sum('amount') }}</td>
                        {{-- <td>-</td> --}}
                    </tr>
                   
                    <tr>
                        <td>3</td>
                        <td>{{ __('app.third party payments') }}</td>
                        <td>{{ $p }}</td>
                        {{-- <td>-</td> --}}
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>{{ __('app.salary loan') }}</td>
                        <td>{{ $sal }}</td>
                        {{-- <td>-</td> --}}
                    </tr>
                    <tr>
                        <td></td>
                        <td>{{ __('app.total') }}</td>
                        <td>{{ $sal+$p+($purchases->sum('total')-$purchases->sum('amount'))+($invoices->sum('total')-$invoices->sum('amount')) }}</td>
                        {{-- <td>-</td> --}}
                    </tr>
                    
                </tbody>
            </table>
            </div>
        </div>
    </div>

@endsection

@push('footer-script')


<script src="{{ asset('plugins/bower_components/raphael/raphael-min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/morrisjs/morris.js') }}"></script>

<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>



<script>
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });
    jQuery('#date-range').datepicker({
        toggleActive: true,
        format: '{{ $global->date_picker_format }}',
    });

    $('#payments-table').on('preXhr.dt', function (e, settings, data) {
        var startDate = $('#start-date').val();

        if (startDate == '') {
            startDate = null;
        }

        var endDate = $('#end-date').val();

        if (endDate == '') {
            endDate = null;
        }

        var status = $('#status').val();
        var project = $('#project').val();
        var client = $('#client').val();

        data['startDate'] = startDate;
        data['endDate'] = endDate;
        data['status'] = status;
        data['project'] = project;
        data['client'] = client;
    });
 
    $('#filter-results').click(function () {
        var token = '{{ csrf_token() }}';
        var url = '{{ route('admin.finance-report.store') }}';

        var startDate = $('#start-date').val();

        if (startDate == '') {
            startDate = null;
        }

        var endDate = $('#end-date').val();

        if (endDate == '') {
            endDate = null;
        }

        // var currencyId = $('#currency_id').val();

        // var project = $('#project').val();
        // var client = $('#client').val();

        $.easyAjax({
            type: 'POST',
            url: url,  
            data: {_token: token, startDate: startDate, endDate: endDate},
            success: function (response) {
                if(response.status == 'success'){
                    chartData = $.parseJSON(response.chartData);
                    // $('#morris-bar-chart').html('');
                    // barChart();
                    // window.LaravelDataTables["payments-table"].draw();         
                }
            }
        });
    })

    function loadTable(){
        window.LaravelDataTables["payments-table"].draw();
    }

</script>

<script>
   

</script>
@endpush