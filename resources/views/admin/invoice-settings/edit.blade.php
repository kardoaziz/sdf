@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('image-picker/image-picker.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/switchery/dist/switchery.min.css') }}">

@endpush

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">@lang('modules.invoiceSettings.updateTitle')</div>

                <div class="vtabs customvtab m-t-10">
                    @include('sections.admin_setting_menu')

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="white-box">

                                        {!! Form::open(['id'=>'editSettings','class'=>'ajax-form','method'=>'PUT']) !!}
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="invoice_prefix" class="required">@lang('modules.invoiceSettings.invoicePrefix')</label>
                                                    <input type="text" class="form-control" id="invoice_prefix" name="invoice_prefix"
                                                           value="{{ $invoiceSetting->invoice_prefix }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="invoice_prefix">@lang('modules.invoiceSettings.invoiceDigit')</label>
                                                    <input type="number" min="2" class="form-control" id="invoice_digit" name="invoice_digit"
                                                           value="{{ $invoiceSetting->invoice_digit }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="invoice_prefix">@lang('modules.invoiceSettings.invoiceLookLike')</label>
                                                    <input type="text" class="form-control" id="invoice_look_like" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="payment_prefix" class="required">@lang('modules.invoiceSettings.paymentPrefix')</label>
                                                    <input type="text" class="form-control" id="payment_prefix" name="payment_prefix"
                                                           value="{{ $invoiceSetting->payment_prefix }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="payment_prefix">@lang('modules.invoiceSettings.paymentDigit')</label>
                                                    <input type="number" min="2" class="form-control" id="payment_digit" name="payment_digit"
                                                           value="{{ $invoiceSetting->payment_digit }}">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="payment_prefix">@lang('modules.invoiceSettings.paymentLookLike')</label>
                                                    <input type="text" class="form-control" id="payment_look_like" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="contract_prefix" class="required">@lang('modules.invoiceSettings.contractPrefix')</label>
                                                    <input type="text" class="form-control" id="contract_prefix" name="contract_prefix"
                                                           value="{{ $invoiceSetting->contract_prefix }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="contract_prefix">@lang('modules.invoiceSettings.contractDigit')</label>
                                                    <input type="number" min="2" class="form-control" id="contract_digit" name="contract_digit"
                                                           value="{{ $invoiceSetting->contract_digit }}">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="contract_prefix">@lang('modules.invoiceSettings.contractLookLike')</label>
                                                    <input type="text" class="form-control" id="contract_look_like" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="thirdpart_prefix" class="required">@lang('modules.invoiceSettings.thirdpartyPrefix')</label>
                                                    <input type="text" class="form-control" id="thirdparty_prefix" name="thirdparty_prefix"
                                                           value="{{ $invoiceSetting->thirdparty_prefix }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="thirdparty_prefix">@lang('modules.invoiceSettings.thirdpartyDigit')</label>
                                                    <input type="number" min="2" class="form-control" id="thirdpart_digit" name="thirdparty_digit"
                                                           value="{{ $invoiceSetting->thirdparty_digit }}">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="thirdpart_prefix">@lang('modules.invoiceSettings.thirdpartyLookLike')</label>
                                                    <input type="text" class="form-control" id="thirdparty_look_like" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="estimate_prefix">@lang('modules.invoiceSettings.estimatePrefix')</label>
                                                    <input type="text" class="form-control" id="estimate_prefix" name="estimate_prefix"
                                                           value="{{ $invoiceSetting->estimate_prefix }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="estimate_digit">@lang('modules.invoiceSettings.estimateDigit')</label>
                                                    <input type="number" min="2" class="form-control" id="estimate_digit" name="estimate_digit"
                                                           value="{{ $invoiceSetting->estimate_digit }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="estimate_look_like">@lang('modules.invoiceSettings.estimateLookLike')</label>
                                                    <input type="text" class="form-control" id="estimate_look_like" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="credit_note_prefix">@lang('modules.invoiceSettings.credit_notePrefix')</label>
                                                    <input type="text" class="form-control" id="credit_note_prefix" name="credit_note_prefix"
                                                           value="{{ $invoiceSetting->credit_note_prefix }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="credit_note_digit">@lang('modules.invoiceSettings.credit_noteDigit')</label>
                                                    <input type="number" min="2" class="form-control" id="credit_note_digit" name="credit_note_digit"
                                                           value="{{ $invoiceSetting->credit_note_digit }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="credit_note_look_like">@lang('modules.invoiceSettings.credit_noteLookLike')</label>
                                                    <input type="text" class="form-control" id="credit_note_look_like" readonly>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12 " style="display: none;">
                                                <div class="form-group">
                                                    <label for="template" class="required">@lang('modules.invoiceSettings.template')</label>
                                                    <select name="template" class="image-picker show-labels show-html">
                                                        <option data-img-src="{{ asset('invoice-template/1.png') }}"
                                                                @if($invoiceSetting->template == 'invoice-1') selected @endif
                                                                value="invoice-1">Template
                                                            1
                                                        </option>
                                                        <option data-img-src="{{ asset('invoice-template/2.png') }}"
                                                                @if($invoiceSetting->template == 'invoice-2') selected @endif
                                                                value="invoice-2">Template
                                                            2
                                                        </option>
                                                        <option data-img-src="{{ asset('invoice-template/3.png') }}"
                                                                @if($invoiceSetting->template == 'invoice-3') selected @endif
                                                                value="invoice-3">Template
                                                            3
                                                        </option>
                                                        <option data-img-src="{{ asset('invoice-template/4.png') }}"
                                                                @if($invoiceSetting->template == 'invoice-4') selected @endif
                                                                value="invoice-4">Template
                                                            4
                                                        </option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-md-6" style="display: none;">
                                                <div class="form-group">
                                                    <label for="due_after" class="required">@lang('modules.invoiceSettings.dueAfter')</label>

                                                    <div class="input-group m-t-10">
                                                        <input type="number" id="due_after" name="due_after" class="form-control" value="{{ $invoiceSetting->due_after }}">
                                                        <span class="input-group-addon">@lang('app.days')</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8" style="display: none;">
                                                <div class="form-group">
                                                    <label for="gst_number">@lang('app.gstNumber')</label>
                                                    <input type="text" id="gst_number" name="gst_number" class="form-control" value="{{ $invoiceSetting->gst_number }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="display: none;">
                                                <div class="form-group">
                                                    <label class="control-label" >@lang('app.showGst')</label>
                                                    <div class="switchery-demo">
                                                        <input type="checkbox" name="show_gst" @if($invoiceSetting->show_gst == 'yes') checked @endif class="js-switch " data-color="#99d683"  />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="invoice_terms" class="required">@lang('modules.invoiceSettings.invoiceTerms')</label>
                            <textarea name="invoice_terms" id="invoice_terms" class="form-control"
                                      rows="4">{{ $invoiceSetting->invoice_terms }}</textarea>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                                                    @lang('app.update')
                                                </button>
                                                <button type="reset"
                                                        class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>
                                            </div>

                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
    <!-- .row -->



    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('image-picker/image-picker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>


<script>
    $(".image-picker").imagepicker();
    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function () {
        new Switchery($(this)[0], $(this).data());

    });
    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.invoice-settings.update', $invoiceSetting->id)}}',
            container: '#editSettings',
            type: "POST",
            redirect: true,
            data: $('#editSettings').serialize()
        })
    });

    $('#invoice_prefix, #invoice_digit, #estimate_prefix, #estimate_digit, #credit_note_prefix, #credit_note_digit').on('keyup', function () {
        genrateInvoiceNumber();
    });

    genrateInvoiceNumber();

    function genrateInvoiceNumber() {
        var invoicePrefix = $('#invoice_prefix').val();
        var invoiceDigit = $('#invoice_digit').val();
        var invoiceZero = '';
        for ($i=0; $i<invoiceDigit-1; $i++){
            invoiceZero = invoiceZero+'0';
        }
        invoiceZero = invoiceZero+'1';
        var invoice_no = invoicePrefix+'#'+invoiceZero;
        $('#invoice_look_like').val(invoice_no);

        var estimatePrefix = $('#estimate_prefix').val();
        var estimateDigit = $('#estimate_digit').val();
        var estimateZero = '';
        for ($i=0; $i<estimateDigit-1; $i++){
            estimateZero = estimateZero+'0';
        }
        estimateZero = estimateZero+'1';
        var estimate_no = estimatePrefix+'#'+estimateZero;
        $('#estimate_look_like').val(estimate_no);

        // var creditNotePrefix = $('#credit_note_prefix').val();
        // var creditNoteDigit = $('#credit_note_digit').val();
        // var creditNoteZero = '';
        // for ($i=0; $i<creditNoteDigit-1; $i++){
        //     creditNoteZero = creditNoteZero+'0';
        // }
        // creditNoteZero = creditNoteZero+'1';
        // var creditNote_no = creditNotePrefix+'#'+creditNoteZero;
        // $('#credit_note_look_like').val(creditNote_no);

         var paymentPrefix = $('#payment_prefix').val();
        var paymentDigit = $('#payment_digit').val();
        var paymentZero = '';
        for ($i=0; $i<paymentDigit-1; $i++){
            paymentZero = paymentZero+'0';
        }
        paymentZero = paymentZero+'1';
        var payment_no = paymentPrefix+'#'+paymentZero;
        $('#payment_look_like').val(payment_no);

        var contractPrefix = $('#contract_prefix').val();
        var contractDigit = $('#contract_digit').val();
        var contractZero = '';
        for ($i=0; $i<contractDigit-1; $i++){
            contractZero = contractZero+'0';
        }
        contractZero = contractZero+'1';
        var contract_no = contractPrefix+'#'+contractZero;
        $('#contract_look_like').val(contract_no);

        var thirdpartyPrefix = $('#thirdparty_prefix').val();
        var thirdpartyDigit = $('#thirdparty_digit').val();
        var thirdpartyZero = '';
        for ($i=0; $i<thirdpartyDigit-1; $i++){
            thirdpartyZero = thirdpartyZero+'0';
        }
        thirdpartyZero = thirdpartyZero+'1';
        var thirdparty_no = thirdpartyPrefix+'#'+thirdpartyZero;
        $('#thirdparty_look_like').val(thirdparty_no);
    }
</script>
@endpush

