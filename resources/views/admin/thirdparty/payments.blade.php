@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }} 
                {{ $thirdparty->name }}
                
            </h4>
             
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
            
            <a onclick="showAdd({{ $thirdparty->id }})" class="btn btn-outline btn-success btn-sm pull-right m-l-5">@lang('modules.thirdparty.addNew') <i class="fa fa-plus" aria-hidden="true"></i></a>

        </div>
        <!-- /.breadcrumb -->
    </div>
    <div class="raw">
        <div class="col col-md-12">
           {{--  {!! $dataTable->table(['class' => 'table table-bordered table-hover toggle-circle default footable-loaded footable']) !!} --}}
        </div>
        
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <style>

        .dashboard-stats .white-box .list-inline {
            margin-bottom: 0;
        }

        .dashboard-stats .white-box {
            padding: 10px;
        }

        .dashboard-stats .white-box .box-title {
            font-size: 13px;
            text-transform: capitalize;
            font-weight: 300;
        }
        #products-table_wrapper .dt-buttons{
            display: none !important;
        }
    </style>
@endpush

@section('content')
<table class="table table-bordered">
	<thead>
		<th>#</th>
		<th>Amount</th>
		<th>date</th>
		<th>note</th>
        <th>delete</th>
	</thead>
    <tbody>
        @foreach($payments as $p)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $p->amount }}</td>
            <td>{{ $p->date }}</td>
            <td>{{ $p->note }}</td>
            <td> <form action="{{ route('admin.thirdparty.delpayment',$p->id) }}" method="get">
                     {{ csrf_field() }}
                            {{-- <input type="hidden" name="_method" value="DELETE"  > --}}

                <a style="padding: 0px;
                  font-size: 12px;
                  color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                  <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                </a>
              </form></td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="modal fade bs-modal-md in" id="edit-column-form" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>

    {{-- {!! $dataTable->scripts() !!} --}}
    <script>
        function showAdd(id) {
            console.log("addddd");
            // var idd = id;
            // var url2 = "$"+idd;
            var url = "{{ route('admin.thirdparty.createpayment',2) }}";
            var res = url.replace("2", id);
            console.log(res);
            $.ajaxModal('#edit-column-form', res);
        }
        // 

        function exportData(){
            var url = '{{ route('admin.thirdparty.export') }}';
            window.location.href = url;
        }


    </script>
@endpush