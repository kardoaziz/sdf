@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i>{{ __('app.menu.invoices') }}                
            </h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        {{-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
            
            <a onclick="showAdd()" class="btn btn-outline btn-success btn-sm pull-right m-l-5">@lang('modules.accounts.addNewAccount') <i class="fa fa-plus" aria-hidden="true"></i></a>

        </div> --}}
        <!-- /.breadcrumb -->
    </div>
    <div class="raw">
        <div class="col col-md-12">
            {!! $dataTable->table(['class' => 'table table-bordered table-hover toggle-circle default footable-loaded footable','data-page-length'=>'25']) !!}
        </div>
        
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <style>

        .dashboard-stats .white-box .list-inline {
            margin-bottom: 0;
        }

        .dashboard-stats .white-box {
            padding: 10px;
        }

        .dashboard-stats .white-box .box-title {
            font-size: 13px;
            text-transform: capitalize;
            font-weight: 300;
        }
        #products-table_wrapper .dt-buttons{
            display: none !important;
        }
    </style>
@endpush

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="white-box">

                <div class="table-responsive">

                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>

    {!! $dataTable->scripts() !!}
    
@endpush