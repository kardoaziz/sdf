@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.products.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('app.update') @lang('app.menu.products')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('app.update') @lang('app.menu.products')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updateProduct','class'=>'ajax-form']) !!}
                        <input name="_method" value="PUT" type="hidden">
                        <div class="form-body">
                            <h3 class="box-title">@lang('app.menu.products') @lang('app.details')</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label required">@lang('app.name')</label>
                                        <input type="text" id="name" name="name" class="form-control" value="{{ $product->name }}">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label required">@lang('app.price')</label>
                                        <input type="text" id="price" name="price" class="form-control" value="{{ $product->price }}">
                                        <span class="help-block"> @lang('messages.productPrice')</span>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->

                            <div class="row">
                                {{-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.tax') <a href="javascript:;" id="tax-settings" ><i class="ti-settings text-info"></i></a></label>
                                        <select id="multiselect" name="tax[]"  multiple="multiple" class="selectpicker form-control type">
                                            @foreach($taxes as $tax)
                                                <option @if (isset($product->taxes) && array_search($tax->id, json_decode($product->taxes)) !== false)
                                                        selected
                                                        @endif
                                                        value="{{ $tax->id }}">{{ $tax->tax_name }}: {{ $tax->rate_percent }}%</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label required">@lang('app.description')</label>
                                        <textarea name="description" id="" cols="30" rows="4" class="form-control">{{ $product->description }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="required">@lang('app.type') </label>
                                   <select name="type" id="type" class="form-control">
                                       <option value="" @if($product->type=="") selected @endif>--</option>
                                       <option value="Rectangular" @if($product->type=="Rectangular") selected @endif>Rectangular</option>
                                       <option value="Oval" @if($product->type=="Oval") selected @endif>Oval</option>
                                       <option value="Circle" @if($product->type=="Circle") selected @endif>Circle</option>
                                   </select>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label required">@lang('app.code')</label>
                                        <input type="text" id="code" name="code" class="form-control" placeholder="A short Code ex: Rec_">
                                        <span class="help-block"> @lang('messages.code')</span>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">

                                        <div class="checkbox checkbox-info">
                                            <input id="purchase_allow" name="purchase_allow" value="no"
                                                   type="checkbox" @if($product->allow_purchase == 1) checked @endif>
                                            <label for="purchase_allow">@lang('app.purchaseAllow')</label>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="row">
                                    <div class="col-md-6">
                                    <label class="required">@lang('app.dimensions') </label>
                                  <div class="form-group">
                                  <div >
                                    <label>
                                      <input type="checkbox" name="dimensions[]" value="A1" @if(in_array('A1', $product->dimensions)) checked="" @endif>
                                      @lang('modules.invoices.A1')
                                    </label>
                                  </div>

                                  <div >
                                    <label>
                                      <input type="checkbox" name="dimensions[]" value="A2" @if(in_array('A2', $product->dimensions)) checked="" @endif>
                                      @lang('modules.invoices.A2')
                                    </label>
                                  </div>
                                  <div >
                                    <label>
                                      <input type="checkbox" name="dimensions[]" value="B1" @if(in_array('B1', $product->dimensions)) checked="" @endif>
                                      @lang('modules.invoices.B1')
                                    </label>
                                  </div>

                                  <div >
                                    <label>
                                      <input type="checkbox" name="dimensions[]" value="B2" @if(in_array('B2', $product->dimensions)) checked="" @endif>
                                     @lang('modules.invoices.B2')
                                    </label>
                                  </div>
                                  <div >
                                    <label>
                                      <input type="checkbox" name="dimensions[]" value="C1" @if(in_array('C1', $product->dimensions)) checked="" @endif>
                                      @lang('modules.invoices.C1')
                                    </label>
                                  </div>

                                  <div >
                                    <label>
                                      <input type="checkbox" name="dimensions[]" value="C2" @if(in_array('C2', $product->dimensions)) checked="" @endif>
                                     @lang('modules.invoices.C2')
                                    </label>
                                  </div>
                                  <div >
                                    <label>
                                      <input type="checkbox" name="dimensions[]" value="D1" @if(in_array('D1', $product->dimensions)) checked="" @endif>
                                      @lang('modules.invoices.D1')
                                    </label>
                                  </div>

                                  <div >
                                    <label>
                                      <input type="checkbox" name="dimensions[]" value="D2" @if(in_array('D2', $product->dimensions)) checked="" @endif>
                                      @lang('modules.invoices.D2')
                                    </label>
                                  </div>
                                  
                                </div>
                                </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                            <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->


    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taxModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('#tax-settings').on('click', function (event) {
            event.preventDefault();
            var url = '{{ route('admin.taxes.create')}}';
            $('#modelHeading').html('Manage Project Category');
            $.ajaxModal('#taxModal', url);
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.products.update', [$product->id])}}',
                container: '#updateProduct',
                type: "POST",
                redirect: true,
                data: $('#updateProduct').serialize()
            })
        });
    </script>
@endpush

