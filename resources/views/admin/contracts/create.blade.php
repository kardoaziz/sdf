@extends('layouts.app')
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
@endpush
@section('content_header')
    {{-- <h1>Invoices</h1> --}}
    <meta name="csrf-token" content="{{ Session::token() }}">
@stop
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.contracts.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@section('content')
@php 
ini_set('max_input_vars', 5000);
@endphp
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('app.add') @lang('app.menu.contract')</div>

                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        @if($clients->isEmpty())
                        <div class="text-center">

                            <div class="empty-space" style="height: 200px;">
                                <div class="empty-space-inner">
                                    <div class="icon" style="font-size:30px"><i
                                                class="fa fa-user-secret"></i>
                                    </div>
                                    <div class="title m-b-15">@lang('messages.noClientFound')</div>
                                    <div class="subtitle">
                                        <a href="{{ route('admin.clients.create') }}"
                                           class="btn btn-outline btn-success btn-sm">@lang('modules.client.addNewClient') <i class="fa fa-plus"
                                                                                                                              aria-hidden="true"></i></a>

                                    </div>
                                </div>
                            </div>

                        </div>
                        @else
                        {!! Form::open(['id'=>'createContract','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="company_name" class="required">@lang('app.client')</label>
                                    <div>
                                        <select class="select2 form-control" data-placeholder="@lang('app.client')" name="client" id="clientID" onchange="getEstimates(this.value)">
                                            <option value="-1">select client</option>
                                            @foreach($clients as $client)
                                                <option
                                                        value="{{ $client->id }}">{{ ucwords($client->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label for="company_name" class="required">@lang('app.project')</label>
                                    <div>
                                        <select class="select2 form-control" data-placeholder="@lang('app.client')" name="project_id" id="project_id" >
                                            <option value="-1">select project</option>
                                            @foreach(App\Project::all() as $client)
                                                <option style="color:black;"
                                                        value="{{ $client->id }}">{{ $client->project_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="company_name" class="required">@lang('app.estimate')</label>
                                    <div>
                                        <select class="select2 form-control" data-placeholder="@lang('app.estimate')" name="estimate_id" id="estimate_id">
                                            {{-- @foreach($clients as $client)
                                                <option
                                                        value="{{ $client->id }}">{{ ucwords($client->name) }}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required" for="subject">@lang('app.subject')</label>
                                    <input type="text" class="form-control" id="subject" name="subject">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required" for="subject">@lang('app.amount') ({{ $global->currency->currency_symbol }})</label>
                                    <input type="number" class="form-control" id="amount" name="amount">
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required" class="control-label">@lang('modules.contracts.contractType')
                                        <a href="javascript:;"
                                        id="createContractType"
                                        class="btn btn-xs btn-outline btn-success">
                                            <i class="fa fa-plus"></i> @lang('modules.contracts.addContractType')
                                        </a>
                                    </label>
                                    <div>
                                        <select class="select2 form-control" data-placeholder="@lang('app.client')" id="contractType" name="contract_type">
                                            @foreach($contractType as $type)
                                                <option
                                                        value="{{ $type->id }}">{{ ucwords($type->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required">@lang('modules.timeLogs.startDate')</label>
                                    <input id="start_date" name="start_date" type="text"
                                        class="form-control"
                                        value="{{ \Carbon\Carbon::today()->format($global->date_format) }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="required">@lang('modules.timeLogs.endDate')</label>
                                    <input id="end_date" name="end_date" type="text"
                                        class="form-control"
                                        value="{{ \Carbon\Carbon::today()->format($global->date_format) }}">
                                </div>
                            </div>
                             <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.file')</label>
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                    <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">@lang('app.selectFile')</span> <span class="fileinput-exists">@lang('app.change')</span>
                    <input type="file" name="file" id="file">
                    </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">@lang('app.remove')</a> </div>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="required">@lang('modules.contracts.notes')</label>
                                    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                                </div>
                            </div>
                             <div class="col-md-12">
                                    <p class="text-muted m-b-30 font-13"></p>
                                    <div class="form-group">
                                <textarea name="contract_detail" id="contract_detail"
                                          class="summernote"></textarea>
                                    </div>
                                    
                                </div>
                        </div>
                        <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                            @lang('app.save')
                        </button>
                        <button type="reset" class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>
                         </div>

                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#start_date, #end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: '{{ $global->date_picker_format }}',
        });
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.contracts.store')}}',
                container: '#createContract',
                type: "POST",
                redirect: true,
                file: (document.getElementById("file").files.length == 0) ? false : true,
                data: $('#createContract').serialize()
            })
        });
        $('#createContractType').click(function(){
            var url = '{{ route('admin.contract-type.create-contract-type')}}';
            $('#modelHeading').html("@lang('modules.contracts.manageContractType')");
            $.ajaxModal('#taskCategoryModal', url);
        })
        $('.summernote').summernote({
            height: 500,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            // toolbar: [
            //     // [groupName, [list of button]]
            //     ['style', ['bold', 'italic', 'underline', 'clear']],
            //     ['font', ['strikethrough']],
            //     ['fontsize', ['fontsize']],
            //     ['para', ['ul', 'ol', 'paragraph']],
            //     ["view", ["fullscreen"]]
            // ]
        });
        var HTMLstring = '<p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: center; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 24px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s1\" style=\"text-decoration-line: underline; font-kerning: none;\">گرێبەستی کار</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: center; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 24px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; min-height: 27px;\"><span class=\"s1\" style=\"text-decoration-line: underline; font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s1\" style=\"text-decoration-line: underline; font-kerning: none;\">پێشەکی:</span></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">لایەنی یەکەم و دووهەم هەردوولا ڕێکەوتن لەسەر ناوەڕۆکی ئەم گرێبەستەی خوارەوە بەجۆرێک کە لایەنی یەکەم<span class=\"Apple-converted-space\">&nbsp; </span>کە کارگەی SDF ە کەکاری دەکت بۆ لایەنی دووهەم کە خاوەن کارە بەڕێز ( <span class=\"Apple-converted-space\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span>) ە<span class=\"Apple-converted-space\">&nbsp; </span>ئەنجام دەدات بەگوێرەی ئەو بەندانەی لە دواتردا ئاماژەی پێدەدرێت و ئەم ڕێکەوتنە لەبەرواری ( <span class=\"Apple-converted-space\">&nbsp; &nbsp; </span>/<span class=\"Apple-converted-space\">&nbsp; &nbsp; &nbsp; </span>/ <span class=\"Apple-converted-space\">&nbsp; &nbsp; &nbsp; &nbsp; </span>) ئیمزا کرا لەئۆفیسی سەرەکی کارگەی SDF لە شاری سلێمانی-تانجەرۆ.ئەم پێشەکییەش بە بەشێکی دانەبڕاو لە گرێبەستەکە هەژمار دەکرێت.</span></p><p dir=\"rtl\" class=\"p5\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; min-height: 13px;\"><span class=\"s2\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p5\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; min-height: 13px;\"><span class=\"s2\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s1\" style=\"text-decoration-line: underline; font-kerning: none;\">بەندی یەکەم:ئەرکەکانی لایەنی یەکەم</span></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">١. لایەنی یەکەم کاری دیزاینی ئەندازیاری و دروستکردن و هەڵواسین و تێستکردن دەکت ئەنجام دەدات بۆ لایەنی دووهەم، بەهەموو بڕگەکانی کە تایبەتە بەدروستکردنی دەکت دەگرێتە ئەستۆ.</span></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٢. لایەنی یەکەم بەرپرسە لەهەموو ئەو پێداویستیانەی کە بۆ کارەکە پێویستە لە دیزاینەوە تاوەکو هەڵواسینی دەکتەکان.</span></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٣. لایەنی یەکەم پلێتی گەلفەنایز بەکاردەهێنێت لەکارەکانیدا و بەهیچ جۆرێک پلیتی خوار 0.60 بەکارناهێنێ لەناو کارەکانیدا</span></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٤. لایەنی یەکەم هەموو پێداویستییەکانی گواستنەوە و هەڵواسین دەگرێتە ئەستۆی لەباری ئاساییدا، کە پێویست بە گۆرانکاری نەکات لە نەخشەدا.</span></p><p dir=\"rtl\" class=\"p5\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; min-height: 13px;\"><span class=\"s2\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٥. هەموو هۆکارەکانی سەلامەتی ستافەکانی کارگەی SDF لە لایەن لایەنی یەکەمەوە دەستەبەر دەکرێت و دەگرێتە ئەستۆ.</span></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٦. لایەنی یەکەم بەرپرسە لەهەموو ئەو نرخانەی کە لەسەر بەرهەمەکانی خۆی جێگیری کردووە بۆماوەی تەنها ٢٤ کاتژمیر لەپێش ئیمزاکردنی گرێبەست.<span class=\"Apple-converted-space\">&nbsp;</span></span></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٧. پێویستە لایەنی یەکەم ڕێنمایی بدات بەلایەنی دوو</span><span class=\"s3\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: &quot;Times New Roman&quot;; font-kerning: none;\">‌</span><span class=\"s2\" style=\"font-kerning: none;\">هەم لەپێش دانانی دەکت هەموو رێنماییەکانی پێویست بەدانانی دەکت بدات بە خاوەن کار کە لایەنی دووهەمە.</span></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٨. لایەنی دووهەم ئەگەر نەخشەی دەکتی تایبەت بە بیناکەی خۆی هەبوو، ئەوا لایەنی یەکەم تەنها بەرپرسە لە نەبوونی لیک و گرنتی کردنی مەوادە بەکارهاتووەکان لە کارەکاندا.</span></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\"><br></span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s1\" style=\"text-decoration-line: underline; font-kerning: none;\">بەندی دووهەم:ئەرکەکانی لایەنی دووهەم</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">١. پێویستە لایەنی دووهەم نەخشەی تەواوی خانوو یاخوود بیناکەی خۆی بداتە لایەنی یەکەم تاوەکو هەڵەی تیدا نەبێت چوونکە کارەکانی کارگەی SDF پشت بە نەخشەی بیناکان دەبەستێت.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٢. هەر شکاندنێک پیوسیت بێت لەناو بیناکەدا پیویستە لایەنی دووهەم جێبەجێیان بکات بەڕێنمایی ستافەکانی لایەنی یەکەم.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٣. لایەنی دووهەم پێویستە کارەبای پێویست بۆ کارکردنی ستافەکانی کارگەی SDF دابین بکات.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٤. ئەو کراوانەی دەکتیان پیدا دەڕوات و بەرزیان لە ٥ مەتر زیاتر بوو، پێویستە لایەنی دووهەم ئەسکەلەی پێویست ببەستێت بۆ کارکردنی لایەنی یەکەم.</span></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; min-height: 13px;\"><span class=\"s2\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; min-height: 13px;\"><span class=\"s2\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s1\" style=\"text-decoration-line: underline; font-kerning: none;\">بەندی سێهەم:شێوازی زەرعەکردن</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">١. لایەنی یەکەم دەکتی گەلڤەنایز لە پلێتی گەلڤەنایز بۆ لایەنی دووهەم دروست دەکات و زەرعەکەی بە مەترە دووجای پلێت دەبێت بەجۆریک کۆی لاکانی دەکتەکە لیکدانی درێژی دەکتەکە دەکرێت.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٢. لایەکانی دەکتەکان ئەوەی لە واقیعدا پێوانە دەکرێت لەکۆی گشتی ٤٠ ملم واتە ٤ سم کەمترە کە لە زەرعەدا پێویستە دابنرێت.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٣. لەباری درێژی دەکتەکان بۆ هەر پارچە دەکتێک کە هەردووسەری فلنجە بێت ٦٣ملم بۆ هەر لایەکی زیاتر پلێتی تێدا بەکاردێت کە دوولامان هەیە و دەکاتە ١٢٦ ملم کە دەکاتە ١٢.٦سم پێویستە لە زەرعەدا دابنرێت.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٤. خەسارەی دروستکردنی دەکتەکان لەسەر لایەنی یەکەمە کەکارگەی SDF<span class=\"Apple-converted-space\">&nbsp; </span>ە.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٥. زەرعەی هەموو پارچەکانی دەکت ئەندازیاریانەیە و هیچ خەسارەیەک ناکەوێتە ئەستۆی لایەنی دووهەم.</span></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; min-height: 13px;\"><span class=\"s2\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; min-height: 13px;\"><span class=\"s2\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s1\" style=\"text-decoration-line: underline; font-kerning: none;\">بەندی چوارەم:کوالیتی</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">١. لایەنی یەکەم دەکت بە ئامێری ئۆتۆماتیکی دروست دەکات و بەهیچ جۆرێک کاری دەستی تیادا بەکارناهێنیت تەنها دەستی کار بۆ بەکارهێنانی ئامێرەکان و گواستنەوەی پارچەکان بۆلای ئامێرەکان بەکاردەهێنێت.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٢. کارەکانی کارگە لەکەمترین ماوەدا دروستدەکرێت.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٣. دەکتەکانی کارگەکە بە فلنجە دەبەسترێنەوە بەیەکەوە لەگەل بەکارهێنانی قەفیز و برغو سەموونە و لەگەل بەکارهێنانی گاسکێت بۆ ئەوەی بەهیچ جۆرێک لیکی هەوا ڕوونەدات لەکاتی ڕۆشتنی هەوا بەناو دەکتەکاندا.</span></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\"></span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s2\" style=\"font-kerning: none;\">٤. کارگەکەمان لە دروستکردن و دامەزراندنی دەکتەکان پشت بە ستانداردی جیهانی SMACNA کە کورتکراوەی </span><span class=\"s3\" style=\"font-kerning: none; color: rgb(26, 26, 26);\">Sheet Metal and Air Conditioning Contractors\' National Association<span class=\"Apple-converted-space\">&nbsp; </span>ە.</span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s1\" style=\"font-kerning: none;\">٥. تێستکردنی دەکتەکان بە ئامێری پێشکەوتو و فلۆومیتەری تایبەت ئەنجام دەدرین بۆ دڵنیابوونی خاوەن کار لە دروستی ئەو کارەی بۆی ئەنجام دراوە.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 16px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s2\" style=\"text-decoration-line: underline; font-kerning: none;\">بەندی پێنجەم:گرنتی</span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s1\" style=\"font-kerning: none;\">کارگەکەمان گرنتی یەک ساڵ بۆ هەر کارێکی دەکت دەکات لە دیزاینەوە تاوەکو تێست کردن و بە هەموو ئەو مەوادانەی لە سیستمەکەماندا بەکارهاتووە.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s2\" style=\"text-decoration-line: underline; font-kerning: none;\">بەندی شەشەم:پێوانەکردن</span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s1\" style=\"font-kerning: none;\">١. لایەنی یەکەم دیزاینی دەکت بۆ لایەنی دووهەم دەکات و لەسەر ئەو بنەمایە پێوانەی دەکتەکە دیاریدەکات و کە بەشێوەیەکی گشتی بەنزیکییە و دواتر دوای دروستکردن و کۆتایی هاتنی کارەکانی هەڵواسین کۆتا زەرعە دەکرێتەوە بۆ دڵنیابوونی هەردوولا.</span></p><p dir=\"rtl\" class=\"p5\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s3\" style=\"font-kerning: none;\">٢. لەحاڵەتی پەشیمان بوونەوەی لایەنی دووهەم لەدوای دیزاینکردن، پێویستە لایەنی دووهەم بڕێک پارە بدات بەلایەنی یەکەم لەبەرامبەر دیزاینکردنی دەکت کە بۆی کراوە، ئەوبڕەش لەلایەن بەشی دیزاینی کارگەی SDF ەوە دیاریدەکرێت.</span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s1\" style=\"font-kerning: none;\">٣. گرێبەستەکە پشت بەستوو بە بەندی شەشەم دەنوسرێت.</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s2\" style=\"text-decoration-line: underline; font-kerning: none;\">بەندی حەوتەم:شێوازی پارەدان</span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s1\" style=\"font-kerning: none;\">١. لەگەڵ ئیمزاکردنی گرێبەست پیویستە لایەنی دووهەم پێویستە ٥٠٪ پەنجا لە سەدی تێچووی ئەو پێوانەکردن و خەمڵاندنەی کە لایەنی یەکەم پێشکەش بەلایەنی دووهەمی دەکات لەدیزاینەکەدا و بەپشت بەستن بە بەندی شەشەم.</span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s1\" style=\"font-kerning: none;\">٢. لەدوای تەواوبوونی کارەکانی دروستکردنی دەکت بۆ لایەنی دووهەم،پێش بردنەدەرەوەی دەکتەکان لەکارگە، پێوویستە لایەنی دووهەم ٥٠٪ کۆی گشتی پێوانەکردن و خەمڵاندن پێشکەش بەلایەنی یەکەم بکات</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s1\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s2\" style=\"text-decoration-line: underline; font-kerning: none;\">هەشتەم:نێوەندگیری</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;;\"><span class=\"s3\" style=\"font-kerning: none; color: rgb(26, 26, 26);\"></span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s1\" style=\"font-kerning: none;\">لەکاتی بوونی هەر ناڕوونییەک لە گرێبەست دا، یاخوود دروستبوونی هەر کێشەیەک لەنێوان لایەنی یەکەم و دووهەم دادگای سلیمانی شوێنی یەکلاکردنەوەی کێشەکانە.</span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s1\" style=\"font-kerning: none;\"><br></span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s1\" style=\"text-decoration-line: underline; font-kerning: none;\">بەندی دەیەم: لایەنەکانی گریبەست و بەرواری گرێبەست</span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s2\" style=\"font-kerning: none;\">ئەم گریبەستە واژۆکرا لەنێوان لایەنی یەکەم کە کارگەی SDF ە یە و لایەنی دووهەم بەڕێز (<span class=\"Apple-converted-space\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span>) لەبەرواری ( <span class=\"Apple-converted-space\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span>/<span class=\"Apple-converted-space\">&nbsp; &nbsp; &nbsp; &nbsp; </span>/<span class=\"Apple-converted-space\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span>)لەشاری سلێمانی لە ئۆفیسی سەرەکی کارگەی SDF لە تانجەرۆ.</span></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p3\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26); min-height: 13px;\"><span class=\"s3\" style=\"font-kerning: none;\"></span><br></p><p dir=\"rtl\" class=\"p4\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s3\" style=\"font-kerning: none;\">لایەنی یەکەم<span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span> <span class=\"Apple-converted-space\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span>لایەنی دووهەم</span></p><p dir=\"rtl\" class=\"p1\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s1\" style=\"font-kerning: none;\"></span></p><p dir=\"rtl\" class=\"p2\" style=\"margin-bottom: 8px; text-align: justify; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: &quot;K24 Kurdish Bold&quot;; color: rgb(26, 26, 26);\"><span class=\"s2\" style=\"font-kerning: none;\">کارگەی SDF<span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span><span class=\"Apple-tab-span\" style=\"white-space: pre;\">  </span></span></p>';
        $('.summernote').summernote('editor.pasteHTML', HTMLstring);

    </script>
    <script type="text/javascript">
        function getEstimates(id) {
            var url = "{!! url('/') !!}";
            console.log("id");
           $.get(url+'/admin/estimates/clientestimates/'+id, {}, function (data) {
        //    success:function(data){
            console.log(JSON.parse(data));
            data = JSON.parse(data);
             $('#estimate_id').empty();
            // $('#test-img-ol').empty();
           
             for(var i=0; i<data.length; i++){
                $('#estimate_id').append('<option value="'+data[i]['id']+'">'+data[i]['original_estimate_number']+' - valid: '+data[i]['valid_date']+' total : '+data[i]['total']+'</option>');
             }
            
        //    }
        });
        }
    </script>
    {{-- <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
         
          CKEDITOR.replace( 'description' , {language: 'ku' ,
          uiColor : '#9AB8F3',
          toolbar :
          [
              { name: 'basicstyles', items : [ 'Bold','Italic' ] },
              { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
              { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
              { name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
              { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote' ] },
              { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
              { name: 'tools', items : [ 'Maximize','-','About' ] }
          ]
        });
      </script> --}}
@endpush

