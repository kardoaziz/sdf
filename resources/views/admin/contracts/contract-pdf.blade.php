<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Contract # {{ $contract->id }}</title>
    <style>

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #0087C3;
            text-decoration: none;
        }

        body {
            /*direction: rtl;*/
            position: relative;
            width: 21cm;
            height: auto;
            margin-right: auto;
            margin-left: auto;
            margin-top: 20px;
            margin-bottom: 7cm;
            color: #555555;
            font-size: 14px;
            font-family: 'DejaVu Sans', sans-serif;
            background-repeat: repeat-y;
            background-size: 20cm;*/
            -webkit-print-color-adjust: exact !important;
            color-adjust: exact !important;  
            /*background-position: center;*/
        }
        @media print {
             * {-webkit-print-color-adjust:exact;}
body {
    width: 21cm;
            height: auto;
            margin-right: auto;
            margin-left: auto;
            margin-top: 20px;
            margin-bottom: 6cm;
            color: #555555;
            font-size: 14px;
            font-family: 'DejaVu Sans', sans-serif;
           
            -webkit-print-color-adjust: exact !important;
            color-adjust: exact !important; 
            /*background-position: initial; */
  }
}

        h2 {
            font-weight:normal;
        }

        header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        #logo {
            float: left;
            margin-top: 11px;
        }

        #logo img {
            height: 55px;
            margin-bottom: 15px;
        }

        #company {

        }

        #details {
            margin-bottom: 50px;
        }

        #client {
            padding-left: 6px;
            float: left;
        }

        #client .to {
            color: #777777;
        }

        h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
        }

        #invoice {

        }

        #invoice h1 {
            color: #0087C3;
            font-size: 2.4em;
            line-height: 1em;
            font-weight: normal;
            margin: 0 0 10px 0;
        }

        #invoice .date {
            font-size: 1.1em;
            color: #777777;
        }

        table {
            width: 100%;
            border-spacing: 0;
            margin-bottom: 20px;
        }

        table th,
        table td {
            padding: 5px 10px 7px 10px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
        }

        table td {
            text-align: right;
        }

        table td h3 {
            color: #767676;
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 0 0;
        }

        table .no {
            color: #FFFFFF;
            font-size: 1.6em;
            background: #767676;
            width: 10%;
        }

        table .desc {
            text-align: left;
        }

        table .unit {
            background: #DDDDDD;
        }


        table .total {
            background: #767676;
            color: #FFFFFF;
        }

        table td.unit,
        table td.qty,
        table td.total
        {
            font-size: 1.2em;
            text-align: center;
        }

        table td.unit{
            width: 35%;
        }

        table td.desc{
            width: 45%;
        }

        table td.qty{
            width: 5%;
        }

        .status {
            margin-top: 15px;
            padding: 1px 8px 5px;
            font-size: 1.3em;
            width: 80px;
            color: #fff;
            float: right;
            text-align: center;
            display: inline-block;
        }

        .status.unpaid {
            background-color: #E7505A;
        }
        .status.paid {
            background-color: #26C281;
        }
        .status.cancelled {
            background-color: #95A5A6;
        }
        .status.error {
            background-color: #F4D03F;
        }

        table tr.tax .desc {
            text-align: right;
            color: #1BA39C;
        }
        table tr.discount .desc {
            text-align: right;
            color: #E43A45;
        }
        table tr.subtotal .desc {
            text-align: right;
            color: #1d0707;
        }
        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            padding: 10px 10px 20px 10px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-bottom: 1px solid #AAAAAA;
        }

        table tfoot tr:first-child td {
            border-top: none;
        }

        table tfoot tr td:first-child {
            border: none;
        }

        #thanks {
            font-size: 2em;
            margin-bottom: 50px;
        }

        #notices {
            padding-left: 6px;
            border-left: 6px solid #0087C3;
        }

        #notices .notice {
            font-size: 1.2em;
        }

        footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
        }

        table.billing td {
            background-color: #fff;
        }

        table td div#invoiced_to {
            text-align: left;
        }

        #notes{
            color: #767676;
            font-size: 11px;
        }
       
div.footer {
    display: none; text-align: center;
    bottom:0px;
    position: fixed;
    padding:0px;
}

@media print {
    div.footer {
        display: block; text-align: center;
        bottom:3cm;
        position: fixed;
        padding:0px;
        z-index: -1;
        height:0px !important;
        page-break-before: always;
    }}
    </style>
</head>
<body>

<main>
    <img style="position: fixed; top: 30; left: 30; z-index:-1;width:225px; height:200px;opacity: 0.5;" src="{{ asset('img/watermark_header.jpeg') }}">
    <div id="details" class="clearfix" style="margin-bottom:5cm;">
        {!! $contract->contract_detail !!}
    </div>
    <div class='footer'>
        <img style="padding:0px;height:3cm;width:100%;" src="{{ asset('img/watermark_footer.jpeg') }}">
    </div>
    {{-- {{ $contract->estimate }} --}}

    @if($contract->signature)
        <div style="text-align: right;">
            <h2 class="name" style="margin-bottom: 20px;">Signature (Customer)</h2>
            {!!  Html::image($contract->signature->signature,'Logo',['class'=>'','width'=>'250px']) !!}
        </div>
    @endif
</main>
</body>
</html>