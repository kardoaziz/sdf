@extends('layouts.app') 
@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li><a href="{{ route('admin.accounts.index') }}">{{ $pageTitle }}</a></li>
            <li class="active">@lang('app.edit')</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection
 @push('head-script')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">

<link rel="stylesheet" href="{{ asset('plugins/bower_components/ion-rangeslider/css/ion.rangeSlider.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/ion-rangeslider/css/ion.rangeSlider.skinModern.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<style>
    .panel-black .panel-heading a,
    .panel-inverse .panel-heading a {
        color: unset!important;
    }
</style>




@endpush 
@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-inverse">
            <div class="panel-heading"> @lang('modules.accounts.updateTitle')</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    {!! Form::open(['id'=>'updateAccount','class'=>'ajax-form','method'=>'PUT']) !!}
                    <div class="form-body ">
                        {{-- <h3 class="box-title m-b-10">@lang('modules.warehouse.projectInfo')</h3> --}}
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>@lang('modules.accounts.accountName')</label>
                                    <input type="text" name="name" id="name" class="form-control" value="{{ $account->name }}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>@lang('modules.accounts.account_no')</label>
                                    <input type="text" name="account_no" id="account_no" class="form-control" value="{{ $account->account_no }}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>@lang('modules.accounts.initial_balance')</label>
                                    <input type="text" name="initial_balance" id="initial_balance" class="form-control" value="{{ $account->initial_balance }}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>@lang('modules.accounts.note')</label>
                                    <input type="text" name="note" id="note" class="form-control" value="{{ $account->note }}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label style="margin-right:20px;">@lang('modules.accounts.is default')</label>
                                    <input type="checkbox" @if($account->is_default==1) checked @endif class="default" data-id="{{ $account->id}}" data-toggle="toggle"  data-onstyle="success" data-offstyle="danger" name="is_default" value="1">
                                </div>
                            </div>
                            {{--  --}}
                            <div class="col-md-5">
                                        <div class="panel panel-inverse">
                                            <div class="panel-heading">@lang('modules.projects.addMemberTitle')</div>

                                            <div class="panel-wrapper collapse in">
                                                <div class="panel-body">
                                                {!! Form::open(['id'=>'createMembers','class'=>'ajax-form','method'=>'POST']) !!}

                                                <div class="form-body">

                                                    {!! Form::hidden('account_id', $account->id) !!}

                                                    <div class="form-group" id="user_id">
                                                        <select class="select2 m-b-10 select2-multiple " multiple="multiple"
                                                                data-placeholder="Choose Members" name="users[]">
                                                            @foreach(App\EmployeeDetails::all() as $emp)
                                                                <option value="{{ $emp->user->id }}">{{ ucwords($emp->user->name) }} @if($emp->user->id == $user->id)
                                                                        (YOU) @endif</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    {{-- <div class="form-actions">
                                                        <button type="submit" id="save-members" class="btn btn-success"><i
                                                                    class="fa fa-check"></i> @lang('app.save')
                                                        </button>
                                                    </div> --}}
                                                </div>

                                                {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            {{--  --}}
                            <div class="col-md-5">
<div class="panel panel-default">
<div class="panel-wrapper collapse in">
<div class="panel-body">
    <table class="table">
        <thead>
            <tr>
                <th>@lang('app.name')</th>
                {{-- <th>@lang('app.role')</th> --}}
                <th>@lang('app.action')</th>
            </tr>
        </thead>
        <tbody>
        @forelse(App\AccountMembers::where('account_id',$account->id)->get() as $member)
            <tr>
                <td>
                    <div class="row">

                        <div class="col-sm-3 col-xs-4">
                            {!!  ($member->user->image) ? '<img src="'.asset_url('avatar/'.$member->user->image).'"
                            alt="user" class="img-circle" width="40">' : '<img src="'.asset('img/default-profile-2.png').'"
                            alt="user" class="img-circle" width="40">' !!}

                        </div>
                        <div class="col-sm-9 col-xs-8">
                                {{ ucwords($member->user->name) }}<br>

                                <span class="text-muted font-12">{{ (!is_null($member->user->employeeDetail) && !is_null($member->user->employeeDetail->designation)) ? ucwords($member->user->employeeDetail->designation->name) : ' ' }}</span>

                        </div>
                    </div>
                </td>
                {{-- <td>
                    <div class="radio radio-info">
                        <input type="radio" name="project_admin" class="assign_role" id="project_admin_{{ $member->user->id }}" value="{{ $member->user->id }}"
                        @if($member->user->id == $project->project_admin) checked @endif
                        >
                        <label for="project_admin_{{ $member->user->id }}"> Project Admin </label>
                    </div>
                </td> --}}
                <td><a href="javascript:;" data-member-id="{{ $member->id }}" class="btn btn-sm btn-danger btn-outline delete-members"><i class="fa fa-trash"></i></a></td>
            </tr>
        @empty
            <tr>
                <td>
                    @lang('messages.noMemberAddedToProject')
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
</div>
</div>
</div>

                            <!--/span-->
                        </div>
                </div>
                <div class="form-actions m-t-15">
                    <button type="submit" id="save-form" class="btn btn-success"><i
                                        class="fa fa-check"></i> @lang('app.update')</button>
                    <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>
<!-- .row -->

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="projectCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
{{--Ajax Modal Ends--}}
@endsection
 @push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js') }}"></script>

<script>
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });
    

  

    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.accounts.update', [$account->id])}}',
            container: '#updateAccount',
            type: "POST",
            redirect: true,
            data: $('#updateAccount').serialize(),
             success: function (response) {
            console.log(response);
        }
        })
    });

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    
   

    

    $(':reset').on('click', function(evt) {
        evt.preventDefault()
        $form = $(evt.target).closest('form')
        $form[0].reset()
        $form.find('select').select2()
    });

  $('body').on('click', '.delete-members', function(){
    var id = $(this).data('member-id');
    console.log('member id');
    console.log(id);
    swal({
        title: "Are you sure?",
        text: "This will remove the member from the project.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes!",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm){
        if (isConfirm) {

            var url = "{{ route('admin.accounts.destroymember',':id') }}";
            url = url.replace(':id', id);
            console.log(url);

            var token = "{{ csrf_token() }}";

            $.easyAjax({
                type: 'GET',
                url: url,
                data: {'_token': token, '_method': 'DELETE'},
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                        window.location.reload();
                    }
                }
            });
        }
    });
});

</script>
@endpush