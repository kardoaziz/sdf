@extends('layouts.app') 
@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li><a href="{{ route('admin.transfers.index') }}">{{ $pageTitle }}</a></li>
            <li class="active">@lang('app.edit')</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection
 @push('head-script')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">

<link rel="stylesheet" href="{{ asset('plugins/bower_components/ion-rangeslider/css/ion.rangeSlider.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/ion-rangeslider/css/ion.rangeSlider.skinModern.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<style>
    .panel-black .panel-heading a,
    .panel-inverse .panel-heading a {
        color: unset!important;
    }
</style>




@endpush 
@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-inverse">
            <div class="panel-heading"> @lang('modules.transfers.updateTitle')</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    {!! Form::open(['id'=>'updateItem','class'=>'ajax-form','method'=>'PUT']) !!}
                    <div class="form-body ">
  
                        <div class="row">
                            <div class="col-md-5">
                    <div id="accountBox" class="form-group ">
                        <label>@lang('modules.transfers.sender_account')</label>
                        <select name="account">
                            @foreach(App\Accounts::join('account_members','account_members.account_id','accounts.id')->where('account_members.user_id',Auth::user()->id)->select('accounts.id','accounts.name')->get() as $acc)
                            <option value="{{ $acc->id }}" @if($acc->id == $transfer->account_id) selected @endif>{{ $acc->name }}</option>
                            @endforeach    
                        </select>
                        <div id="errorAccount"></div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div id="account_recBox" class="form-group ">
                        <label>@lang('modules.transfers.reciever_account')</label>
                        <select name="account_id_rec">
                            @foreach(App\Accounts::all() as $acc)
                            <option value="{{ $acc->id }}" @if($acc->id == $transfer->account_id_rec) selected @endif>{{ $acc->name }}</option>
                            @endforeach    
                        </select>
                        <div id="errorAccountRec"></div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div id="amountBox" class="form-group ">
                        <input class="form-control " autocomplete="off" id="amount" name="amount" type="number"  placeholder="Amount" value="{{ $transfer->amount }}"/>
                        <div id="errorAmount"></div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div id="dateBox" class="form-group ">
                        <input class="form-control " autocomplete="off" id="date" name="date" type="date"  placeholder="Date" value="{{ $transfer->date }}"/>
                        <div id="errorDate"></div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div id="noteBox" class="form-group ">
                        <input class="form-control " autocomplete="off" id="note" name="note" type="text"  placeholder="note" value="{{ $transfer->note }}"/>
                        <div id="errorNote"></div>
                    </div>
                </div>
                            
                            <!--/span-->
                        </div>
                </div>
                <div class="form-actions m-t-15">
                    <button type="submit" id="save-form" class="btn btn-success"><i
                                        class="fa fa-check"></i> @lang('app.update')</button>
                    <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>
<!-- .row -->

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="projectCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
{{--Ajax Modal Ends--}}
@endsection
 @push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js') }}"></script>

<script>
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });
    

  

    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.transfers.update', [$transfer->id])}}',
            container: '#updateItem',
            type: "POST",
            redirect: true,
            data: $('#updateItem').serialize()
        })
    });

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    
   

    

    $(':reset').on('click', function(evt) {
        evt.preventDefault()
        $form = $(evt.target).closest('form')
        $form[0].reset()
        $form.find('select').select2()
    });

  

</script>
@endpush