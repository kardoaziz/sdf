@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.invoices.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
{{--<link rel="stylesheet" href="{{ asset('css/bootstrap-multiselect.css') }}" type="text/css"/>--}}
<style>
    .dropdown-content {
        width: 250px;
        max-height: 250px;
        overflow-y: scroll;
        overflow-x: hidden;
    }
    .multiselect-container li input[type=checkbox] {
        opacity: 1 !important;
    }
</style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.invoices.addInvoice')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-2">

                                    <div class="form-group">
                                        <label class="control-label">@lang('app.invoice') #</label>
                                        <div>
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="invoicePrefix" data-prefix="{{ $invoiceSetting->invoice_prefix }}">{{ $invoiceSetting->invoice_prefix }}</span>#<span class="noOfZero" data-zero="{{ $invoiceSetting->invoice_digit }}">{{ $zero }}</span></div>
                                                <input type="text"  class="form-control readonly-background" readonly name="invoice_number" id="invoice_number" value="@if(is_null($lastInvoice))1 @else{{ ($lastInvoice) }}@endif">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form-group" >
                                        <label class="control-label">@lang('app.project')</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select class="select2 form-control" onchange="getCompanyName()" data-placeholder="Choose Project" name="project_id" id="project_id">
                                                    <option value="">--</option>
                                                    @foreach($projects as $project)
                                                        <option value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" id="selected_project" name="selected_project" value="-1">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.company_name')</label>
                                        <div class="row">
                                            <div class="col-md-12" id="client_company_div">
                                                <div class="input-icon">
                                                    <input type="text" readonly class="form-control" name="" id="company_name" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 ">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.menu.third party')
                                           
                                        </label>
                                        <select class="selectpicker form-control" name="thirdparty_id" id="thirdparty_id"
                                                data-style="form-control">
                                                <option value="">@lang('messages.noProjectThirdPartyAdded')</option>
                                            @forelse(App\ThirdParty::all() as $c)
                                                <option value="{{ $c->id }}">{{ ucwords($c->name) }}</option>
                                            @empty
                                                <option value="">@lang('messages.noProjectThirdPartyAdded')</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.menu.third party ratio')
                                           
                                        </label>
                                       <input type="number" step="any" class="form-control" name="ratio" id="ratio" value="0">
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group" >
                                        <label class="control-label required">@lang('modules.invoices.invoiceDate')</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control" name="issue_date" id="invoice_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label required">@lang('app.dueDate')</label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="due_date" id="due_date" value="{{ Carbon\Carbon::today()->addDays($invoiceSetting->due_after)->format($global->date_format) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.currency')</label>
                                        <select class="form-control" name="currency_id" id="currency_id">
                                            @foreach($currencies as $currency)
                                                <option value="{{ $currency->id }}">{{ $currency->currency_symbol.' ('.$currency->currency_code.')' }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.transporter')</label>
                                        <select class="form-control" name="id_user_transporter" id="id_user_transporter">
                                            @foreach(App\User::join('employee_details','employee_details.user_id','users.id')->select('users.id','users.name')->get() as $u)
                                                <option value="{{ $u->id }}">{{ $u->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="row">
                                <div class="col-md-3">

                                    <div class="form-group" style="display: none;">
                                        <label class="control-label">@lang('modules.invoices.isRecurringPayment') </label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select class="form-control" name="recurring_payment" id="recurring_payment" onchange="recurringPayment()">
                                                    <option value="no">@lang('app.no')</option>
                                                    <option value="yes">@lang('app.yes')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 recurringPayment" style="display: none;">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.billingFrequency')</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select class="form-control" name="billing_frequency" id="billing_frequency">
                                                    <option value="day">@lang('app.day')</option>
                                                    <option value="week">@lang('app.week')</option>
                                                    <option value="month">@lang('app.month')</option>
                                                    <option value="year">@lang('app.year')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 recurringPayment" style="display: none;">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.billingInterval')</label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="billing_interval" id="billing_interval" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 recurringPayment" style="display: none;">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.billingCycle')</label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="billing_cycle" id="billing_cycle" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group m-b-10">
                                        <button aria-expanded="false" data-toggle="dropdown" class="btn btn-info dropdown-toggle waves-effect waves-light" type="button">@lang('app.menu.products') <span class="caret"></span></button>
                                        <ul role="menu" class="dropdown-menu dropdown-content">
                                            @foreach($products as $product)
                                                <li class="m-b-10">
                                                    <div class="row m-t-10">
                                                        <div class="col-md-6" style="padding-left: 30px">
                                                            {{ $product->name }} - {{ $product->type }}
                                                        </div>
                                                        <div class="col-md-6" style="text-align: right;padding-right: 30px;">
                                                            <a href="javascript:;" data-pk="{{ $product->id }}" class="btn btn-success btn btn-outline btn-xs waves-effect add-product">@lang('app.add') <i class="fa fa-plus" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12  visible-md visible-lg">
                                    <div class="col-md-2 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.item')
                                    </div>
                                    <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.code')
                                    </div>
                                    <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.qty')
                                    </div>
                                     <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.unit')
                                    </div>
                                    <div class="col-sm-1 font-bold" style="padding: 8px 15px">
                                            @lang('modules.invoices.A1')
                                        </div>
                                        <div class="col-sm-1 font-bold" style="padding: 8px 15px">
                                            @lang('modules.invoices.A2')
                                        </div>
                                        <div class="col-sm-1 font-bold" style="padding: 8px 15px">
                                            @lang('modules.invoices.B1')
                                        </div>
                                        <div class="col-sm-1 font-bold" style="padding: 8px 15px">
                                            @lang('modules.invoices.B2')
                                        </div>
                                        <div class="col-sm-1 font-bold" style="padding: 8px 15px">
                                            @lang('modules.invoices.C1')
                                        </div>
                                        <div class="col-sm-1 font-bold" style="padding: 8px 15px">
                                            @lang('modules.invoices.C2')
                                        </div>
                                        <div class="col-sm-1 font-bold" style="padding: 8px 15px">
                                            @lang('modules.invoices.D1')
                                        </div>
                                        <div class="col-sm-1 font-bold" style="padding: 8px 15px">
                                            @lang('modules.invoices.D2')
                                        </div>
                                    <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.unitPrice')
                                    </div>
                                    {{-- <div class="col-md-2 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.tax') <a href="javascript:;" id="tax-settings" ><i class="ti-settings text-info"></i></a>
                                    </div> --}}
                                    <div class="col-md-1 text-center font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.amount')
                                    </div>
                                    <div class="col-md-1" style="padding: 8px 15px">
                                        &nbsp;
                                    </div>
                                </div>
                                <div id="sortable">
                                    {{-- <div class="col-xs-12 item-row margin-top-5">
                                        <div class="col-md-2">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                                                        <input type="text" class="form-control item_name" name="item_name[]">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <textarea name="item_summary[]" class="form-control" placeholder="@lang('app.description')" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.code')</label>
                                                <input type="text"  class="form-control"  name="codes[]" readonly="">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>
                                                <input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.A1')</label>
                                                <input type="number" min="1" class="form-control " value="1" name="A1[]" >
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.A2')</label>
                                                <input type="number" min="1" class="form-control " value="1" name="A2[]" >
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.B1')</label>
                                                <input type="number" min="1" class="form-control " value="1" name="B1[]" >
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.B2')</label>
                                                <input type="number" min="1" class="form-control " value="1" name="B2[]" >
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.C1')</label>
                                                <input type="number" min="1" class="form-control " value="1" name="C1[]" >
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.C2')</label>
                                                <input type="number" min="1" class="form-control " value="1" name="C2[]" >
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.D1')</label>
                                                <input type="number" min="1" class="form-control " value="1" name="D1[]" >
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.D2')</label>
                                                <input type="number" min="1" class="form-control " value="1" name="D2[]" >
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-1">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>
                                                    <input type="text"  class="form-control cost_per_item" name="cost_per_item[]" value="0" >
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-2 border-dark  text-center">
                                            <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>
                                            <p class="form-control-static"><span class="amount-html">0.00</span></p>
                                            <input type="hidden" class="amount" name="amount[]" value="0">
                                        </div>
                                        <div class="col-md-1 text-right visible-md visible-lg">
                                            <button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>
                                        </div>
                                        <div class="col-md-1 hidden-md hidden-lg">
                                            <div class="row">
                                                <button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i> @lang('app.remove')</button>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                                {{-- <div class="col-xs-12 m-t-5">
                                    <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i> @lang('modules.invoices.addItem')</button>
                                </div> --}}
                               
                                 <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group m-b-10">
                                        <button aria-expanded="false" data-toggle="dropdown" class="btn btn-danger dropdown-toggle waves-effect waves-light" type="button">@lang('app.menu.returned') <span class="caret"></span></button>
                                        <ul role="menu" class="dropdown-menu dropdown-content">
                                            @foreach($products as $product)
                                                <li class="m-b-10">
                                                    <div class="row m-t-10">
                                                        <div class="col-md-6" style="padding-left: 30px">
                                                            {{ $product->name }} - {{ $product->type }}
                                                        </div>
                                                        <div class="col-md-6" style="text-align: right;padding-right: 30px;">
                                                            <a href="javascript:;" data-rpk="{{ $product->id }}" class="btn btn-success btn btn-outline btn-xs waves-effect add-returned-product">@lang('app.add') <i class="fa fa-download" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="sortable3">
                                
                            </div>
     <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group m-b-10">
                                            <button aria-expanded="false" data-toggle="dropdown" class="btn btn-info dropdown-toggle waves-effect waves-light" type="button">@lang('app.menu.materials') <span class="caret"></span></button>
                                            <ul role="menu" class="dropdown-menu dropdown-content">
                                                @foreach($rowMaterial as $material)
                                                    <li class="m-b-10">
                                                        <div class="row m-t-10">
                                                            <div class="col-md-6" style="padding-left: 30px">
                                                                {{ $material->name }}
                                                            </div>
                                                            <div class="col-md-6" style="text-align: right;padding-right: 30px;">
                                                                <a href="javascript:;" data-pk="{{ $material->id }}" class="btn btn-success btn btn-outline btn-xs waves-effect add-material">@lang('app.add') <i class="fa fa-plus" aria-hidden="true"></i></a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
    
                                    <div class="col-xs-12  visible-md visible-lg">
    
                                            <div class="col-sm-2 font-bold" style="padding: 8px 15px">
                                                @lang('modules.invoices.item')
                                            </div>
                                            <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                                @lang('modules.invoices.qty')
                                            </div>
                                            <div class="col-sm-1 font-bold" style="padding: 8px 15px">
                                                @lang('modules.invoices.price')
                                            </div>
        
    
                                            <div class="col-md-1 text-center font-bold" style="padding: 8px 15px">
                                                @lang('modules.invoices.amount')
                                            </div>
        
                                            <div class="col-md-1" style="padding: 8px 15px">
                                                &nbsp;
                                            </div>
    
                                    </div>
    
                                    <div id="sortable2">
                                    
                                    </div>
    
                                    <div id="item-list">
    
                                    </div>
    
                                    {{-- <div class="col-xs-12 m-t-5">
                                        <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i> @lang('modules.invoices.addItem')</button>
                                    </div> --}}
    
                                    <div class="col-xs-12 ">
                                        <div class="row">
                                            <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10" >@lang('modules.invoices.subTotal')</div>
    
                                            <p class="form-control-static col-xs-6 col-md-2" >
                                                <span class="sub-total">0.00</span>
                                            </p>
    
    
                                            <input type="hidden" class="sub-total-field" name="sub_total" value="0">
                                        </div>
    
                                        {{-- <div class="row" style="display:none">
                                            <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                                @lang('modules.invoices.discount')
                                            </div>
                                            <div class="form-group col-xs-6 col-md-1" >
                                                <input type="number" min="0" value="0" name="discount_value" class="form-control discount_value">
                                            </div>
                                            <div class="form-group col-xs-6 col-md-1" >
                                                <select class="form-control" name="discount_type" id="discount_type">
                                                    <option value="percent">%</option>
                                                    <option value="fixed">@lang('modules.invoices.amount')</option>
                                                </select>
                                            </div>
                                        </div> --}}
    
                                        <div class="row m-t-5" id="invoice-taxes" style="display:none">
                                            <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                                @lang('modules.invoices.tax')
                                            </div>
    
                                            <p class="form-control-static col-xs-6 col-md-2" >
                                                <span class="tax-percent">0.00</span>
                                            </p>
                                        </div>
    <div class="row m-t-5 font-bold">
                                            <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10" >@lang('app.menu.returned')</div>
    
                                            <p class="form-control-static col-xs-6 col-md-2" >
                                                <span class="total_r">0.00</span>
                                            </p>
    
    
                                            <input type="hidden" class="total_r-field" name="total_r" value="0">
                                        </div>
                                        <div class="row m-t-5 font-bold">
                                            <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10" >@lang('modules.invoices.total')</div>
    
                                            <p class="form-control-static col-xs-6 col-md-2" >
                                                <span class="total">0.00</span>
                                            </p>
    
    
                                            <input type="hidden" class="total-field" name="total" value="0">
                                        </div>
                                         
        
                                    </div>
    
                                </div>
                                <div class="col-xs-12 ">

                                 {{--    <div class="row">
                                        <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10" >@lang('modules.invoices.subTotal')</div>
                                        <p class="form-control-static col-xs-6 col-md-2" >
                                            <span class="sub-total">0.00</span>
                                        </p>
                                        <input type="hidden" class="sub-total-field" name="sub_total" value="0">
                                    </div> --}}

                                    <div class="row">
                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                            @lang('modules.invoices.discount')
                                        </div>
                                        <div class="form-group col-xs-6 col-md-1" >
                                            <input type="number" min="0" value="0" name="discount_value" class="form-control discount_value">
                                        </div>
                                        <div class="form-group col-xs-6 col-md-1" >
                                            <select class="form-control" name="discount_type" id="discount_type">
                                                <option value="percent">%</option>
                                                <option value="fixed">@lang('modules.invoices.amount')</option>
                                            </select>
                                        </div>
                                    </div>

                                   {{--  <div class="row m-t-5" id="invoice-taxes">
                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                            @lang('modules.invoices.tax')
                                        </div>
                                        <p class="form-control-static col-xs-6 col-md-2" >
                                            <span class="tax-percent">0.00</span>
                                        </p>
                                    </div> --}}

                                    <div class="row m-t-5 font-bold">
                                        <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10" >@lang('modules.invoices.total')</div>
                                        <p class="form-control-static col-xs-6 col-md-2" >
                                            <span class="total">0.00</span>
                                        </p>
                                        <input type="hidden" class="total-field" name="total" value="0">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group" >
                                    <label class="control-label">@lang('app.note')</label>
                                    <textarea class="form-control" name="note" id="note" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions" style="margin-top: 70px">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->


    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taxModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('js/bootstrap-multiselect.js') }}"></script>--}}

<script>
    $(function () {
        $( "#sortable" ).sortable();
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    jQuery('#invoice_date, #due_date').datepicker({
        format: '{{ $global->date_picker_format }}',
        autoclose: true,
        todayHighlight: true
    });

    getCompanyName();

    function getCompanyName(){
        var projectID = $('#project_id').val();
        $('#selected_project').val(projectID);
        var url2 = "{{ route('admin.thirdparty.getProjectthirdparty',':id') }}";
        url2 = url2.replace(':id', projectID); 
        $.ajax({
            type: 'GET',
            url: url2,
            success: function (data) {
                console.log(data);
                if(data)
                {
                    console.log(data);
                  // $('#thirdparty_id').attr('data-id',data) 
                  $('select[name=thirdparty_id]').val(data);
                  $('.selectpicker').selectpicker('refresh');
                  $('#thirdparty_id').attr('disabled', true);
                }
                else{
                $('#thirdparty_id').attr('disabled', false); 
                }
            }
        });
        var url = "{{ route('admin.all-invoices.get-client-company') }}";
        if(projectID != '')
        {
            url = "{{ route('admin.all-invoices.get-client-company',':id') }}";
            url = url.replace(':id', projectID);
        }

        console.log(url);
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                $('#client_company_div').html(data.html);

            }
        });
    }

    $('#save-form').click(function(){
        calculateTotal();

        var discount = $('.discount-amount').html();
        var total = $('.total-field').val();

        if(parseFloat(discount) > parseFloat(total)){
            $.toast({
                heading: 'Error',
                text: 'Discount cannot be more than total amount.',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
            return false;
        }

        $.easyAjax({
            url:'{{route('admin.all-invoices.store')}}',
            container:'#storePayments',
            type: "POST",
            redirect: true,
            data:$('#storePayments').serialize()
        })
    });
    $(document).on('click', '#add-item', function () {
        var i = $(document).find('.item_name').length;
        var item = '<div class="col-xs-12 item-row margin-top-5">'

            +'<div class="col-md-4">'
            +'<div class="row">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>'
            +'<div class="input-group">'
            +'<div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>'
            +'<input type="text" class="form-control item_name" name="item_name[]" >'
            +'</div>'
            +'</div>'

            +'<div class="form-group">'
            +'<textarea name="item_summary[]" class="form-control" placeholder="@lang('app.description')" rows="2"></textarea>'
            +'</div>'

            +'</div>'

            +'</div>'

            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.code')</label>'
            +'<input type="text"  class="form-control " value="item" name="codes[]" >'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>'
            +'<input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unit')</label>'
            +'<select>\
            <option value="square meter">m<sup>2</sup></option>\
            </select>'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.A1')</label>'
            +'<input type="number" min="1" class="form-control" value="1" name="A1[]" >'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.A2')</label>'
            +'<input type="number" min="1" class="form-control" value="1" name="A2[]" >'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.B1')</label>'
            +'<input type="number" min="1" class="form-control" value="1" name="B1[]" >'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.B2')</label>'
            +'<input type="number" min="1" class="form-control" value="1" name="B2[]" >'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.C1')</label>'
            +'<input type="number" min="1" class="form-control" value="1" name="C1[]" >'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.C2')</label>'
            +'<input type="number" min="1" class="form-control" value="1" name="C2[]" >'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.D1')</label>'
            +'<input type="number" min="1" class="form-control" value="1" name="D1[]" >'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.D2')</label>'
            +'<input type="number" min="1" class="form-control" value="1" name="D2[]" >'
            +'</div>'
            +'</div>'
            
            +'<div class="col-md-1">'
            +'<div class="row">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>'
            +'<input type="text" min="0" class="form-control cost_per_item" value="0" name="cost_per_item[]">'
            +'</div>'
            +'</div>'

            +'</div>'


            

            +'<div class="col-md-1 text-center">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>'
            +'<p class="form-control-static"><span class="amount-html">0.00</span></p>'
            +'<input type="hidden" class="amount" name="amount[]">'
            +'</div>'

            +'<div class="col-md-1 text-right visible-md visible-lg">'
            +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'

            +'<div class="col-md-1 hidden-md hidden-lg">'
            +'<div class="row">'
            +'<button type="button" class="btn remove-item btn-danger"><i class="fa fa-remove"></i> @lang('app.remove')</button>'
            +'</div>'
            +'</div>'

            +'</div>';

        $(item).hide().appendTo("#sortable").fadeIn(500);
        $('#multiselect'+i).selectpicker();
        // i++;
    });



    $('#storePayments').on('click','.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function() {
            $(this).remove();
            $('select.customSequence').each(function(index){
                $(this).attr('name', 'taxes['+index+'][]');
                $(this).attr('id', 'multiselect'+index+'');
            });
            calculateTotal();
        });
    });

    $('#storePayments').on('keyup','.quantity,.cost_per_item,.item_name, .discount_value', function () {
        console.log("here");
        var quantity = $(this).closest('.item-row').find('.quantity').val();
        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();
        var amount = (quantity*perItemCost);

        $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();
    });
    $('#storePayments').on('keyup','.quantity_r,.cost_per_item_r,.item_name_r, .discount_value_r', function () {
        console.log("here<--");
        var quantity = $(this).closest('.item_r-row').find('.quantity_r').val();
        var perItemCost = $(this).closest('.item_r-row').find('.cost_per_item_r').val();
        var amount = (quantity*perItemCost);
        console.log("qty:"+quantity);
        console.log("cost:"+perItemCost);
        console.log("amount:"+amount);
        $(this).closest('.item_r-row').find('.amount_r').val(decimalupto2(amount));
        $(this).closest('.item_r-row').find('.amount-html_r').html(decimalupto2(amount));

        calculateTotal();
    });

    $('#storePayments').on('change','.type, #discount_type', function () {
        var quantity = $(this).closest('.item-row').find('.quantity').val();
        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();
        var amount = (quantity*perItemCost);

        $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();
    });

    function calculateTotalm(){
        var amount = $(this).find('.mamount').val();
        console.log(amount);
    }

    function calculateTotal()
    {
        var subtotal = 0;
        var subtotal_r = 0;
        var discount = 0;
        var tax = '';
        var taxList = new Object();
        var taxTotal = 0;
        $(".quantity").each(function (index, element) {
            var itemTax = [];
            var itemTaxName = [];
            $(this).closest('.item-row').find('select.type option:selected').each(function (index) {
                itemTax[index] = $(this).data('rate');
                itemTaxName[index] = $(this).text();
            });
            var itemTaxId = $(this).closest('.item-row').find('select.type').val();
            var amount = $(this).closest('.item-row').find('.amount').val();
            subtotal = parseFloat(subtotal)+parseFloat(amount);

            if(itemTaxId != ''){
                for(var i = 0; i<=itemTaxName.length; i++)
                {
                    if(typeof (taxList[itemTaxName[i]]) === 'undefined'){
                        taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                    }
                    else{
                        taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                    }
                }
            }
        });
         $(".quantity_r").each(function (index, element) {
            console.log("index: "+index);
            var itemTax = [];
            var itemTaxName = [];
            $(this).closest('.item_r-row').find('select.type option:selected').each(function (index) {
                itemTax[index] = $(this).data('rate');
                itemTaxName[index] = $(this).text();
            });
            var itemTaxId = $(this).closest('.item_r-row').find('select.type').val();
            var amount = $(this).closest('.item_r-row').find('.amount_r').val();
            subtotal_r = parseFloat(subtotal_r)+parseFloat(amount);
            console.log("tax: "+itemTaxId);
            console.log("amount_r: "+amount);
            console.log("subtotal_r: "+subtotal_r);
            if(itemTaxId != ''){
                for(var i = 0; i<=itemTaxName.length; i++)
                {
                    if(typeof (taxList[itemTaxName[i]]) === 'undefined'){
                        taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                    }
                    else{
                        taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                    }
                }
            }
        });

        $.each( taxList, function( key, value ) {
            if(!isNaN(value)){
                tax = tax+'<div class="col-md-offset-8 col-md-2 text-right p-t-10">'
                    +key
                    +'</div>'
                    +'<p class="form-control-static col-xs-6 col-md-2" >'
                    +'<span class="tax-percent">'+decimalupto2(value)+'</span>'
                    +'</p>';
                taxTotal = taxTotal+decimalupto2(value);
            }
        });

        $('.sub-total').html(decimalupto2(subtotal));
        $('.total_r').html(decimalupto2(subtotal_r));
        $('.sub-total-field').val(decimalupto2(subtotal));
        $('.total_r-field').val(decimalupto2(subtotal_r));

        var discountType = $('#discount_type').val();
        var discountValue = $('.discount_value').val();

        if(discountValue != ''){
            if(discountType == 'percent'){
                discount = ((parseFloat(subtotal)/100)*parseFloat(discountValue));
            }
            else{
                discount = parseFloat(discountValue);
            }

        }

//       show tax
        $('#invoice-taxes').html(tax);

//      calculate total
        var totalAfterDiscount = decimalupto2(subtotal-subtotal_r-discount);
        totalAfterDiscount = (totalAfterDiscount < 0) ? 0 : totalAfterDiscount;
        var total = decimalupto2(totalAfterDiscount+taxTotal);

        $('.total').html(total);
        $('.total-field').val(total);
    }

    function recurringPayment() {
        var recurring = $('#recurring_payment').val();

        if(recurring == 'yes')
        {
            $('.recurringPayment').show().fadeIn(300);
        } else {
            $('.recurringPayment').hide().fadeOut(300);
        }
    }

    $('#tax-settings').click(function () {
        var url = '{{ route('admin.taxes.create')}}';
        $('#modelHeading').html('Manage Project Category');
        $.ajaxModal('#taxModal', url);
    });

    function decimalupto2(num) {
        var amt =  Math.round(num * 100) / 100;
        return parseFloat(amt.toFixed(2));
    }

    $('.add-product').on('click', function(event) {
        event.preventDefault();
        var id = $(this).data('pk');
        $.easyAjax({
            url:'{{ route('admin.all-invoices.update-item') }}',
            type: "GET",
            data: { id: id },
            success: function(response) {
                $(response.view).hide().appendTo("#sortable").fadeIn(500);
                var noOfRows = $(document).find('#sortable .item-row').length;
                var i = $(document).find('.item_name').length-1;
                var itemRow = $(document).find('#sortable .item-row:nth-child('+noOfRows+') select.type');
                // itemRow.attr('id', 'multiselect'+i);
                // itemRow.attr('name', 'taxes['+i+'][]');
                // $(document).find('#multiselect'+i).selectpicker();

            }
        });
    });
      $('.add-returned-product').on('click', function(event) {
        event.preventDefault();
        var id = $(this).data('rpk');
        console.log(id);
        $.easyAjax({
            url:'{{ route('admin.all-invoices.update-returned-item') }}',
            type: "GET",
            data: { id: id },
            success: function(response) {
                console.log(response);
                $(response.view).hide().appendTo("#sortable3").fadeIn(500);
                var noOfRRows = $(document).find('#sortable3 .item-row').length;
                var i = $(document).find('.item_name').length-1;
                var itemRRow = $(document).find('#sortable3 .item-row:nth-child('+noOfRRows+') select.type');

            }
        });
    });
    $('.add-material').on('click', function(event) {
        event.preventDefault();
        var id = $(this).data('pk');
        $.easyAjax({
            url:'{{ route('admin.all-invoices.addMaterialsToInvoice') }}',
            type: "GET",
            data: { id: id },
            success: function(response) {
                $(response.view).hide().appendTo("#sortable2").fadeIn(500);
                var noOfRows = $(document).find('#sortable2 .material-row').length;
                var i = $(document).find('.item_name').length-1;
                var itemRow = $(document).find('#sortable2 .material-row:nth-child('+noOfRows+') select.type');
                // itemRow.attr('id', 'multiselect'+i);
                // itemRow.attr('name', 'taxes['+i+'][]');
                // $(document).find('#multiselect'+i).selectpicker();

            }
        });
    });
</script>
@endpush