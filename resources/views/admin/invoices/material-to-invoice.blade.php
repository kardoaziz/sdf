<div class="col-xs-12 item-row margin-top-5">
    <div class="col-md-2">
        <div class="row">
            <div class="form-group">
                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>
                <div class="input-group">
                    <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                    <input type="text" class="form-control material_name" name="material_name[]"
                           value="{{ $items->name }}" >
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-1">
        <div class="form-group">
            <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>
            <input type="number" min="1" class="form-control mquantity" data-item-id="{{ $items->id }}" value="1" name="mquantity[]" >
        </div>
    </div>

    <div class="col-md-1">
        <div class="row">
            <div class="form-group">
                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>
                <input type="text"  class="form-control mcost_per_item" name="mcost_per_item[]" data-item-id="{{ $items->id }}" value="{{ $items->price }}">
            </div>
        </div>
    </div>

    <div class="col-md-1 border-dark  text-center">
        <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>

        <p class="form-control-static"><span class="mamount-html" data-item-id="{{ $items->id }}">0</span></p>
        <input type="hidden" class="mamount" name="mamount[]" data-item-id="{{ $items->id }}">
    </div>

    <div class="col-md-1 text-right visible-md visible-lg">
        <button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>
    </div>

    <div class="col-xs-12 text-center hidden-md hidden-lg">
        <div class="row">
            <button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>
        </div>
    </div>

    <script>
        $(function () {
            var mquantity = $('#sortable2').find('.mquantity[data-item-id="{{ $items->id }}"]').val();
            var mperItemCost = $('#sortable2').find('.mcost_per_item[data-item-id="{{ $items->id }}"]').val();
            var mamount = (mquantity*mperItemCost);
            $('#sortable2').find('.mamount[data-item-id="{{ $items->id }}"]').val(mamount);
            $('#sortable2').find('.mamount-html[data-item-id="{{ $items->id }}"]').html(mamount);

            calculateTotalm();
        });
    </script>
</div>
