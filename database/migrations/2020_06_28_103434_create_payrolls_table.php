<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrolls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->date('month');
            $table->integer('no_work_day')->nullable();
            $table->decimal('no_work_hours',10,2);
            $table->integer('absence_day')->default(0);
            $table->decimal('extra_hour',10,2)->default(0);
            $table->decimal('extra_hour_rate',10,2)->default(0);
            $table->decimal('hour_rate',10,2)->default(0);
            $table->float('gift',12,2)->default(0);
            $table->float('penalty',12,2)->default(0);
            $table->float('total',12,2)->default(0);
            $table->integer('paid')->default(0);
            $table->date('paid_on')->nullable();
            $table->string('note')->nullable();  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
}
