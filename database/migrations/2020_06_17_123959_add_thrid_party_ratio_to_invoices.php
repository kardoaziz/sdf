<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddThridPartyRatioToInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            //
            $table->decimal('ratio',8,2)->default(0);
        });
        Schema::table('estimates', function (Blueprint $table) {
            //
            $table->decimal('ratio',8,2)->default(0);
        });
        Schema::table('projects', function (Blueprint $table) {
            //
            $table->decimal('ratio',8,2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            //
        });
    }
}
