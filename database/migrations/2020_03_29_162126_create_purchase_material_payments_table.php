<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseMaterialPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_material_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purchase_id')->unsigned();
            $table->integer('currency_id')->unsigned();
            $table->double('amount');
            $table->string('gateway');
            $table->string('remarks')->nullable();
            $table->string('transaction_id')->unique()->nullable();
            $table->enum('status', ['complete', 'pending'])->default('pending');
            $table->dateTime('paid_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_material_payments');
    }
}
