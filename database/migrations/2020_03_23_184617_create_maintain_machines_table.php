<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateMaintainMachinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintain_machines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("machine_id");
            $table->integer("part_id");
            $table->integer("qty")->default(1);
            $table->date("date")->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string("note")->nullable();
            $table->string("status")->nullable();
            $table->date("schedule")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintain_machines');
    }
}
