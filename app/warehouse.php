<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class warehouse extends BaseModel
{
    // 
        protected $fillable = ['name', 'location'];
  public function purchases()
    {
        return $this->hasMany(PurchaseItems::class,"warehouse_id");
    }

}
