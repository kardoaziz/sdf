<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintainMachine extends Model
{
    //
    protected $fillable = ['machine_id','date','part_id','note','qty','status','schedule'];
     public function machine()
    {
        return $this->belongsTo(Machine::class,"machine_id");
    }
    public function part()
    {
        return $this->belongsTo(Parts::class,"part_id");
    }
}
