<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchase_material extends BaseModel
{

    public function material_items() {
        return $this->hasMany(purchase_material_item::class, 'estimate_id');
    }

    public function currency(){
        return $this->belongsTo(Currency::class, 'currency_id');
    }
    public function account()
    {
        return $this->belongsTo(Accounts::class, 'account_id');
    }
    public function supplier(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
    public function payment()
    {
        return $this->hasMany(purchase_material_payment::class, 'purchase_id')->orderBy('paid_on', 'desc');
    }
    public function amountDue()
    {
        return $this->total - ($this->amountPaid());
    }

    public function amountPaid()
    {
        return $this->payment->sum('amount');
    }

    public function getPaidAmount()
    {
        return purchase_material_payment::where('purchase_id', $this->id)->sum('amount');
    }

}
