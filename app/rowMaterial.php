<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rowMaterial extends BaseModel
{
    protected $table = 'row_materials';

    protected $fillable = ['id','name', 'price','code','description'];

    public function amounts()
    {
        return $this->hasMany(purchase_material_item::class, 'item_name')->sum('quantity');
    }
}
