<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThirdPartyPayments extends Model
{
    //
    protected $appends = ['invoice_number'];

    public function getInvoiceNumberAttribute($value)
    {
        // if (!is_null($value)) {
            $invoiceSettings = InvoiceSetting::select('thirdparty_prefix', 'thirdparty_digit')->first();
            $zero = '';
            if (strlen($value) < $invoiceSettings->thirdparty_digit) {
                for ($i = 0; $i < $invoiceSettings->thirdparty_digit - strlen($value); $i++) {
                    $zero = '0' . $zero;
                }
            }
            $zero = $invoiceSettings->thirdparty_prefix . '#' . $zero . $this->attributes['id'];
            return $zero;
        // }
        // return "";
    }
}
