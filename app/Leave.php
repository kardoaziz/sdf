<?php

namespace App;

use App\Observers\LeaveObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Leave extends BaseModel
{
    protected $dates = ['leave_date'];
    protected $appends = ['date'];

    protected static function boot()
    {
        parent::boot();
        static::observe(LeaveObserver::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withoutGlobalScopes(['active']);
    }

    public function type()
    {
        return $this->belongsTo(LeaveType::class, 'leave_type_id');
    }

    public function getDateAttribute()
    {
        return $this->leave_date->toDateString();
    }

    public function getLeavesTakenCountAttribute()
    {
        $userId = $this->user_id;
        $setting = Setting::first();
        $user = User::withoutGlobalScope('active')->findOrFail($userId);

        if ($setting->leaves_start_from == 'joining_date') {
            return Leave::where('user_id', $userId)
                ->where('leave_date', '<=', $user->employee[0]->joining_date->format((Carbon::now()->year + 1) . '-m-d'))
                ->where('status', 'approved')
                ->count();
        } else {
            return Leave::where('user_id', $userId)
                ->where('leave_date', '<=', Carbon::today()->endOfYear()->format('Y-m-d'))
                ->where('status', 'approved')
                ->count();
        }

    }
     public static function byUserType($userId,$type=null)
    {
        $setting = Setting::first();
        $user = User::withoutGlobalScope('active')->findOrFail($userId);
        // if(empty($type)){
        if ($setting->leaves_start_from == 'joining_date') {
            return Leave::where('user_id', $userId)
                ->where('leave_date', '<=', $user->employee[0]->joining_date->format((Carbon::now()->year + 1) . '-m-d'))
                ->where('status', 'approved')
                ->where('duration', 'hours')
                ->sum('hours');
        } else {
            return Leave::where('user_id', $userId)
                ->where('leave_date', '<=', Carbon::today()->endOfYear()->format('Y-m-d'))
                ->where('status', 'approved')
                ->where('duration', 'hours')
                ->sum('hours');
        }
    // }
}
    public static function byUser($userId,$type=null)
    {
        $setting = Setting::first();
        $user = User::withoutGlobalScope('active')->findOrFail($userId);
        if(empty($type)){
        if ($setting->leaves_start_from == 'joining_date') {
            return Leave::where('user_id', $userId)
                ->where('leave_date', '<=', $user->employee[0]->joining_date->format((Carbon::now()->year + 1) . '-m-d'))
                ->where('status', 'approved')
                ->get();
        } else {
            return Leave::where('user_id', $userId)
                ->where('leave_date', '<=', Carbon::today()->endOfYear()->format('Y-m-d'))
                ->where('status', 'approved')
                ->get();
        }
    }
    else{
         if ($setting->leaves_start_from == 'joining_date') {
            return Leave::where('user_id', $userId)
                ->where('leave_date', '<=', $user->employee[0]->joining_date->format((Carbon::now()->year + 1) . '-m-d'))
                ->where('status', 'approved')
                ->where('duration','<>','hours')
                ->get();
        } else {
            return Leave::where('user_id', $userId)
                ->where('leave_date', '<=', Carbon::today()->endOfYear()->format('Y-m-d'))
                ->where('status', 'approved')
                ->where('duration','<>','hours')
                ->get();
        }
    }
    }
}
