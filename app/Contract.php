<?php

namespace App;

use App\Observers\ContractObserver;

class Contract extends BaseModel
{
    protected $dates = [
        'start_date',
        'end_date'
    ];
    protected $appends = ['invoice_number'];

    protected static function boot()
    {
        parent::boot();

        static::observe(ContractObserver::class);

    }
    public function client()
    {
        return $this->belongsTo(User::class, 'client_id')->withoutGlobalScopes(['active']);
    }
     public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function contract_type()
    {
        return $this->belongsTo(ContractType::class, 'contract_type_id');
    }

    public function signature()
    {
        return $this->hasOne(ContractSign::class, 'contract_id');
    }

    public function discussion()
    {
        return $this->hasMany(ContractDiscussion::class);
    }

    public function renew_history()
    {
        return $this->hasMany(ContractRenew::class, 'contract_id');
    }
    public function estimate()
    {
        return $this->belongsTo(Estimate::class, 'estimate_id');
    }

    public function getInvoiceNumberAttribute($value)
    {
        // if (!is_null($value)) {
            $invoiceSettings = InvoiceSetting::select('contract_prefix', 'contract_digit')->first();
            $zero = '';
            if (strlen($value) < $invoiceSettings->contract_digit) {
                for ($i = 0; $i < $invoiceSettings->contract_digit - strlen($value); $i++) {
                    $zero = '0' . $zero;
                }
            }
            $zero = $invoiceSettings->contract_prefix . '#' . $zero . $this->attributes['id'];
            return $zero;
        // }
        // return "";
    }
}
