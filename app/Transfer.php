<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    //
    protected $fillable = ['account_id', 'account_id_rec','amount','date','status','note'];
    public function account()
    {
        return $this->belongsTo(Accounts::class, 'account_id');
    }
    public function reciever()
    {
        return $this->belongsTo(Accounts::class, 'account_id_rec');
    }
    
}
