<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountMembers extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withoutGlobalScopes(['active']);
    }
    public function account()
    {
        return $this->belongsTo(Accounts::class, 'account_id');
    }
}
