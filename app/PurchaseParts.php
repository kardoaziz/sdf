<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseParts extends Model
{
    //
         protected $fillable = ['part_id','qty','price','date','note','account_id'];
	public function part()
    {
        return $this->belongsTo(Parts::class,"part_id");
    }
    public function account()
    {
        return $this->belongsTo(Accounts::class, 'account_id');
    }
}
