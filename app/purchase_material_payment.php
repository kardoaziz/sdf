<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchase_material_payment extends BaseModel
{
    public function purchaseMaterial(){
        return $this->belongsTo(purchase_material::class, 'purchase_id');
    }

    public function currency(){
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}
