<?php

namespace App\Observers;

use App\Expense;
use App\Notifications\NewExpenseAdmin;
use App\Notifications\NewExpenseMember;
use App\Notifications\NewExpenseStatus;
use App\User;
use Illuminate\Support\Facades\Notification;

class ExpenseObserver
{
    public function created(Expense $expense)
    {
        if (!app()->runningInConsole() ) {
            // Default status is approved means it is posted by admin
            if ($expense->status == 'approved') {
                // $expense->user->notify(new NewExpenseAdmin($expense));
            }

            // Default status is pending that mean it is posted by member
            if ($expense->status == 'pending') {
                // Notification::send(User::allAdmins(), new NewExpenseMember($expense));
            }
        }
    }

    public function saved(Expense $expense)
    {
        if (!app()->runningInConsole() && request('_method') == 'PUT' ) {
            if ($expense->isDirty('status')) {
                $expense->user->notify(new NewExpenseStatus($expense));
            }

        }
    }
}
