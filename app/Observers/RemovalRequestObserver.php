<?php

namespace App\Observers;

use App\Notifications\RemovalRequestAdminNotification;
use App\Notifications\RemovalRequestApprovedRejectUser;
use App\RemovalRequest;
use App\User;

class RemovalRequestObserver
{
    public function created(RemovalRequest $removalRequest)
    {
        if (!app()->runningInConsole() ) {
            $notifyUsers = User::allAdmins();
            foreach ($notifyUsers as $notifyUser) {
                // $notifyUser->notify(new RemovalRequestAdminNotification());
            }
        }
    }

    public function saved(RemovalRequest $removal)
    {
        if (!app()->runningInConsole() && request('_method') == 'PUT') {
            try {
                if ($removal->user) {
                    // $removal->user->notify(new RemovalRequestApprovedRejectUser($removal->status));
                }
            } catch (\Exception $e) {

            }
        }
    }
}
