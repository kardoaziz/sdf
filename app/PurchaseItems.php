<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseItems extends BaseModel
{
    //
    protected $fillable = ['warehouse_id', 'item_id','amount','price','date','note','account_id'];
    public function item()
    {
        return $this->belongsTo(items::class, 'item_id');
    }
    public function account()
    {
        return $this->belongsTo(Accounts::class, 'account_id');
    }
    public function warehouse()
    {
        return $this->belongsTo(warehouse::class, 'warehouse_id');
    }
}
