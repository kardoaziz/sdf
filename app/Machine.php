<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    //
     protected $fillable = ['name'];
     public function maintains()
    {
        return $this->hasMany(MaintainMachine::class,"machine_id");
    }
}
