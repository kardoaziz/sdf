<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parts extends Model
{
    //
         protected $fillable = ['name'];

    public function maintain()
    {
        return $this->belongsTo(MaintainMachine::class,"part_id");
    }
}
