<?php

namespace App\DataTables\Admin;

use App\DataTables\BaseDataTable;
use App\Payroll;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use DB;
class PayrollDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $action = '<div class="btn-group dropdown m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn dropdown-toggle waves-effect waves-light" type="button"><i class="ti-more"></i></button>
                <ul role="menu" class="dropdown-menu pull-right">
                <li><a href="' . route('admin.payrolls.edit', [$row->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i> ' . trans('app.edit') . '</a></li>
                  <li><a href="javascript:;"  data-user-id="' . $row->id . '"  class="sa-params"><i class="fa fa-times" aria-hidden="true"></i> ' . trans('app.delete') . '</a></li>';

                $action .= '</ul> </div>';

                return $action;
            })
            ->editColumn('user', function ($row) {
                return $row->user->name;
            })
            ->editColumn('work_days', function ($row) {
                return $row->no_work_day;
            })
            ->editColumn('work_hour', function ($row) {
                return $row->no_work_hours;
            })
            ->editColumn('hour_rate', function ($row) {
                return number_format($row->hour_rate);
            })
            ->editColumn('gift', function ($row) {
                return number_format($row->gift);
            })
            ->editColumn('penalty', function ($row) {
                return number_format($row->penalty);
            })
            ->editColumn('extra_hour', function ($row) {
                return $row->extra_hour;
            })
            ->editColumn('extra_hour_price', function ($row) {
                return number_format($row->extra_hour_rate);
            })
            ->editColumn('absence_day', function ($row) {
                return $row->absence_day;
            })
            // ->editColumn('total', function ($row) {
            //     return number_format($row->total);
            // })
            ->editColumn('paid', function ($row) {
                if($row->paid==1) return '<input type="checkbox"  checked disabled>' ;
                else return '<input type="checkbox"   disabled>' ;
            }) 
            ->editColumn('total_salary', function ($row) {
                return number_format($row->total_salary);
            })
            ->editColumn('note', function ($row) {
                return $row->note;
            }) 
            ->editColumn('month', function ($row) {
                return $row->month;
            })
            // ->editColumn('location', function ($row) {
            //     return ucfirst($row->location);
            // })
            // ->editColumn('allow_purchase', function ($row) {
            //     if ($row->allow_purchase == 1) {
            //         return '<label class="label label-success">' . __('app.allowed') . '</label>';
            //     } else {
            //         return '<label class="label label-danger">' . __('app.notAllowed') . '</label>';
            //     }
            // })
            // ->editColumn('price', function ($row) {
            //     if (!is_null($row->taxes)) {
            //         $totalTax = 0;
            //         foreach (json_decode($row->taxes) as $tax) {
            //             $this->tax = Product::taxbyid($tax)->first();
            //             $totalTax = $totalTax + ($row->price * ($this->tax->rate_percent / 100));
            //         }
            //         return $this->global->currency->currency_symbol . ($row->price + $totalTax);
            //     } else {
            //         return $this->global->currency->currency_symbol . $row->price;
            //     }
            // })
            ->addIndexColumn()
            ->rawColumns(['action','paid']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Items $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Payroll $model)
    {
        // $parts = Parts::where('id','>',0)
        // ->select('parts.id','parts.name',
        //     DB::raw('((select COALESCE(sum(purchase_parts.qty),0)  from purchase_parts where purchase_parts.part_id=parts.id  )-(select COALESCE(sum(maintain_machines.qty),0)  from maintain_machines where maintain_machines.part_id=parts.id and status="Done" )) as stock'))->orderby('stock','desc');
        // return $model->select('id', 'name', 'price', 'taxes', 'allow_purchase');
        return $model->select('*' );
        // return $parts;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('parts-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom("<'row'<'col-md-6'l><'col-md-6'Bf>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>")
            ->orderBy(0)
            ->destroy(true)
            ->responsive(true)
            ->serverSide(true)
            ->stateSave(true)
            ->processing(true)
            ->language(__("app.datatable"))
            ->buttons(
                Button::make(['extend'=> 'export','buttons' => ['excel', 'csv']])
            )
            ->parameters([
                'initComplete' => 'function () {
                   window.LaravelDataTables["parts-table"].buttons().container()
                    .appendTo( ".bg-title .text-right")
                }',
                'fnDrawCallback' => 'function( oSettings ) {
                    $("body").tooltip({
                        selector: \'[data-toggle="tooltip"]\'
                    })
                }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => ['data' => 'id', 'name' => 'id', 'visible' => true],
            __('app.user') => ['data' => 'user', 'name' => 'user'],
            __('app.month') => ['data' => 'month', 'name' => 'month'],
            __('app.no work day') => ['data' => 'no_work_day', 'name' => 'no work day'],
            __('app.no work hour') => ['data' => 'no_work_hours', 'name' => 'no work hour'],
            __('app.absence day') => ['data' => 'absence_day', 'name' => 'absence day'],
            __('app.hour rate') => ['data' => 'hour_rate', 'name' => 'hour rate'],
            __('app.extra hour') => ['data' => 'extra_hour', 'name' => 'month'],
            __('app.extra hours rate') => ['data' => 'extra_hour_rate', 'name' => 'extra_hour_rate'],
            __('app.gift') => ['data' => 'gift', 'name' => 'gift'],
            __('app.penalty') => ['data' => 'penalty', 'name' => 'penalty'],
            // __('app.total') => ['data' => 'total', 'name' => 'total'],
            __('app.paid') => ['data' => 'paid', 'name' => 'paid'],
            __('app.paid on') => ['data' => 'paid_on', 'name' => 'paid_on'],
            __('app.total salary') => ['data' => 'total_salary', 'name' => 'total_salary'],
            __('app.note') => ['data' => 'note', 'name' => 'note'],
            // __('app.location') => ['data' => 'location', 'name' => 'location'],
            // __('app.price') . '(' . __('app.inclusiveAllTaxes') . ')' => ['data' => 'price', 'name' => 'price'],
            // __('app.purchaseAllow') => ['data' => 'allow_purchase', 'name' => 'allow_purchase'],
            
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->orderable(false)
                ->searchable(false)
                ->width(150)
                ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'paayrolls_' . date('YmdHis');
    }

    public function pdf()
    {
        set_time_limit(0);
        if ('snappy' == config('datatables-buttons.pdf_generator', 'snappy')) {
            return $this->snappyPdf();
        }

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('datatables::print', ['data' => $this->getDataForPrint()]);

        return $pdf->download($this->getFilename() . '.pdf');
    }
}
