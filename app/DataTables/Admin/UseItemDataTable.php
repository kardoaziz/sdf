<?php

namespace App\DataTables\Admin;

use App\DataTables\BaseDataTable;
use App\UseItem;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;

class UseItemDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $action = '<div class="btn-group dropdown m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn dropdown-toggle waves-effect waves-light" type="button"><i class="ti-more"></i></button>
                <ul role="menu" class="dropdown-menu pull-right">

                <li><a href="' . route('admin.useitem.edit', [$row->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i> ' . trans('app.edit') . '</a></li>
                  <li><a href="javascript:;"  data-user-id="' . $row->id . '"  class="sa-params"><i class="fa fa-times" aria-hidden="true"></i> ' . trans('app.delete') . '</a></li>';

                $action .= '</ul> </div>';

                return $action;
            })
            ->editColumn('warehouse', function ($row) {
                return ucfirst($row->warehouse_name);
            })
            ->editColumn('item', function ($row) {
                return ucfirst($row->item_name);
            })
            ->editColumn('amount', function ($row) {
                return ucfirst($row->amount);
            })
            // ->editColumn('price', function ($row) {
            //     return ucfirst($row->price);
            // })
            ->editColumn('date', function ($row) {
                return ucfirst($row->date);
            })
            ->editColumn('note', function ($row) {
                return ucfirst($row->note);
            })
            // ->editColumn('location', function ($row) {
            //     return ucfirst($row->location);
            // })
            // ->editColumn('allow_purchase', function ($row) {
            //     if ($row->allow_purchase == 1) {
            //         return '<label class="label label-success">' . __('app.allowed') . '</label>';
            //     } else {
            //         return '<label class="label label-danger">' . __('app.notAllowed') . '</label>';
            //     }
            // })
            // ->editColumn('price', function ($row) {
            //     if (!is_null($row->taxes)) {
            //         $totalTax = 0;
            //         foreach (json_decode($row->taxes) as $tax) {
            //             $this->tax = Product::taxbyid($tax)->first();
            //             $totalTax = $totalTax + ($row->price * ($this->tax->rate_percent / 100));
            //         }
            //         return $this->global->currency->currency_symbol . ($row->price + $totalTax);
            //     } else {
            //         return $this->global->currency->currency_symbol . $row->price;
            //     }
            // })
            ->addIndexColumn()
            ->rawColumns(['action']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Items $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(UseItem $model)
    {
        // return $model->select('id', 'name', 'price', 'taxes', 'allow_purchase');
        $uses = UseItem::join('warehouses','warehouses.id','use_items.warehouse_id')->join('items','items.id','use_items.item_id')->join('employee_details','employee_details.id','use_items.employee_id')->join('users','users.id','employee_details.user_id')->select('use_items.id','warehouses.name as warehouse_name','items.name as item_name','use_items.date','use_items.amount','use_items.note','users.name as employee');
        return $uses;
        // return $model->select('id', 'name' );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('items-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom("<'row'<'col-md-6'l><'col-md-6'Bf>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>")
            ->orderBy(0)
            ->destroy(true)
            ->responsive(true)
            ->serverSide(true)
            ->stateSave(true)
            ->processing(true)
            ->language(__("app.datatable"))
            ->buttons(
                Button::make(['extend'=> 'export','buttons' => ['excel', 'csv']])
            )
            ->parameters([
                'initComplete' => 'function () {
                   window.LaravelDataTables["items-table"].buttons().container()
                    .appendTo( ".bg-title .text-right")
                }',
                'fnDrawCallback' => 'function( oSettings ) {
                    $("body").tooltip({
                        selector: \'[data-toggle="tooltip"]\'
                    })
                }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => ['data' => 'id', 'name' => 'id', 'visible' => true],
            __('app.warehouse') => ['data' => 'warehouse', 'name' => 'warehouse'],
            __('app.item') => ['data' => 'item', 'name' => 'item'],
            __('app.employee')  => ['data' => 'employee', 'name' => 'employee'],
            __('app.amount') . '(qty)' => ['data' => 'amount', 'name' => 'amount'],
            // __('app.price') . '/qty' => ['data' => 'price', 'name' => 'price'],
            __('app.date')  => ['data' => 'date', 'name' => 'date'],
            __('app.note')  => ['data' => 'note', 'name' => 'note'],
            // __('app.purchaseAllow') => ['data' => 'allow_purchase', 'name' => 'allow_purchase'],
            
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->orderable(false)
                ->searchable(false)
                ->width(150)
                ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'use_items_' . date('YmdHis');
    }

    public function pdf()
    {
        set_time_limit(0);
        if ('snappy' == config('datatables-buttons.pdf_generator', 'snappy')) {
            return $this->snappyPdf();
        }

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('datatables::print', ['data' => $this->getDataForPrint()]);

        return $pdf->download($this->getFilename() . '.pdf');
    }
}
