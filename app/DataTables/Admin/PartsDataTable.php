<?php

namespace App\DataTables\Admin;

use App\DataTables\BaseDataTable;
use App\Parts;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use DB;
class PartsDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $action = '<div class="btn-group dropdown m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn dropdown-toggle waves-effect waves-light" type="button"><i class="ti-more"></i></button>
                <ul role="menu" class="dropdown-menu pull-right">
                <li><a href="' . route('admin.parts.edit', [$row->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i> ' . trans('app.edit') . '</a></li>
                  <li><a href="javascript:;"  data-user-id="' . $row->id . '"  class="sa-params"><i class="fa fa-times" aria-hidden="true"></i> ' . trans('app.delete') . '</a></li>';

                $action .= '</ul> </div>';

                return $action;
            })
            ->editColumn('name', function ($row) {
                return ucfirst($row->name);
            }) 
            ->editColumn('stock', function ($row) {
                return ucfirst($row->stock);
            })
            // ->editColumn('location', function ($row) {
            //     return ucfirst($row->location);
            // })
            // ->editColumn('allow_purchase', function ($row) {
            //     if ($row->allow_purchase == 1) {
            //         return '<label class="label label-success">' . __('app.allowed') . '</label>';
            //     } else {
            //         return '<label class="label label-danger">' . __('app.notAllowed') . '</label>';
            //     }
            // })
            // ->editColumn('price', function ($row) {
            //     if (!is_null($row->taxes)) {
            //         $totalTax = 0;
            //         foreach (json_decode($row->taxes) as $tax) {
            //             $this->tax = Product::taxbyid($tax)->first();
            //             $totalTax = $totalTax + ($row->price * ($this->tax->rate_percent / 100));
            //         }
            //         return $this->global->currency->currency_symbol . ($row->price + $totalTax);
            //     } else {
            //         return $this->global->currency->currency_symbol . $row->price;
            //     }
            // })
            ->addIndexColumn()
            ->rawColumns(['action']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Items $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Parts $model)
    {
        $parts = Parts::where('id','>',0)
        ->select('parts.id','parts.name',
            DB::raw('((select COALESCE(sum(purchase_parts.qty),0)  from purchase_parts where purchase_parts.part_id=parts.id  )-(select COALESCE(sum(maintain_machines.qty),0)  from maintain_machines where maintain_machines.part_id=parts.id and status="Done" )) as stock'))->orderby('stock','desc');
        // return $model->select('id', 'name', 'price', 'taxes', 'allow_purchase');
        // return $model->select('id', 'name' );
        return $parts;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('parts-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom("<'row'<'col-md-6'l><'col-md-6'Bf>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>")
            ->orderBy(0)
            ->destroy(true)
            ->responsive(true)
            ->serverSide(true)
            ->stateSave(true)
            ->processing(true)
            ->language(__("app.datatable"))
            ->buttons(
                Button::make(['extend'=> 'export','buttons' => ['excel', 'csv']])
            )
            ->parameters([
                'initComplete' => 'function () {
                   window.LaravelDataTables["parts-table"].buttons().container()
                    .appendTo( ".bg-title .text-right")
                }',
                'fnDrawCallback' => 'function( oSettings ) {
                    $("body").tooltip({
                        selector: \'[data-toggle="tooltip"]\'
                    })
                }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => ['data' => 'id', 'name' => 'id', 'visible' => true],
            __('app.name') => ['data' => 'name', 'name' => 'name'],
            __('app.stock') => ['data' => 'stock', 'name' => 'stock'],
            // __('app.location') => ['data' => 'location', 'name' => 'location'],
            // __('app.price') . '(' . __('app.inclusiveAllTaxes') . ')' => ['data' => 'price', 'name' => 'price'],
            // __('app.purchaseAllow') => ['data' => 'allow_purchase', 'name' => 'allow_purchase'],
            
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->orderable(false)
                ->searchable(false)
                ->width(150)
                ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'parts_' . date('YmdHis');
    }

    public function pdf()
    {
        set_time_limit(0);
        if ('snappy' == config('datatables-buttons.pdf_generator', 'snappy')) {
            return $this->snappyPdf();
        }

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('datatables::print', ['data' => $this->getDataForPrint()]);

        return $pdf->download($this->getFilename() . '.pdf');
    }
}
