<?php

namespace App\DataTables\Admin;

use App\DataTables\BaseDataTable;
use App\Accounts;
use App\Payment;
use App\Expense;
use App\PurchaseItems;
use App\PurchaseParts;
use App\purchase_material_payment;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use DB;
class accountdetailDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $action = '<div class="btn-group dropdown m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn dropdown-toggle waves-effect waves-light" type="button"><i class="ti-more"></i></button>
                <ul role="menu" class="dropdown-menu pull-right">
                <li><a href="' . route('admin.accounts.edit', [$row->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i> ' . trans('app.edit') . '</a></li>
                  <li><a href="javascript:;"  data-user-id="' . $row->id . '"  class="sa-params"><i class="fa fa-times" aria-hidden="true"></i> ' . trans('app.delete') . '</a></li>';

                $action .= '</ul> </div>';

                return $action;
            })
            // ->editColumn('account_no', function ($row) {
            //     return ucfirst($row->account_no);
            // })
            // ->editColumn('name', function ($row) {
            //     return ucfirst($row->name);
            // })
            ->editColumn('amount', function ($row) {
                if($row->type=='in' )
                {
                return ucfirst($row->amount);
                }
                else
                {
                    return ucfirst($row->amount*-1);
                }
            }) 
            ->editColumn('type', function ($row) {
                if($row->type=='in' )
                {
                    return '<label class="label label-success" style="font-size:25px;">' . $row->type. '</label>';
                }else 
                {
                    return '<label class="label label-danger" style="font-size:25px;">' .$row->type. '</label>';
                }
            })
            ->editColumn('date', function ($row) {
                return ucfirst($row->date);
            })
            ->editColumn('detail', function ($row) {
                return $row->detail;
            //     // return $row->initial_balance+($row->incomings)-($row->outgoings);
            })
           
            
            // ->editColumn('allow_purchase', function ($row) {
            //     if ($row->allow_purchase == 1) {
            //         return '<label class="label label-success">' . __('app.allowed') . '</label>';
            //     } else {
            //         return '<label class="label label-danger">' . __('app.notAllowed') . '</label>';
            //     }
            // })
            // ->editColumn('price', function ($row) {
            //     if (!is_null($row->taxes)) {
            //         $totalTax = 0;
            //         foreach (json_decode($row->taxes) as $tax) {
            //             $this->tax = Product::taxbyid($tax)->first();
            //             $totalTax = $totalTax + ($row->price * ($this->tax->rate_percent / 100));
            //         }
            //         return $this->global->currency->currency_symbol . ($row->price + $totalTax);
            //     } else {
            //         return $this->global->currency->currency_symbol . $row->price;
            //     }
            // })
            ->addIndexColumn()
            ->rawColumns(['action','type']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Items $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Accounts $model)
    {
        $id = $this->account_id;
        $incomings = Payment::where('account_id',$id)->select('id','amount',DB::raw('paid_on as "date"'),DB::raw('"in" as "type"'),DB::raw('(select projects.project_name from projects where projects.id=payments.project_id limit 1) as detail'));
        $expenses = Expense::where('account_id',$id)->select('id',DB::raw('price as "amount"'),DB::raw('purchase_date as "date"'),DB::raw('"out" as "type"'),DB::raw('CONCAT(expenses.item_name," For ",(select projects.project_name from projects where projects.id=expenses.project_id limit 1)) as detail'));
        $purchaseitems = PurchaseItems::where('account_id',$id)->select('id',DB::raw('amount*price as "amount"'),'date',DB::raw('"out" as "type"'),DB::raw('(select items.name from items where items.id=purchase_items.item_id limit 1) as detail'));
        $purchaseparts = PurchaseParts::where('account_id',$id)->select('id',DB::raw('qty*price as "amount"'),'date',DB::raw('"out" as "type"'),DB::raw('(select parts.name from parts where parts.id=purchase_parts.part_id limit 1) as detail'));
        $purchasematerials = purchase_material_payment::where('account_id',$id)->select('id','amount',DB::raw('paid_on as "date"'),DB::raw('"out" as "type"'),DB::raw('CONCAT("buying machines parts, in purchase no ",(select purchase_materials.purchase_no from purchase_materials where purchase_materials.id=purchase_material_payments.purchase_id limit 1)) as detail'));
        // return $incomings;
        // return $expenses;
        // return $purchaseitems;
        // return $purchaseparts;
        // return $purchasematerials;
         $incomings->union($expenses)->union($purchaseitems)->union($purchaseparts)->union($purchasematerials)->orderBy('date','desc');
        return $incomings;
        // return $model->select('id','name');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('items-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom("<'row'<'col-md-6'l><'col-md-6'Bf>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>")
            ->orderBy(0)
            ->destroy(true)
            ->responsive(true)
            ->serverSide(true)
            ->stateSave(true)
            ->processing(true)
            ->language(__("app.datatable"))
            ->buttons(
                Button::make(['extend'=> 'export','buttons' => ['excel', 'csv']])
            )
            ->parameters([
                'initComplete' => 'function () {
                   window.LaravelDataTables["items-table"].buttons().container()
                    .appendTo( ".bg-title .text-right")
                }',
                'fnDrawCallback' => 'function( oSettings ) {
                    $("body").tooltip({
                        selector: \'[data-toggle="tooltip"]\'
                    })
                }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => ['data' => 'id', 'name' => 'id', 'visible' => true],
            __('app.amount') => ['data' => 'amount', 'name' => 'amount'],
            __('app.date') => ['data' => 'date', 'name' => 'date'],
            // __('app.initial balance') => ['data' => 'initial_balance', 'name' => 'initial_balance'],
            __('app.type') => ['data' => 'type', 'name' => 'type'],
            __('app.detail') => ['data' => 'detail', 'name' => 'detail'],
            // __('app.balance') => ['data' => 'balance', 'name' => 'balance'],
          
            
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->orderable(false)
                ->searchable(false)
                ->width(150)
                ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'accounts_details_' . date('YmdHis');
    }

    public function pdf()
    {
        set_time_limit(0);
        if ('snappy' == config('datatables-buttons.pdf_generator', 'snappy')) {
            return $this->snappyPdf();
        }

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('datatables::print', ['data' => $this->getDataForPrint()]);

        return $pdf->download($this->getFilename() . '.pdf');
    }
}
