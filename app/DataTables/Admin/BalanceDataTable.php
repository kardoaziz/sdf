<?php

namespace App\DataTables\Admin;

use App\DataTables\BaseDataTable;
use App\Accounts;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use DB;
class BalanceDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $action = '<div class="btn-group dropdown m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn dropdown-toggle waves-effect waves-light" type="button"><i class="ti-more"></i></button>
                <ul role="menu" class="dropdown-menu pull-right">
                
                <li><a href="' . route('admin.accounts.detail', [$row->id]) . '"><i class="fa fa-clipboard" aria-hidden="true"></i> ' . trans('app.detail') . '</a></li>
                 ';

                $action .= '</ul> </div>';

                return $action;
            })
            ->editColumn('account_no', function ($row) {
                return ucfirst($row->account_no);
            })
            ->editColumn('name', function ($row) {
                return ucfirst($row->name);
            })
            ->editColumn('initial_balance', function ($row) {
                return ucfirst($row->initial_balance);
            }) 
            ->editColumn('incomings', function ($row) {
                return ucfirst($row->incomings);
            })
            ->editColumn('outgoings', function ($row) {
                return ucfirst($row->outgoings);
            })
            ->editColumn('balance', function ($row) {
                if(($row->initial_balance+($row->incomings)-($row->outgoings))>0 )
                {
                    return '<label class="label label-success" style="font-size:25px;">' . number_format(($row->initial_balance+($row->incomings)-($row->outgoings))). '</label>';
                }else {
                    return '<label class="label label-danger" style="font-size:25px;">' .number_format( ($row->initial_balance+($row->incomings)-($row->outgoings))). '</label>';}
                // return $row->initial_balance+($row->incomings)-($row->outgoings);
            })
           
            
            // ->editColumn('allow_purchase', function ($row) {
            //     if ($row->allow_purchase == 1) {
            //         return '<label class="label label-success">' . __('app.allowed') . '</label>';
            //     } else {
            //         return '<label class="label label-danger">' . __('app.notAllowed') . '</label>';
            //     }
            // })
            // ->editColumn('price', function ($row) {
            //     if (!is_null($row->taxes)) {
            //         $totalTax = 0;
            //         foreach (json_decode($row->taxes) as $tax) {
            //             $this->tax = Product::taxbyid($tax)->first();
            //             $totalTax = $totalTax + ($row->price * ($this->tax->rate_percent / 100));
            //         }
            //         return $this->global->currency->currency_symbol . ($row->price + $totalTax);
            //     } else {
            //         return $this->global->currency->currency_symbol . $row->price;
            //     }
            // })
            ->addIndexColumn()
            ->rawColumns(['action','balance']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Items $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Accounts $model)
    {
        $accounts = Accounts::where('id','>',0)
        ->select('accounts.id','accounts.name','accounts.account_no','accounts.initial_balance',
             DB::raw('(select COALESCE(sum(payments.amount),0)  from payments where payments.account_id=accounts.id ) as incomings'),
            DB::raw('((select COALESCE(sum(expenses.price),0)  from expenses where expenses.account_id=accounts.id ) + (select COALESCE(sum(purchase_items.amount*purchase_items.price),0)  from purchase_items where purchase_items.account_id=accounts.id)+(select COALESCE(sum(purchase_parts.price*purchase_parts.qty),0)  from purchase_parts where purchase_parts.account_id=accounts.id)+(select COALESCE(sum(purchase_material_payments.amount),0)  from purchase_material_payments where purchase_material_payments.account_id=accounts.id) ) as outgoings'));
        // return $model->select('id','account_no', 'name', 'initial_balance', 'is_default','note' );
        return $accounts;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('items-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom("<'row'<'col-md-6'l><'col-md-6'Bf>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>")
            ->orderBy(0)
            ->destroy(true)
            ->responsive(true)
            ->serverSide(true)
            ->stateSave(true)
            ->processing(true)
            ->language(__("app.datatable"))
            ->buttons(
                Button::make(['extend'=> 'export','buttons' => ['excel', 'csv']])
            )
            ->parameters([
                'initComplete' => 'function () {
                   window.LaravelDataTables["items-table"].buttons().container()
                    .appendTo( ".bg-title .text-right")
                }',
                'fnDrawCallback' => 'function( oSettings ) {
                    $("body").tooltip({
                        selector: \'[data-toggle="tooltip"]\'
                    })
                }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => ['data' => 'id', 'name' => 'id', 'visible' => true],
            __('app.name') => ['data' => 'name', 'name' => 'name'],
            __('app.account no') => ['data' => 'account_no', 'name' => 'account_no'],
            __('app.initial balance') => ['data' => 'initial_balance', 'name' => 'initial_balance'],
            __('app.incomings') => ['data' => 'incomings', 'name' => 'incomings'],
            __('app.outgoings') => ['data' => 'outgoings', 'name' => 'outgoings'],
            __('app.balance') => ['data' => 'balance', 'name' => 'balance'],
          
            
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->orderable(false)
                ->searchable(false)
                ->width(150)
                ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'accounts_balance_' . date('YmdHis');
    }

    public function pdf()
    {
        set_time_limit(0);
        if ('snappy' == config('datatables-buttons.pdf_generator', 'snappy')) {
            return $this->snappyPdf();
        }

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('datatables::print', ['data' => $this->getDataForPrint()]);

        return $pdf->download($this->getFilename() . '.pdf');
    }
}
