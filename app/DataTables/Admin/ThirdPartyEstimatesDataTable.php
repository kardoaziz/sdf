<?php

namespace App\DataTables\Admin;

use App\DataTables\BaseDataTable;
use App\ThirdParty;
use App\Project;
use App\Estimate;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use DB;
class ThirdPartyEstimatesDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $action = '<div class="btn-group dropdown m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn dropdown-toggle waves-effect waves-light" type="button"><i class="ti-more"></i></button>
                <ul role="menu" class="dropdown-menu pull-right">
                <li><a href="' . route('admin.items.edit', [$row->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i> ' . trans('app.edit') . '</a></li>
                  <li><a href="javascript:;"  data-user-id="' . $row->id . '"  class="sa-params"><i class="fa fa-times" aria-hidden="true"></i> ' . trans('app.delete') . '</a></li>';

                $action .= '</ul> </div>';

                return $action;
            })
            ->editColumn('name', function ($row) {
                $name = '<a href="' . route('front.estimate.show', md5($row->id)) . '" " target="_blank">' . ucfirst($row->client->name) . '</a>';

                return $name;
            })
             ->editColumn('estimate_number', function ($row) {
                $name = '<a href="' . route("front.estimate.show", md5($row->id)) . '" target="_blank">' . ucfirst($row->id) . '</a>';

                return $name;
            })
             ->editColumn('total', function ($row) {
                return ucfirst($row->total);
            })
            // ->editColumn('location', function ($row) {
            //     return ucfirst($row->location);
            // })
            // ->editColumn('allow_purchase', function ($row) {
            //     if ($row->allow_purchase == 1) {
            //         return '<label class="label label-success">' . __('app.allowed') . '</label>';
            //     } else {
            //         return '<label class="label label-danger">' . __('app.notAllowed') . '</label>';
            //     }
            // })
            // ->editColumn('price', function ($row) {
            //     if (!is_null($row->taxes)) {
            //         $totalTax = 0;
            //         foreach (json_decode($row->taxes) as $tax) {
            //             $this->tax = Product::taxbyid($tax)->first();
            //             $totalTax = $totalTax + ($row->price * ($this->tax->rate_percent / 100));
            //         }
            //         return $this->global->currency->currency_symbol . ($row->price + $totalTax);
            //     } else {
            //         return $this->global->currency->currency_symbol . $row->price;
            //     }
            // })
            ->addIndexColumn()
            ->rawColumns(['name','estimate_number','action']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Items $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Estimate $model)
    {
        // return $model->select('id', 'name', 'price', 'taxes', 'allow_purchase');
        $id = $this->thirdparty_id;
        $estimates = Estimate::with('client')->where('thirdparty_id',$id)->select('id','client_id','total','estimate_number');
        return $estimates;//$model->select('id', 'name' );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('items-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom("<'row'<'col-md-6'l><'col-md-6'Bf>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>")
            ->orderBy(0)
            ->destroy(true)
            ->responsive(true)
            ->serverSide(true)
            ->stateSave(true)
            ->processing(true)
            ->language(__("app.datatable"))
            ->buttons(
                Button::make(['extend'=> 'export','buttons' => ['excel', 'csv']])
            )
            ->parameters([
                'initComplete' => 'function () {
                   window.LaravelDataTables["items-table"].buttons().container()
                    .appendTo( ".bg-title .text-right")
                }',
                'fnDrawCallback' => 'function( oSettings ) {
                    $("body").tooltip({
                        selector: \'[data-toggle="tooltip"]\'
                    })
                }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => ['data' => 'id', 'name' => 'id', 'visible' => true],
            __('app.name') => ['data' => 'name', 'name' => 'name'],
            __('app.estimate_number') => ['data' => 'estimate_number', 'name' => 'estimate_number'],
            __('app.total') => ['data' => 'total', 'name' => 'total'],
            // __('app.location') => ['data' => 'location', 'name' => 'location'],
            // __('app.price') . '(' . __('app.inclusiveAllTaxes') . ')' => ['data' => 'price', 'name' => 'price'],
            // __('app.purchaseAllow') => ['data' => 'allow_purchase', 'name' => 'allow_purchase'],
            
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->orderable(false)
                ->searchable(false)
                ->width(150)
                ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ThirdPartyEstimates_' . date('YmdHis');
    }

    public function pdf()
    {
        set_time_limit(0);
        if ('snappy' == config('datatables-buttons.pdf_generator', 'snappy')) {
            return $this->snappyPdf();
        }

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('datatables::print', ['data' => $this->getDataForPrint()]);

        return $pdf->download($this->getFilename() . '.pdf');
    }
}
