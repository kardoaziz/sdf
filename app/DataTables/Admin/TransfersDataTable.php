<?php

namespace App\DataTables\Admin;

use App\DataTables\BaseDataTable;
use App\Transfer;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use DB;
use Auth;
class TransfersDataTable extends BaseDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $action = '<div class="btn-group dropdown m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn dropdown-toggle waves-effect waves-light" type="button"><i class="ti-more"></i></button>
                <ul role="menu" class="dropdown-menu pull-right">
                <li><a href="' . route('admin.transfers.edit', [$row->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i> ' . trans('app.edit') . '</a></li>
                  <li><a href="javascript:;"  data-user-id="' . $row->id . '"  class="sa-params"><i class="fa fa-times" aria-hidden="true"></i> ' . trans('app.delete') . '</a></li>';
                  

                $action .= '</ul> </div>';

                return $action;
            })
            ->editColumn('sender', function ($row) {
                return ucfirst($row->account->name);
            })
            ->editColumn('reciever', function ($row) {
                return ucfirst($row->reciever->name);
            })
            ->editColumn('amount', function ($row) {
                return ucfirst($row->amount);
            })
            ->editColumn('date', function ($row) {
                return ucfirst($row->date);
            })
            ->editColumn('status', function ($row) {
                // return ucfirst($row->status);
                if($row->status=="recieved")
                {
                    return '<label class="label label-success" style="">' . $row->status. '</label>';
                }else {
                    if(in_array(Auth::user()->id, explode(',', $row->members)))
                    {
                        return '<a  onclick="return confirm(\'دڵنیایت لە وەرگرتنی پارەکە ؟\')"  href="' . route('admin.transfers.accept', [$row->id]) . '"    ><i style="color:#00c292;" class="fa fa-check" aria-hidden="true"></i> ' . trans('modules.transfers.accept') . '</a>
                  ';
                  }else{
                        return '<label class="label label-danger" style="">' .$row->status. '</label>';}
                }
            })
            ->editColumn('note', function ($row) {
                return ucfirst($row->note);
            })
            ->addIndexColumn()
            ->rawColumns(['action','status']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Items $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Transfer $model)
    {
        // return $model->select('id', 'name', 'price', 'taxes', 'allow_purchase');
        $transfers = Transfer::where('id','>',0)->select('id', 'account_id','account_id_rec','amount','date','note','status' ,DB::raw('(SELECT GROUP_CONCAT(user_id) as users FROM account_members where account_id=transfers.account_id_rec

    ) as members') );
        return $transfers;
        // return $model->select('id', 'account_id','account_id_rec','amount','date','note','status' );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('transfers-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom("<'row'<'col-md-6'l><'col-md-6'Bf>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>")
            ->orderBy(0)
            ->destroy(true)
            ->responsive(true)
            ->serverSide(true)
            ->stateSave(true)
            ->processing(true)
            ->language(__("app.datatable"))
            ->buttons(
                Button::make(['extend'=> 'export','buttons' => ['excel', 'csv']])
            )
            ->parameters([
                'initComplete' => 'function () {
                   window.LaravelDataTables["transfers-table"].buttons().container()
                    .appendTo( ".bg-title .text-right")
                }',
                'fnDrawCallback' => 'function( oSettings ) {
                    $("body").tooltip({
                        selector: \'[data-toggle="tooltip"]\'
                    })
                }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => ['data' => 'id', 'name' => 'id', 'visible' => true],
            __('modules.transfers.sender') => ['data' => 'sender', 'name' => 'sender'],
            __('modules.transfers.reciever') => ['data' => 'reciever', 'name' => 'reciever'],
            __('app.amount') => ['data' => 'amount', 'name' => 'amount'],
            __('app.date') => ['data' => 'date', 'name' => 'date'],
            __('app.note') => ['data' => 'note', 'name' => 'note'],
            __('modules.transfers.status') => ['data' => 'status', 'name' => 'status'],
            
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->orderable(false)
                ->searchable(false)
                ->width(150)
                ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Transfers' . date('YmdHis');
    }

    public function pdf()
    {
        set_time_limit(0);
        if ('snappy' == config('datatables-buttons.pdf_generator', 'snappy')) {
            return $this->snappyPdf();
        }

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('datatables::print', ['data' => $this->getDataForPrint()]);

        return $pdf->download($this->getFilename() . '.pdf');
    }
}
