<?php

namespace App\DataTables\Admin;

use App\DataTables\BaseDataTable;
use App\purchase_material;
use App\Task;
use App\TaskboardColumn;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;

class PurchaseMDataTable extends BaseDataTable
{
    protected $firstEstimate;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $firstEstimate = $this->firstEstimate;
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($firstEstimate) {
                $action = '<div class="btn-group dropdown m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn dropdown-toggle waves-effect waves-light" type="button"><i class="ti-more"></i></button>
                <ul role="menu" class="dropdown-menu">
                <li><a href="' . route("admin.purchasematerial.download", $row->id) . '" ><i class="fa fa-download"></i> Download</a></li>
                <li><a href="' . route("admin.purchasematerial.createPayment", $row->id) . '" ><i class="fa fa-plus"></i> Create Payment</a></li>
                  ';
                  if ($row->status != 'unpaid') {
                    $action .= '<li><a href="' . route("admin.purchasematerialpayment.show", $row->id) . '" ><i class="fa fa-minus"></i> Payments</a></li>';
                }
                    $action .= '<li><a href="' . route("admin.purchasematerial.edit", $row->id) . '" ><i class="fa fa-pencil"></i> Edit</a></li>';
                    $action .= '<li><a class="sa-params" href="javascript:;" data-estimate-id="' . $row->id . '"><i class="fa fa-times"></i> Delete</a></li>';
                
                $action .= '</ul>
              </div>
              ';
                return $action;
            })
            ->addColumn('original_estimate_number', function ($row) {
                return $row->original_estimate_number;
            })
            ->editColumn('status', function ($row) {
                if ($row->credit_note) {
                    return '<label class="label label-warning">' . strtoupper(__('app.credit-note')) . '</label>';
                } else {
                    if ($row->status == 'unpaid') {
                        return '<label class="label label-danger">' . strtoupper($row->status) . '</label>';
                    } elseif ($row->status == 'paid') {
                        return '<label class="label label-success">' . strtoupper($row->status) . '</label>';
                    } elseif ($row->status == 'canceled') {
                        return '<label class="label label-danger">' . strtoupper(__('app.canceled')) . '</label>';
                    } else {
                        return '<label class="label label-info">' . strtoupper(__('modules.invoices.partial')) . '</label>';
                    }
                }
            })
            ->editColumn('total', function ($row) {
                $currencySymbol = $row->currency_symbol;

                return '<div class="text-right">'.__('app.total').': ' . $currencySymbol . $row->total . '<br><span class="text-success">'.__('app.paid').':</span> ' . $currencySymbol . $row->amountPaid()  . '<br><span class="text-danger">'.__('app.unpaid').':</span> ' . $currencySymbol . $row->amountDue() . '</div>';
            })
            ->editColumn(
                'valid_till',
                function ($row) {
                    return Carbon::parse($row->valid_till)->format($this->global->date_format);
                }
            )
            ->rawColumns(['name', 'action', 'status','total'])
            ->removeColumn('currency_symbol')
            ->removeColumn('client_id');

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Product $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(purchase_material $model)
    {
        $request = $this->request();

        $this->firstEstimate = purchase_material::latest()->first();
        $model = $model->join('suppliers', 'purchase_materials.supplier_id', '=', 'suppliers.id')
            ->join('currencies', 'currencies.id', '=', 'purchase_materials.currency_id')
            ->select('purchase_materials.id', 'purchase_materials.supplier_id', 'suppliers.name', 'purchase_materials.total', 'currencies.currency_symbol', 'purchase_materials.status', 'purchase_materials.valid_till', 'purchase_materials.purchase_no');

        if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
            $model = $model->where(DB::raw('DATE(purchase_materials.`valid_till`)'), '>=', $request->startDate);
        }

        if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
            $model = $model->where(DB::raw('DATE(purchase_materials.`valid_till`)'), '<=', $request->endDate);
        }

        if ($request->status != 'all' && !is_null($request->status)) {
            $model = $model->where('purchase_materials.status', '=', $request->status);
        }

        return $model->orderBy('purchase_materials.id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('estimates-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom("<'row'<'col-md-6'l><'col-md-6'Bf>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>")
            ->orderBy(0)
            ->destroy(true)
            ->responsive(true)
            ->serverSide(true)
            ->stateSave(true)
            ->processing(true)
            ->language(__("app.datatable"))
            ->buttons(
                Button::make(['extend'=> 'export','buttons' => ['excel', 'csv']])
            )
            ->parameters([
                'initComplete' => 'function () {
                   window.LaravelDataTables["estimates-table"].buttons().container()
                    .appendTo( ".bg-title .text-right")
                }',
                'fnDrawCallback' => 'function( oSettings ) {
                    $("body").tooltip({
                        selector: \'[data-toggle="tooltip"]\'
                    })
                }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            __('app.id') => ['data' => 'id', 'name' => 'id', 'visible' => false],
            '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false ],
            __('modules.material.estimatesNumber'). '#' => ['data' => 'purchase_no', 'name' => 'purchase_no'],
            __('app.supplier')  => ['data' => 'name', 'name' => 'suppliers.name'],
            __('modules.invoices.total') => ['data' => 'total', 'name' => 'total'],
            __('modules.material.validTill') => ['data' => 'valid_till', 'name' => 'valid_till'],
            __('app.status') => ['data' => 'status', 'name' => 'status'],
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->orderable(false)
                ->searchable(false)
                ->width(150)
                ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'estimates_' . date('YmdHis');
    }

    public function pdf()
    {
        set_time_limit(0);
        if ('snappy' == config('datatables-buttons.pdf_generator', 'snappy')) {
            return $this->snappyPdf();
        }

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('datatables::print', ['data' => $this->getDataForPrint()]);

        return $pdf->download($this->getFilename() . '.pdf');
    }
}
