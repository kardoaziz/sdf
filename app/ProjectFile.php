<?php

namespace App;

use App\Observers\FileUploadObserver;
use Illuminate\Database\Eloquent\Model;

class ProjectFile extends BaseModel
{
    protected static function boot()
    {
        parent::boot();
        static::observe(FileUploadObserver::class);
    }
}
