<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UseItem extends BaseModel
{
    //
    protected $fillable = ['warehouse_id', 'item_id','amount','employee_id','date','note'];
    public function item()
    {
        return $this->belongsTo(items::class, 'item_id');
    }
    public function warehouse()
    {
        return $this->belongsTo(warehouse::class, 'warehouse_id');
    }
    public function employee()
    {
        return $this->belongsTo(EmployeeDetails::class, 'employee_id');
    }
}
