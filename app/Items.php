<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends BaseModel
{
    //
     protected $fillable = ['name'];
     public function purchases()
    {
        return $this->hasMany(PurchaseItems::class,"item_id");
    }

}
