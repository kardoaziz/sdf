<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounts extends BaseModel
{
    //
         protected $fillable = ['name','account_no','initial_balance','note'];
 	public function expenses() {
 		return $this->hasMany(Expenses::class,"account_id");

     }
     public function purchase_items() {
 		return $this->hasMany(PurchaseItems::class,"account_id");

     }
     public function purchase_parts() {
 		return $this->hasMany(PurchaseParts::class,"account_id");

     }
     public function purchase_materials() {
 		return $this->hasMany(purchase_material::class,"account_id");

     }
     public function members()
    {
        return $this->hasMany(AccountMembers::class, 'account_id');
    }
}
