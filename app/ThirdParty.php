<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThirdParty extends Model
{
    //
    protected $fillable = ['name','email','phone','note'];
    
     public function payments()
    {
        return $this->hasMany(ThirdPartyPayments::class, 'thirdparty_id');
    }
    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'thirdparty_id');
    }
}
