<?php

namespace App\Http\Controllers\Admin;

use App\Payroll;
use App\DataTables\Admin\PayrollDataTable;
use App\GdprSetting;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;

use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class PayrollController extends AdminBaseController
{ 

     public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.payrolls');
        $this->pageIcon = 'glyphicon glyphicon-list';
        $this->middleware(function ($request, $next) {
            if (!in_array('products', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PayrollDataTable $dataTable)
    {
        //
        return $dataTable->render('admin.payrolls.index', $this->data);
        // return view('admin.payrolls.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.payrolls.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $request;
        $month = $request->month;
        if(isset($request->paid))$paid = 1;
        else $paid = 0;
        if(isset($request->paid_on))$paid_on =$request->paid_on;
        else $paid_on = null;
         
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payroll  $payroll
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payroll  $payroll
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->payroll = Payroll::findOrFail($id);
        // return $this->payroll;
        return view('admin.payrolls.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payroll  $payroll
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $payroll = Payroll::find($id);
        $payroll->month = $request->month;
        $payroll->no_work_day = $request->no_work_day;
        $payroll->no_work_hours = $request->no_work_hours;
        $payroll->absence_day = $request->absence_day;
        $payroll->hour_rate = $request->hour_rate;
        $payroll->extra_hour = $request->extra_hour;
        $payroll->extra_hour_rate = $request->extra_hour_rate;
        $payroll->gift = $request->gift;
        $payroll->penalty = $request->penalty;
        $payroll->note = $request->note;
        if(isset($request->paid_on)){
        $payroll->paid_on = $request->paid_on;
        $payroll->paid = 1;

        }else{
            $payroll->paid_on = null;
        $payroll->paid = 0;
        }
        $payroll->total = $request->no_work_hours*$request->hour_rate+$request->extra_hour*$request->extra_hour_rate-$request->penalty+$request->gift;
        $payroll->save();
         return Reply::redirect(route('admin.payrolls.index'), __('messages.purchaseUpdatedSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payroll  $payroll
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         Payroll::destroy($id);
        return Reply::redirect(route('admin.payrolls.index'), __('messages.purchaseDeletedSuccess'));
    }
}
