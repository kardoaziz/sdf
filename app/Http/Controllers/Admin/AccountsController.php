<?php

namespace App\Http\Controllers\Admin;

// use App\warehouse;
use App\DataTables\Admin\AccountsDataTable;
use App\DataTables\Admin\BalanceDataTable;
use App\DataTables\Admin\accountdetailDataTable;
// use App\GdprSetting;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;

use Illuminate\Support\Facades\View;
use App\Accounts;
use App\AccountMembers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class AccountsController extends AdminBaseController
{
     public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.accounts');
        $this->pageIcon = 'glyphicon glyphicon-list';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AccountsDataTable $dataTable)
    {
        //
        return $dataTable->render('admin.accounts.index', $this->data);
        // return view("admin.warehouse.index" , $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return View::make('admin.accounts.createaccount', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function balancing(BalanceDataTable $dataTable)
    {
        // return "yes";
        
        // $dataTable = new BalanceDataTable;
        return $dataTable->render('admin.accounts.balance', $this->data);
    }
    public function balancingdetail(accountdetailDataTable $dataTable,$id)
    {
        // return "yes";
        
        // $dataTable = new BalanceDataTable;
        return $dataTable->with('account_id',$id)->render('admin.accounts.detail', $this->data);
    }
    public function store(Request $request)
    {
        //
         $add = Accounts::firstOrCreate([
                    'account_no' => $request->account_no,
                    'name' => $request->name,
                    'initial_balance' => $request->balance,
                    'note' => $request->note,
                    // 'is_default' => $request->is_[0],
                    // 'location' => $request->location[0],
                ]);
         $users = $request->users;
         foreach ($users as $user) {
            $member = new AccountMembers();
            $member->user_id = $user;
            $member->account_id = $request->account_id;
            $member->save();
         }
        // $warehouse = array_combine($request->date, $request->occasion);
        // foreach ($warehouse as $index => $value) {
        //     if ($index) {
        //         $add = warehouse::firstOrCreate([
        //             'name' => Carbon::createFromFormat($this->global->date_format, $index)->format('Y-m-d'),
        //             'location' => $value,
        //         ]);
        //     }
        // }
        return Reply::redirect(route('admin.accounts.index'), __('messages.accountAddedSuccess'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->account = Accounts::find($id);


        return view('admin.accounts.edit', $this->data);
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if($request->is_default ==1)
        {
            $accounts = Accounts::where("is_default",1)->get();
            foreach ($accounts as $acc) {
                $acc->is_default=0;
                $acc->save();
            }
        }
        $users = $request->users;
        
        // return $ac_del;
        foreach ($users as $user) {
            $ac = AccountMembers::where('user_id',$user)->where('account_id',$id)->first();
            if(!$ac){
                $member = new AccountMembers();
                $member->user_id = $user;
                $member->account_id = $request->account_id;
                $member->save();  
            }
            
        }
        $account = Accounts::find($id);
        $account->account_no = $request->account_no;
        $account->name = $request->name;
        $account->initial_balance = $request->initial_balance;
        $account->is_default = $request->is_default;
        $account->note = $request->note;
        $account->save();
        return Reply::redirect(route('admin.accounts.index'), __('messages.accountUpdateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Accounts::destroy($id);
        return Reply::redirect(route('admin.accounts.index'), __('messages.accountsDeletedSuccess'));
    }
    public function destroymember($id)
    {
        //
        AccountMembers::destroy($id);
        return Reply::success(__('messages.memberRemovedFromAccount'));

        // return Reply::redirect(route('admin.transfers.index'), __('messages.transferDeletedSuccess'));
    }
}
