<?php

namespace App\Http\Controllers\Admin;

// use App\warehouse;
use App\DataTables\Admin\UseItemDataTable;
use App\GdprSetting;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use Illuminate\Support\Facades\View;
use App\UseItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class UseItemController extends AdminBaseController
{
     public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.use item');
        $this->pageIcon = 'glyphicon glyphicon-shopping-cart';
        $this->middleware(function ($request, $next) {
            if (!in_array('products', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UseItemDataTable $dataTable)
    {
        //
        return $dataTable->render('admin.useitem.index', $this->data);
        // return view("admin.warehouse.index" , $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
                return View::make('admin.useitem.createuse', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $add = UseItem::firstOrCreate([
                    'warehouse_id' => $request->warehouse,
                    'item_id' => $request->item,
                    'employee_id' => $request->employee,
                    'amount' => $request->amount,
                    // 'price' => $request->price,
                    'note' => $request->note,
                    'date' => $request->date,
                ]);
        // return $request;
        // $warehouse = array_combine($request->date, $request->occasion);
        // foreach ($warehouse as $index => $value) {
        //     if ($index) {
        //         $add = warehouse::firstOrCreate([
        //             'name' => Carbon::createFromFormat($this->global->date_format, $index)->format('Y-m-d'),
        //             'location' => $value,
        //         ]);
        //     }
        // }
        return Reply::redirect(route('admin.useitem.index'), __('messages.useItemAddedSuccess'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $this->useitem = UseItem::find($id);


        return view('admin.useitem.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $useitem = UseItem::find($id);
        $useitem->item_id = $request->item_id;
        $useitem->warehouse_id = $request->warehouse_id;
        $useitem->employee_id = $request->employee_id;
        $useitem->amount = $request->amount;
        $useitem->date  = $request->date;
        $useitem->note  = $request->note;
        $useitem->save();
        return Reply::redirect(route('admin.useitem.index'), __('messages.useItemUpdateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        UseItem::destroy($id);
        return Reply::redirect(route('admin.useitem.index'), __('messages.useItemDeletedSuccess'));
    }
}
