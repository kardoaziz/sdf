<?php

namespace App\Http\Controllers\Admin;

use App\supplier;
use App\purchase_material;
use App\purchase_material_payment;
use App\Helper\Reply;
use Illuminate\Http\Request;

class SupplierController extends AdminBaseController
{


    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.suppliers');
        $this->pageIcon = 'icon-user';
    }


    public function index()
    {
        $this->supplier = Supplier::get();
        foreach ($this->supplier as  $value) {
            # code...
             $value->purchase = purchase_material::where('supplier_id',$value->id)->sum('total'); 
            $value->payment= purchase_material_payment::join('purchase_materials','purchase_materials.id','purchase_material_payments.purchase_id')->where('purchase_materials.supplier_id',$value->id)->sum('amount'); 
            $value->balance= $value->purchase- $value->payment;
        }
        return view('admin.suppliers.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.suppliers.create', $this->data);
    }

    public function quickCreate()
    {
        $this->supplier = Supplier::all();
        return view('admin.suppliers.quick-create', $this->data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplier = new Supplier();
        $supplier->name = $request->name;
        $supplier->email = $request->email;
        $supplier->phone = $request->phone;
        $supplier->address = $request->address;
        $supplier->note = $request->note;
        $supplier->save();

        return Reply::redirect(route('admin.suppliers.index'), __('messages.suppliersAdded'));    }

    /**
     * Display the specified resource.
     *
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(supplier $supplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->supplier = Supplier::findOrFail($id);
        return view('admin.suppliers.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);
        $supplier->name = $request->name;
        $supplier->email = $request->email;
        $supplier->phone = $request->phone;
        $supplier->address = $request->address;
        $supplier->note = $request->note;
        $supplier->save();

        return Reply::redirect(route('admin.suppliers.index'), __('messages.groupUpdatedSuccessfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Supplier::destroy($id);
        return Reply::dataOnly(['status' => 'success']);
    }
}
