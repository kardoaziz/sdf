<?php

namespace App\Http\Controllers\Admin;

// use App\warehouse;
use App\DataTables\Admin\purchasePartsDataTable;
use App\GdprSetting;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;

use Illuminate\Support\Facades\View;
use App\PurchaseParts;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class PurchasePartsController extends AdminBaseController
{
     public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.purchase parts');
        $this->pageIcon = 'glyphicon glyphicon-shopping-cart';
        $this->middleware(function ($request, $next) {
            if (!in_array('products', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PurchasePartsDataTable $dataTable)
    {
        //
        return $dataTable->render('admin.purchase-parts.index', $this->data);
        // return view("admin.warehouse.index" , $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
                return View::make('admin.purchase-parts.createpurchase', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $add = PurchaseParts::firstOrCreate([
                    'part_id' => $request->part,
                    'qty' => $request->qty,
                    'price' => $request->price,
                    'note' => $request->note,
                    'date' => $request->date,
                    'account_id' => $request->account_id,
                ]);
        // return $request;
        // $warehouse = array_combine($request->date, $request->occasion);
        // foreach ($warehouse as $index => $value) {
        //     if ($index) {
        //         $add = warehouse::firstOrCreate([
        //             'name' => Carbon::createFromFormat($this->global->date_format, $index)->format('Y-m-d'),
        //             'location' => $value,
        //         ]);
        //     }
        // }
        return Reply::redirect(route('admin.purchaseparts.index'), __('messages.purchaseAddedSuccess'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->purchase = PurchaseParts::find($id);


        return view('admin.purchase-parts.edit', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $purchase = PurchaseParts::find($id);
        $purchase->part_id = $request->part_id;
        $purchase->qty = $request->qty;
        $purchase->price = $request->price;
        $purchase->date  = $request->date;
        $purchase->note  = $request->note;
        $purchase->account_id  = $request->account_id;
        $purchase->save();
        return Reply::redirect(route('admin.purchaseparts.index'), __('messages.purchaseUpdateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        PurchaseParts::destroy($id);
        return Reply::redirect(route('admin.purchaseparts.index'), __('messages.purchaseDeletedSuccess'));
    }
}
