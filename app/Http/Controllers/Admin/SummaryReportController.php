<?php

namespace App\Http\Controllers\Admin;

// use App\DataTables\Admin\ThirdPartyDataTable;
// use App\DataTables\Admin\ThirdPartyProjectsDataTable;
// use App\DataTables\Admin\ThirdPartyEstimatesDataTable;
// use App\DataTables\Admin\ThirdPartyInvoicesDataTable;
use App\GdprSetting;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;

use Illuminate\Support\Facades\View;
// use App\ThirdParty;
// use App\ThirdPartyPayments;
// use App\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
class SummaryReportController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.summaryReport');
        $this->pageIcon = 'ti-pie-chart';
    }
    public function index()
    {
       $this->fromDate = Carbon::today()->subDays(30);
        $this->toDate = Carbon::today()->addDays(1)->subMinutes(1);
        return view('admin.reports.summary', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request;
        $this->fromDate = Carbon::parse($request->startDate)->format('Y-m-d');
         // date("d-m-Y", strtotime($request->startDate));
        $this->toDate = Carbon::parse($request->endDate)->format('Y-m-d');
        // return $this->data;
        return view('admin.reports.summary', $this->data);
        // return view('admin.reports.summary', $this->data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
