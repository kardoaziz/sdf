<?php

namespace App\Http\Controllers;

use App\ThirdPartyPayments;
use Illuminate\Http\Request;

class ThirdPartyPaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return View::make('admin.thirdparty.createpayment', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $p = new ThirdPartyPayments();
        $p->amount = $require->amount;
        $p->date = $require->date;
        $p->note = $require->note;
        $p->thirdparty_id = $require->thirdparty_id;
        $p->save();
        return Reply::redirect(route('admin.thirdparty.payments',$request->thirdparty_id), __('messages.thirdPartyPaymentAddedSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ThirdPartyPaymentsm  $thirdPartyPaymentsm
     * @return \Illuminate\Http\Response
     */
    public function show(ThirdPartyPaymentsm $thirdPartyPaymentsm)
    {
        //
    } 
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ThirdPartyPaymentsm  $thirdPartyPaymentsm
     * @return \Illuminate\Http\Response
     */
    public function edit(ThirdPartyPaymentsm $thirdPartyPaymentsm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ThirdPartyPaymentsm  $thirdPartyPaymentsm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ThirdPartyPaymentsm $thirdPartyPaymentsm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ThirdPartyPaymentsm  $thirdPartyPaymentsm
     * @return \Illuminate\Http\Response
     */
    public function destroy(ThirdPartyPaymentsm $thirdPartyPaymentsm)
    {
        //
    }
}
