<?php
namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\machinesDataTable;
use App\GdprSetting;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;

use Illuminate\Support\Facades\View;
use App\Machine;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class MachineController extends AdminBaseController
{
     public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.machines');
        $this->pageIcon = 'glyphicon glyphicon-list';
        $this->middleware(function ($request, $next) {
            if (!in_array('products', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(machinesDataTable $dataTable)
    {
        //
               return $dataTable->render('admin.machines.index', $this->data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    return View::make('admin.machines.createmachine', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $add = Machine::firstOrCreate([
                    'name' => $request->machine[0],
                    // 'location' => $request->location[0],
                ]);
        
        return Reply::redirect(route('admin.machine.index'), __('messages.machineAddedSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
          $this->machine = Machine::find($id);


        return view('admin.machines.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $machine = Machine::find($id);
        $machine->name = $request->name;
        $machine->save();
        return Reply::redirect(route('admin.machine.index'), __('messages.machineUpdateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        machine::destroy($id);
        return Reply::redirect(route('admin.machine.index'), __('messages.machineDeletedSuccess'));
    }
}
