<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\RowMaterialDataTable;
use App\Helper\Reply;
use App\Http\Requests\RowMaterial\StoreRowMaterialRequest;
use App\Http\Requests\RowMaterial\UpdateRowMaterialRequest;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\rowMaterial;
use Illuminate\Http\Request;

class RowMaterialController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        parent::__construct();
        $this->pageTitle = __('app.menu.rowmaterial');
        $this->pageIcon = 'icon-basket';
    }

    public function index(RowMaterialDataTable $dataTable)
    {
        $this->totalrowMaterial = rowMaterial::count();
        return $dataTable->render('admin.rowmaterial.index', $this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.rowmaterial.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRowMaterialRequest $request)
    {
        $rowMaterial = new rowMaterial();
        $rowMaterial->name = $request->name;
        $rowMaterial->price = $request->price;
        $rowMaterial->description = $request->description;
        // $rowMaterial->amount = $request->amount;
        $rowMaterial->code = $request->code;
        $rowMaterial->save();

        return Reply::redirect(route('admin.rowmaterial.index'), __('messages.productAdded'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\rowMaterial  $rowMaterial
     * @return \Illuminate\Http\Response
     */
    public function show(rowMaterial $rowMaterial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\rowMaterial  $rowMaterial
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->rowMaterial = rowMaterial::find($id);

        return view('admin.rowmaterial.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\rowMaterial  $rowMaterial
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRowMaterialRequest $request, $id)
    {
        $rowMaterial = rowMaterial::find($id);
        $rowMaterial->name = $request->name;
        $rowMaterial->price = $request->price;
        $rowMaterial->description = $request->description;
        // $rowMaterial->amount = $request->amount;
        $rowMaterial->code = $request->code;
        $rowMaterial->save();

        return Reply::redirect(route('admin.rowmaterial.index'), __('messages.productUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\rowMaterial  $rowMaterial
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        rowMaterial::destroy($id);
        return Reply::success(__('messages.productDeleted'));
    }

    public function export() {
        $rowMaterial = rowMaterial::select('id', 'name', 'code', 'price','description')
            ->get()->makeHidden($attributes);;

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Code', 'Price','Description'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rowMaterial as $row) {
            $dataArray = $row->toArray();
            $dataArray['total_amount'] = $this->global->currency->currency_symbol . '' . $dataArray['total_amount'];
            $exportArray[] = $dataArray;
        }

        // Generate and return the spreadsheet
        Excel::create('rowMaterial', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Row Material');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('Product file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }
}
