<?php

namespace App\Http\Controllers\Admin;

// use App\warehouse;
use App\DataTables\Admin\ThirdPartyDataTable;
use App\DataTables\Admin\ThirdPartyProjectsDataTable;
use App\DataTables\Admin\ThirdPartyEstimatesDataTable;
use App\DataTables\Admin\ThirdPartyInvoicesDataTable;
use App\GdprSetting;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;

use Illuminate\Support\Facades\View;
use App\ThirdParty;
use App\ThirdPartyPayments;
use App\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ThirdPartyController extends AdminBaseController
{
     public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.thirdparties');
        $this->pageIcon = 'glyphicon glyphicon-list';
        $this->middleware(function ($request, $next) {
            if (!in_array('products', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ThirdPartyDataTable $dataTable)
    {
        //
        return $dataTable->render('admin.thirdparty.index', $this->data);
        // return view("admin.warehouse.index" , $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
                return View::make('admin.thirdparty.create', $this->data);

    }
    public function createpayment($id)
    {
        $this->id = $id;
      return View::make('admin.thirdparty.createpayment', $this->data);

    }
    public function payments($id)
    {
        //
        // return "yes";
        $this->pageTitle = __('app.menu.thirdparties');
        $this->pageIcon = 'glyphicon glyphicon-list';
         $this->thirdparty = ThirdParty::find($id);
        $this->payments = ThirdPartyPayments::where('thirdparty_id',$id)->get();
        return view('admin.thirdparty.payments',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $add = ThirdParty::firstOrCreate([
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'email' => $request->email,
                    'note' => $request->note,
                    // 'location' => $request->location[0],
                ]);
        // $warehouse = array_combine($request->date, $request->occasion);
        // foreach ($warehouse as $index => $value) {
        //     if ($index) {
        //         $add = warehouse::firstOrCreate([
        //             'name' => Carbon::createFromFormat($this->global->date_format, $index)->format('Y-m-d'),
        //             'location' => $value,
        //         ]);
        //     }
        // }
        return Reply::redirect(route('admin.thirdparty.index'), __('messages.thirdPartyAddedSuccess'));

    }
public function storepayment(Request $request)
    {
        //
        $p = new ThirdPartyPayments();
        $p->amount = $request->amount;
        $p->date = $request->date;
        $p->note = $request->note;
        $p->thirdparty_id = $request->thirdparty_id;
        $p->save();
        return Reply::redirect(route('admin.thirdparty.payments',$request->thirdparty_id), __('messages.thirdPartyPaymentAddedSuccess'));
    }
    public function delpayment($id)
    {
        //
        $p =  ThirdPartyPayments::find($id);
        $pp= $p->thirdparty_id;
        $p->destroy($id);

        return redirect(route('admin.thirdparty.payments',$pp))->with('success', trans('main.item model Deleted Successfully'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function projects(ThirdPartyProjectsDataTable $dataTable,$id)
    {
        //
        // $projects = Project::where('thirdparty_id',$id)->get();
        // return view('admin.thirdparty.projects', $this->data);
        return $dataTable->with('thirdparty_id',$id)->render('admin.thirdparty.projects', $this->data);
    }
    public function thirdparty($id)
    {
        $thirdparty = Project::find($id);
        return $thirdparty->thirdparty_id;
    }
    public function invoices(ThirdPartyInvoicesDataTable $dataTable,$id)
    {
        // return "yes";
        //
        // $projects = Project::where('thirdparty_id',$id)->get();
        // return view('admin.thirdparty.projects', $this->data);
        return $dataTable->with('thirdparty_id',$id)->render('admin.thirdparty.invoices', $this->data);
    }
    public function estimates(ThirdPartyEstimatesDataTable $dataTable,$id)
    {
        //
        // $projects = Project::where('thirdparty_id',$id)->get();
        // return view('admin.thirdparty.projects', $this->data);
        return $dataTable->with('thirdparty_id',$id)->render('admin.thirdparty.estimates', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->thirdparty = ThirdParty::find($id);


        return view('admin.thirdparty.edit', $this->data);
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $item = ThirdParty::find($id);
        $item->name = $request->name;
        $item->phone = $request->phone;
        $item->email = $request->email;
        $item->note = $request->note;
        $item->save();
        return Reply::redirect(route('admin.thirdparty.index'), __('messages.thirdPartyUpdateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        ThirdParty::destroy($id);
        return Reply::redirect(route('admin.thirdparty.index'), __('messages.thirdPartyDeletedSuccess'));
    }
}
