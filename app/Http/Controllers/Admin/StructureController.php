<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\EventAttendee;
use App\Helper\Reply;
use App\Http\Requests\Events\StoreEvent;
use App\Http\Requests\Events\UpdateEvent;
use App\Notifications\EventInvite;
use App\User;
use App\EmployeeDetails;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class StructureController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.structure');
        $this->pageIcon = 'glyphicon glyphicon-grain';
        // $this->middleware(function ($request, $next) {
        //     if (!in_array('events', $this->user->modules)) {
        //         abort(403);
        //     }
            // return $next($request);
        // });
    }
// 
    public function index()
    {
       
        $employees = EmployeeDetails::all();
        $this->employees= $employees;
        return view('admin.structure.index', $this->data);
    }

    public function store(StoreEvent $request)
    {
        
    }

    public function edit($id)
    {
        
    }

    public function update(UpdateEvent $request, $id)
    {
        
    }

    public function show($id)
    {
        
    }

    public function removeAttendee(Request $request)
    {
        
    }

    public function destroy($id)
    {
        
    }
}
