<?php

namespace App\Http\Controllers\Admin;

// use App\warehouse;
use App\DataTables\Admin\purchaseItemsDataTable;
use App\GdprSetting;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;

use Illuminate\Support\Facades\View;
use App\purchaseItems;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class PurchaseItemController extends AdminBaseController
{
     public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.purchase items');
        $this->pageIcon = 'glyphicon glyphicon-shopping-cart';
        $this->middleware(function ($request, $next) {
            if (!in_array('products', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(purchaseItemsDataTable $dataTable)
    {
        //
        return $dataTable->render('admin.purchaseitems.index', $this->data);
        // return view("admin.warehouse.index" , $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
                return View::make('admin.purchaseitems.createpurchase', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $add = purchaseItems::firstOrCreate([
                    'warehouse_id' => $request->warehouse,
                    'item_id' => $request->item,
                    'amount' => $request->amount,
                    'price' => $request->price,
                    'note' => $request->note,
                    'date' => $request->date,
                    'account_id' => $request->account_id,
                ]);
        // return $request;
        // $warehouse = array_combine($request->date, $request->occasion);
        // foreach ($warehouse as $index => $value) {
        //     if ($index) {
        //         $add = warehouse::firstOrCreate([
        //             'name' => Carbon::createFromFormat($this->global->date_format, $index)->format('Y-m-d'),
        //             'location' => $value,
        //         ]);
        //     }
        // }
        return Reply::redirect(route('admin.purchaseitems.index'), __('messages.purchaseAddedSuccess'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->purchase = purchaseItems::find($id);


        return view('admin.purchaseitems.edit', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $purchase = purchaseItems::find($id);
        $purchase->item_id = $request->item_id;
        $purchase->warehouse_id = $request->warehouse_id;
        $purchase->amount = $request->amount;
        $purchase->price = $request->price;
        $purchase->date  = $request->date;
        $purchase->note  = $request->note;
        $purchase->save();
        return Reply::redirect(route('admin.purchaseitems.index'), __('messages.purchaseUpdateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        purchaseItems::destroy($id);
        return Reply::redirect(route('admin.purchaseitems.index'), __('messages.purchaseDeletedSuccess'));
    }
}
