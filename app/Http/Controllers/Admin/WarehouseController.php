<?php

namespace App\Http\Controllers\Admin;
// 
// use App\warehouse;
use App\DataTables\Admin\warehouseDataTable;
use App\DataTables\Admin\stockListDataTable;
use App\GdprSetting;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;

use Illuminate\Support\Facades\View;
use App\warehouse;
use App\Items;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class WarehouseController extends AdminBaseController
{
     public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.warehouses');
        $this->pageIcon = 'glyphicon glyphicon-compressed';
        $this->middleware(function ($request, $next) {
            if (!in_array('products', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(warehouseDataTable $dataTable)
    {
        //
        return $dataTable->render('admin.warehouse.index', $this->data);
        // return view("admin.warehouse.index" , $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
                return View::make('admin.warehouse.createwarehouse', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $add = warehouse::firstOrCreate([
                    'name' => $request->warehouse[0],
                    'location' => $request->location[0],
                ]);
        // $warehouse = array_combine($request->date, $request->occasion);
        // foreach ($warehouse as $index => $value) {
        //     if ($index) {
        //         $add = warehouse::firstOrCreate([
        //             'name' => Carbon::createFromFormat($this->global->date_format, $index)->format('Y-m-d'),
        //             'location' => $value,
        //         ]);
        //     }
        // }
        return Reply::redirect(route('admin.warehouse.index'), __('messages.warehouseAddedSuccess'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(stockListDataTable $dataTable,$id)
    {
        //
    //     $dataTable = stockListDataTable
    // return $dataTable->render('admin.warehouse.stocklist', $this->data);
        $this->warehouse=warehouse::find($id)->name;
        return $dataTable->with('warehouse_id',$id)
           ->render('admin.warehouse.stocklist',$this->data);
        // return redirect()->route('datatable', [$id]);

    }
    public function getItems($id)
    {
        # code...
        $items = Items::where('id','>',0)
        ->select('items.id','items.name',
            DB::raw('((select COALESCE(sum(purchase_items.amount),0)  from purchase_items where purchase_items.item_id=items.id and warehouse_id='.$id.' )-(select COALESCE(sum(use_items.amount),0)  from use_items where use_items.item_id=items.id and warehouse_id='.$id.')) as stock'))->orderby('stock','desc')->get();
        return $items;

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);
        // $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->id)->first();
        // $this->skills = Skill::all()->pluck('name')->toArray();
        // $this->teams  = Team::all();
        $this->warehouse = warehouse::find($id);

        // if (!is_null($this->warehouse)) {
        //     $this->employeeDetail = $this->employeeDetail->withCustomFields();
        //     $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
        // }

        return view('admin.warehouse.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $warehouse = warehouse::find($id);
        $warehouse->name = $request->name;
        $warehouse->location = $request->location;
        $warehouse->save();
        return Reply::redirect(route('admin.warehouse.index'), __('messages.warehouseUpdateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        warehouses::destroy($id);
        return Reply::redirect(route('admin.warehouse.index'), __('messages.warehouseDeletedSuccess'));
    }
}
