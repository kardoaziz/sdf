<?php

namespace App\Http\Controllers\Admin;

use App\purchase_material;
use Illuminate\Http\Request;
use App\Currency;
use App\DataTables\Admin\PurchaseMDataTable;
use App\Helper\Reply;
use App\Http\Requests\StoreEstimate;
use App\InvoiceSetting;
use App\Notifications\NewEstimate;
use App\rowMaterial;
use App\purchase_material_item;
use App\Setting;
use App\supplier;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class PurchaseMaterialController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.purchasematerial');
        $this->pageIcon = 'ti-file';
    }

    public function index(PurchaseMDataTable $dataTable)
    {
        // return $this->data;
        return $dataTable->render('admin.purchasematerial.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->suppliers = supplier::all();
        $this->currencies = Currency::all();
        $this->rowMaterial = rowMaterial::all();
        return view('admin.purchasematerial.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $items = $request->input('item_name');
        $cost_per_item = $request->input('cost_per_item');
        $quantity = $request->input('quantity');
        $amount = $request->input('amount');
        // $account = $request->input('account');

        if (trim($items[0]) == '' || trim($items[0]) == '' || trim($cost_per_item[0]) == '') {
            return Reply::error(__('messages.addItem'));
        }

        foreach ($quantity as $qty) {
            if (!is_numeric($qty) && (intval($qty) < 1)) {
                return Reply::error(__('messages.quantityNumber'));
            }
        }

        foreach ($cost_per_item as $rate) {
            if (!is_numeric($rate)) {
                return Reply::error(__('messages.unitPriceNumber'));
            }
        }

        foreach ($amount as $amt) {
            if (!is_numeric($amt)) {
                return Reply::error(__('messages.amountNumber'));
            }
        }

        foreach ($items as $itm) {
            if (is_null($itm)) {
                return Reply::error(__('messages.itemBlank'));
            }
        }

        $estimate = new purchase_material();
        $estimate->supplier_id = $request->supplier_id;
        $estimate->purchase_no = $request->purchase_no;
        $estimate->valid_till = Carbon::parse($request->valid_till)->format('Y-m-d');
        $estimate->sub_total = round($request->sub_total, 2);
        $estimate->total = round($request->total, 2);
        $estimate->currency_id = 1;
        $estimate->note = $request->note;
        $estimate->account_id = $request->account_id;
        $estimate->status = 'unpaid';
        $estimate->save();

        foreach ($items as $key => $item):
            if (!is_null($item)) {
                purchase_material_item::create(
                    [
                        'estimate_id' => $estimate->id,
                        'item_name' => $item[$key],
                        'type' => 'item',
                        'quantity' => $quantity[$key],
                        'unit_price' => round($cost_per_item[$key], 2),
                        'amount' => round($amount[$key], 2),
                        // 'account_id' => $account[$key],
                    ]
                );
            }
        endforeach;

        $this->logSearchEntry($estimate->id, 'Estimate #' . $estimate->id, 'admin.purchasematerial.edit', 'estimate');

        return Reply::redirect(route('admin.purchasematerial.index'), __('messages.estimateCreated'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\purchase_material  $purchase_material
     * @return \Illuminate\Http\Response
     */
    public function show(purchase_material $purchase_material)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\purchase_material  $purchase_material
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->purchase_materials = purchase_material::findOrFail($id);
        $this->suppliers = supplier::all();
        $this->currencies = Currency::all();
        $this->rowMaterial = rowMaterial::all();

        return view('admin.purchasematerial.edit', $this->data);    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\purchase_material  $purchase_material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $items = $request->input('item_name');
        $itemsSummary = $request->input('item_summary');
        $cost_per_item = $request->input('cost_per_item');
        $quantity = $request->input('quantity');
        $account = $request->input('account');

        $amount = $request->input('amount');

        if (trim($items[0]) == '' || trim($items[0]) == '' || trim($cost_per_item[0]) == '') {
            return Reply::error(__('messages.addItem'));
        }

        foreach ($quantity as $qty) {
            if (!is_numeric($qty) && $qty < 1) {
                return Reply::error(__('messages.quantityNumber'));
            }
        }

        foreach ($cost_per_item as $rate) {
            if (!is_numeric($rate)) {
                return Reply::error(__('messages.unitPriceNumber'));
            }
        }

        foreach ($amount as $amt) {
            if (!is_numeric($amt)) {
                return Reply::error(__('messages.amountNumber'));
            }
        }

        foreach ($items as $itm) {
            if (is_null($itm)) {
                return Reply::error(__('messages.itemBlank'));
            }
        }

        $estimate = purchase_material::findOrFail($id);
        $estimate->supplier_id = $request->supplier_id;
        $estimate->valid_till = Carbon::parse($request->valid_till)->format('Y-m-d');
        $estimate->sub_total = round($request->sub_total, 2);
        $estimate->total = round($request->total, 2);
        $estimate->currency_id = $request->currency_id;
        $estimate->account_id = $request->account;
        $estimate->save();

        // delete and create new
        purchase_material_item::where('estimate_id', $estimate->id)->delete();
// 
        foreach ($items as $key => $item):
            purchase_material_item::create([
                'estimate_id' => $estimate->id,
                'item_name' => $item,
                'type' => 'item',
                'quantity' => $quantity[$key],
                //'account_id' => $account[$key],
                'unit_price' => round($cost_per_item[$key], 2),
                'amount' => round($amount[$key], 2),
            ]);
        endforeach;

        return Reply::redirect(route('admin.purchasematerial.index'), __('messages.estimateUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\purchase_material  $purchase_material
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        purchase_material::destroy($id);
        return Reply::success(__('messages.estimateDeleted'));
    }

    public function domPdfObjectForDownload($id)
    {
        $this->estimate = purchase_material::findOrFail($id);


        $items = purchase_material_item::where('estimate_id', $this->estimate->id)->get();


        //        return $this->invoice->project->client->client[0]->address;
        $this->settings = Setting::findOrFail(1);

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('admin.purchasematerial.estimate-pdf', $this->data);
        $filename = 'material-' . $this->estimate->id;

        return [
            'pdf' => $pdf,
            'fileName' => $filename
        ];
    }

    public function download($id)
    {
        $pdfOption = $this->domPdfObjectForDownload($id);
        $pdf = $pdfOption['pdf'];
        $filename = $pdfOption['fileName'];

        return $pdf->download($filename . '.pdf');
    }


    public function createPayment($invoiceId)
    {
        $this->invoice = purchase_material::findOrFail($invoiceId);
        $this->paidAmount = $this->invoice->amountPaid();


        if ($this->invoice->status == 'paid') {
            return "Invoice already paid";
        } 

        return view('admin.purchasematerial.createPayment', $this->data);
    }

}
