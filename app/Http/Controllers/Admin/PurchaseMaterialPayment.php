<?php

namespace App\Http\Controllers\Admin;

use App\Currency;
use App\DataTables\Admin\PaymentsDataTable;
use App\Helper\Reply;
use App\Http\Requests\PurchaseMaterialPayment\ImportPayment;
use App\Http\Requests\PurchaseMaterialPayment\StorePayment;
use App\Http\Requests\PurchaseMaterialPayment\UpdatePayments;
use App\purchase_material;
use App\purchase_material_payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class PurchaseMaterialPayment extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.purchasematerial');
        $this->pageIcon = 'ti-file';
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePayment $request)
    {
        $payment = new purchase_material_payment();
             if ($request->has('purchase_id')) {
            $invoice = purchase_material::findOrFail($request->purchase_id);
            $payment->purchase_id = $invoice->id;
            $payment->currency_id = $invoice->currency->id;
            $paidAmount = $invoice->amountPaid();
        } else {
            $currency = Currency::first();
            $payment->currency_id = $request->currency_id;
        }
 
        $payment->amount = round($request->amount, 2);
        $payment->gateway = $request->gateway;
        $payment->transaction_id = $request->transaction_id;
        $payment->paid_on =  Carbon::createFromFormat('d/m/Y H:i', $request->paid_on)->format('Y-m-d H:i:s');

        $payment->remarks = $request->remarks;
        $payment->save();

        if ($request->has('purchase_id')) {

            if (($paidAmount + $request->amount) >= $invoice->total) {
                $invoice->status = 'paid';
            } else {
                $invoice->status = 'partial';
            }
            $invoice->save();
        }



        return Reply::redirect(route('admin.purchasematerial.index'), __('messages.paymentSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->payments = purchase_material_payment::where('purchase_id',$id)->get();
        return view('admin.purchasematerial.payments', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->payment = purchase_material_payment::findOrFail($id);
        $this->currencies = Currency::all();

        return view('admin.purchasematerial.editPayment', $this->data);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePayments $request, $id)
    {
        $payment = purchase_material_payment::findOrFail($id);
        $payment->currency_id = $request->currency_id;
        $payment->amount = round($request->amount, 2);
        $payment->gateway = $request->gateway;
        $payment->transaction_id = $request->transaction_id;

        if ($request->paid_on != '') {
            $payment->paid_on = Carbon::createFromFormat('d/m/Y H:i', $request->paid_on)->format('Y-m-d H:i:s');
        } else {
            $payment->paid_on = null;
        }

        $payment->status = $request->status;
        $payment->remarks = $request->remarks;
        $payment->save();

        // change invoice status if exists
        if ($payment->purchaseMaterial) {
            if ($payment->purchaseMaterial->amountDue() <= 0) {
                $payment->purchaseMaterial->status = 'paid';
            } else if ($payment->purchaseMaterial->amountDue() >= $payment->purchaseMaterial->total) {
                $payment->purchaseMaterial->status = 'unpaid';
            } else {
                $payment->purchaseMaterial->status = 'partial';
            }
            $payment->purchaseMaterial->save();
        }

        return Reply::redirect(route('admin.purchasematerial.index'), __('messages.paymentSuccess'));
    }


    public function destroy($id)
    {
        $payment = purchase_material_payment::find($id);

        // change invoice status if exists
        if ($payment->purchaseMaterial) {
            $due = $payment->purchaseMaterial->amountDue() + $payment->amount;
            if ($due <= 0) {
                $payment->purchaseMaterial->status = 'paid';
            } else if ($due >= $payment->purchaseMaterial->total) {
                $payment->purchaseMaterial->status = 'unpaid';
            } else {
                $payment->purchaseMaterial->status = 'partial';
            }
            $payment->purchaseMaterial->save();
        }

        $payment->delete();

        return Reply::success(__('messages.paymentDeleted'));
    }
}
