<?php

namespace App\Http\Controllers\Admin;

// use App\warehouse;
use App\DataTables\Admin\MaintainMachineDataTable;
use App\GdprSetting;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;

use Illuminate\Support\Facades\View;
use App\MaintainMachine;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class MaintainMachineController extends AdminBaseController
{
     public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.maintain machine');
        $this->pageIcon = 'glyphicon glyphicon-shopping-cart';
        $this->middleware(function ($request, $next) {
            if (!in_array('products', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(MaintainMachineDataTable $dataTable)
    {
        //
        return $dataTable->render('admin.maintain-machine.index', $this->data);
        // return view("admin.warehouse.index" , $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
                return View::make('admin.maintain-machine.createmaintain', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $add = MaintainMachine::firstOrCreate([
                    'part_id' => $request->part_id,
                    'machine_id' => $request->machine_id,
                    'qty' => $request->qty,
                    'note' => $request->note,
                    'status' => $request->status,
                    'schedule' => $request->schedule,
                    'date' => $request->date,
                ]);
        // return $request;
        // $warehouse = array_combine($request->date, $request->occasion);
        // foreach ($warehouse as $index => $value) {
        //     if ($index) {
        //         $add = warehouse::firstOrCreate([
        //             'name' => Carbon::createFromFormat($this->global->date_format, $index)->format('Y-m-d'),
        //             'location' => $value,
        //         ]);
        //     }
        // }
        return Reply::redirect(route('admin.maintainmachine.index'), __('messages.purchaseAddedSuccess'));

    }
// 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MaintainMachineDataTable $dataTable,$id)
    {
        //
        // $this->maintain=MaintainMachine::where($id)->name;
        return $dataTable->with('schedule',1)
           ->render('admin.maintain-machine.index',$this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->maintain = MaintainMachine::find($id);


        return view('admin.maintain-machine.edit', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $maintain = MaintainMachine::find($id);
        $maintain->part_id = $request->part_id;
        $maintain->machine_id = $request->machine_id;
        $maintain->qty = $request->qty;
        // $maintain->price = $request->price;
        $maintain->date  = $request->date;
        $maintain->status  = $request->status;
        $maintain->schedule  = $request->schedule;
        $maintain->note  = $request->note;
        $maintain->save();
        return Reply::redirect(route('admin.maintainmachine.index'), __('messages.purchaseUpdateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        MaintainMachine::destroy($id);
        return Reply::redirect(route('admin.maintainmachine.index'), __('messages.purchaseDeletedSuccess'));
    }
}
