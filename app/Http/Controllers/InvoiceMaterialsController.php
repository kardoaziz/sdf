<?php

namespace App\Http\Controllers;

use App\invoice_materials;
use Illuminate\Http\Request;

class InvoiceMaterialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\invoice_materials  $invoice_materials
     * @return \Illuminate\Http\Response
     */
    public function show(invoice_materials $invoice_materials)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\invoice_materials  $invoice_materials
     * @return \Illuminate\Http\Response
     */
    public function edit(invoice_materials $invoice_materials)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\invoice_materials  $invoice_materials
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, invoice_materials $invoice_materials)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\invoice_materials  $invoice_materials
     * @return \Illuminate\Http\Response
     */
    public function destroy(invoice_materials $invoice_materials)
    {
        //
    }
}
