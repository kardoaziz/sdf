<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends BaseModel
{
    //
    protected $appends = ['total_salary'];
      public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function getTotalSalaryAttribute($value)
    {
    	return $this->no_work_hours*$this->hour_rate+$this->extra_hour*$this->extra_hour_rate-$this->penalty+$this->gift;//$value;
       
    }
}
